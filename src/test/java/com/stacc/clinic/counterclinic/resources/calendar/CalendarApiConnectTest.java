package com.stacc.clinic.counterclinic.resources.calendar;

import com.stacc.clinic.counterclinic.resources.appointment.calendar.CalendarConnect;
import org.junit.Assert;
import org.junit.Test;

public class CalendarApiConnectTest {
    @Test
    public void itShouldReturnAccessToken()
    {
        String accessToken = CalendarConnect.getCredentials().getAccessToken();
        System.out.println(accessToken);
        Assert.assertNotNull(accessToken);
    }
}
