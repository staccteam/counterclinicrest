package com.stacc.clinic.counterclinic.resources.calendar;

import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.*;
import com.stacc.clinic.counterclinic.resources.appointment.AppointmentService;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.CalendarConnect;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.CalendarServiceImpl;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.EventModel;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.slots.TimeSlotService;
import com.stacc.clinic.counterclinic.resources.user.User;
import com.stacc.clinic.counterclinic.resources.user.UserService;
import com.stacc.clinic.counterclinic.resources.user.UserServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = {"classpath:/spring/applicationContext.xml"})
public class CalendarServiceTest {

    private static final String[] TEST_CAL_IDS = {
            "q6biuc95bk9ftmtevaklvervjk@group.calendar.google.com",
            "rd2j42vm121navh4ln6et1ahns@group.calendar.google.com"
    };

    Calendar gCalendarService;
//    @Autowired
    UserService userService;
//    @Autowired
    CalendarServiceImpl service;

    TimeSlotService  timeSlotService;

    AppointmentService appointmentService;

    @Before
    public void setUp()
    {
        gCalendarService = new Calendar.Builder(new NetHttpTransport(), JacksonFactory.getDefaultInstance(), CalendarConnect.getCredentials())
                                .setApplicationName("Counter Clinic")
                                .build();
        service = new CalendarServiceImpl(gCalendarService, userService, timeSlotService, appointmentService);
    }

    @Ignore
    @Test
    public void itShouldCreateNewCalendar()
    {
        com.google.api.services.calendar.model.Calendar calendarModel = new com.google.api.services.calendar.model.Calendar();
        calendarModel.setSummary("Counter Clinic Appointments");
        com.google.api.services.calendar.model.Calendar createdCal =  service.createCalendar(calendarModel);
        System.out.println(createdCal.getId());
        Assert.assertNotNull(calendarModel);
    }

    @Ignore
    @Test
    public void itShouldDeleteCalendar()
    {
        String calId = "95202gf8gikd35316ako8no184@group.calendar.google.com";
//        calId = "s0skk1b1eup9d8r4vut6d422bc@group.calendar.google.com";
        boolean flag = service.deleteCalendar(calId);
        Assert.assertTrue(flag);
    }

    @Test
    public void itShouldReturnCalendarListEntries()
    {
        List<CalendarListEntry> calendarListEntryList = service.getAllCalendars();
        calendarListEntryList.forEach(calendarListEntry -> {
            System.out.println(calendarListEntry.getId());
        });
        Assert.assertNotNull(calendarListEntryList);
        Assert.assertFalse(calendarListEntryList.isEmpty());
    }

    @Test
    public void deleteCalendars()
    {
        List<CalendarListEntry> calendarListEntryList = service.getAllCalendars();
        for (CalendarListEntry calendarListEntry : calendarListEntryList) {
            if (Arrays.asList(TEST_CAL_IDS).contains(calendarListEntry.getId()))
                continue;
            service.deleteCalendar(calendarListEntry.getId());
        }
    }

    @Ignore
    @Test
    public void itShouldCreateEventInCalendar()
    {
        java.util.Calendar cal = java.util.Calendar.getInstance();
        cal.add(java.util.Calendar.DAY_OF_MONTH, 17);

        String calendarId = "q6biuc95bk9ftmtevaklvervjk@group.calendar.google.com";
        Event event = new Event()
                .setSummary("Appointment With Doctor Priyanka Yadav")
                .setStart(new EventDateTime()
                        .setDateTime(new DateTime(cal.getTime())));
        cal.add(java.util.Calendar.DAY_OF_MONTH, 18);
        event.setEnd(new EventDateTime().setDateTime(new DateTime(cal.getTime())))
            .setAttendees(Arrays.asList(
                    new EventAttendee[]{
                            new EventAttendee().setEmail("varunshrivastava007@gmail.com"),
                            new EventAttendee().setEmail("pvrano@gmail.com")
                    }
            ))
            .setReminders(new Event.Reminders()
                .setUseDefault(false)
                .setOverrides(Arrays.asList(
                        new EventReminder[]{
                                new EventReminder().setMethod("email").setMinutes(24*60),
                                new EventReminder().setMethod("popup").setMinutes(15)
                        }
                )));

        Event eventCreated = service.createEvent(calendarId, event);
        System.out.println("Event ID: "+eventCreated.getId());
        Assert.assertNotNull(eventCreated);
        Assert.assertNotNull(eventCreated.getId());
    }

    @Ignore
    @Test
    public void itShouldDeleteAllEventsFromCalendar()
    {
        List<String> calIds = Arrays.asList(TEST_CAL_IDS);

        for (String calId : calIds) {
            service.getAllEvents(calId)
                .forEach(event -> {
                    System.out.println("Delete Event: " + event.getId());
                    service.deleteEvent(calId, event.getId());
                });
        }
    }

    @Ignore
    @Test
    public void itShouldListAllTheEventsFromTheCalendar()
    {
        String calendarId  = "q6biuc95bk9ftmtevaklvervjk@group.calendar.google.com";
        calendarId = "rd2j42vm121navh4ln6et1ahns@group.calendar.google.com";
        List<Event> events = service.getAllEvents(calendarId);
        events.forEach(event -> {
            System.out.println("Event Summary");
            System.out.println("======================");
            System.out.println(event.getSummary());
            System.out.println(event.getId());
            System.out.println(event.getDescription());
            System.out.println(event.getStart());
            System.out.println(event.getEnd());
            System.out.println("\n");
        });
        Assert.assertNotNull(events);
        Assert.assertFalse(events.isEmpty());
    }

    @Test
    public void itShouldUpdateExistingEvent()
    {
        System.out.println(TimeZone.getTimeZone("Asia/Calcutta").getID());
        String calendarId  = "q6biuc95bk9ftmtevaklvervjk@group.calendar.google.com";
        Event modifiedEvent = service.updateEvent(new EventModel()
                .eventId("qg4m3scr9lpcac2a19ahpt843g")
                .calendarId(calendarId)
                .doctorEmail("pvrano@gmail.com")
                .patientEmail("varunshrivastava007@gmail.com")
                .startTime(1516968000000L)
                .endTime(1516968900000L)
                .summary("Google I/O 2019")
                .timezone(TimeZone.getTimeZone("Asia/Calcutta").getID())
        );
        Assert.assertEquals("Google I/O 2019", modifiedEvent.getSummary());
    }

    @Ignore
    @Test
    public void itShouldDeleteEventFromTheCalendar()
    {
        String calendarId = "rd2j42vm121navh4ln6et1ahns@group.calendar.google.com";
        String eventId = "dus2vpu8ak556gdv6a2uua0j0k";
        service.deleteEvent(calendarId, eventId);
    }
}
