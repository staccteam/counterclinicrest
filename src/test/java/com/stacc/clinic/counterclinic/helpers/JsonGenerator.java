package com.stacc.clinic.counterclinic.helpers;

import com.google.gson.Gson;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.CalendarModel;
import com.stacc.clinic.counterclinic.resources.appointment.queue.AppointmentQueue;
import com.stacc.clinic.counterclinic.resources.appointment.queue.DoctorStatus;
import com.stacc.clinic.counterclinic.resources.clinic.Clinic;
import com.stacc.clinic.counterclinic.resources.user.Permission;
import com.stacc.clinic.counterclinic.resources.user.Role;
import com.stacc.clinic.counterclinic.resources.user.User;
import com.stacc.clinic.counterclinic.resources.user.UserRoles;
import org.junit.Test;

import java.util.Arrays;

public class JsonGenerator {

    @Test
    public void generateJsonForUser()
    {
        User user = User.newInstance();
        user.setId("thisissomestringidfortheuser");
        user.setFirstName("FirstName");
        user.setLastName("LastName");
        user.setClinic(Clinic.newInstance("ClinicIDString", "RoomNumberForTheClinic"));
        user.setRoles(Arrays.asList(
                new Role[]{
                        Role.newInstance(UserRoles.DOCTOR)
                        .addPermission(Permission.newInstance("call_next_patient"))
                }
        ));
        user.setPassword("SomePassword");
        user.setAppointmentQueue(buildAppointmentQueue());
        user.setCalendar(buildCalendarModel());
        user.setEmail("varunshrivastava007@gmail.com");
        user.setMobile("9960543885");
        user.setUsername("vslala");

        System.out.println(new Gson().toJson(user));
    }

    @Test
    public void generateJsonForCalendarModel()
    {
        CalendarModel model = buildCalendarModel();

        System.out.println(new Gson().toJson(model));
    }

    private CalendarModel buildCalendarModel() {
        CalendarModel model = new CalendarModel();
        model.setCalendarId("somecalendarid@google.com");
        model.setTimezone("Asia/Calcutta");
        model.setKind("SomeKind");
        model.setSummary("This is a calendar entry");
        return model;
    }

    @Test
    public void generateJsonForAppointmentQueue()
    {
        AppointmentQueue queue = buildAppointmentQueue();

        System.out.println(new Gson().toJson(queue));
    }

    private AppointmentQueue buildAppointmentQueue() {
        AppointmentQueue queue = new AppointmentQueue();
        queue.setId("SomeStringId");
        queue.setAvgCheckupTime(15395485L);
        queue.setPatientInTimeStamp(76378126312387L);
        queue.setTotalCheckupTime(123412L);
        queue.setExtendedTime(1231L);
        queue.setCurrQueueCount(4);
        queue.setDoctorStatus(DoctorStatus.IN_PROGRESS);
        queue.setQueueSize(10);
        queue.setDoctorId("somedoctoridinstringformat");
        return queue;
    }
}
