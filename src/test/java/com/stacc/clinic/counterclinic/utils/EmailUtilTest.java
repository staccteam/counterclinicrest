package com.stacc.clinic.counterclinic.utils;

import com.stacc.clinic.counterclinic.resources.appointment.OnlineAppointment;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.slots.TimeSlot;
import com.stacc.clinic.counterclinic.resources.user.User;
import org.junit.Ignore;
import org.junit.Test;

public class EmailUtilTest {

    @Ignore
    @Test
    public void itShouldSendEmailToAnyRecepient()
    {
        TimeSlot timeSlot = new TimeSlot();
        timeSlot.setStartTime("11:00");
        OnlineAppointment.Expand onlineAppointment = new OnlineAppointment().new Expand();
        onlineAppointment.setDoctor(User.newInstance().firstName("Varun").lastName("Shrivastava"));
        onlineAppointment.setDate("2019-01-10");
        onlineAppointment.setSlot(timeSlot);
        EmailUtil.sendMail(onlineAppointment, "pvrano@gmail.com");
        EmailUtil.sendMail(onlineAppointment, "varunshrivastava007@gmail.com");
    }
}
