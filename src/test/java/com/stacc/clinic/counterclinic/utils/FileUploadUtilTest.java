package com.stacc.clinic.counterclinic.utils;

import io.jsonwebtoken.lang.Assert;
import org.junit.Test;
import org.springframework.mock.web.MockMultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileUploadUtilTest {

    @Test
    public void itShouldUploadFileOnTheDirectory() throws IOException {
        byte[] fileContent  = Files.readAllBytes(Paths.get("src/test/resources/testData/codes_venue_logo.png"));
        MockMultipartFile mockMultipartFile = new MockMultipartFile("website_icon.png",  fileContent);
        File file = FileUploadUtil.uploadFile(mockMultipartFile, "src/test/resources/tested");
        Assert.notNull(file);
    }
}
