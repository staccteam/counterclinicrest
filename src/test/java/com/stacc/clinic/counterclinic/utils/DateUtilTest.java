package com.stacc.clinic.counterclinic.utils;

import com.stacc.clinic.counterclinic.resources.appointment.calendar.slots.TimeSlot;
import org.junit.Test;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

public class DateUtilTest {

    @Test
    public void itShouldGiveDateAndTime()
    {
        Long timeInMillis =  DateUtil.convertISO8601LocalToMilliseconds("2018-12-26T05:40");
        Long endTimeInMillis = DateUtil.addMinutesToTime(timeInMillis, 15);
        System.out.println(String.format("%d, %d", timeInMillis, endTimeInMillis));
    }

    @Test
    public void itShouldConvertDateToUTCFormat()
    {
        LocalDateTime localDateTime = Instant.ofEpochMilli(1516925400000L).atZone(ZoneId.systemDefault()).toLocalDateTime();
        String date = DateUtil.convertDateToIsoOffsetFormat(localDateTime);
        System.out.println(date);
    }

    @Test
    public void createDateFromMillis()
    {
        System.out.println(new Date(1516925400000L));
    }

    @Test
    public void itShouldCreateTimeSlotsOf15Minutes()
    {
        List<String> timeSlots = DateUtil.getTimeIn15MinutesSlots();
        timeSlots.forEach(slot-> System.out.println(slot));
    }

    @Test
    public void itShouldCreateListOfTimeSlotsBetweenRange()
    {
        List<TimeSlot> timeSlots = DateUtil.getTimeSlots("01:00", "19:00", 30);
        timeSlots.forEach(slot->System.out.println(slot));
    }

    @Test
    public void itShouldConvertTimeSlotTimeToYodaTime()
    {
        String time = DateUtil.convertDateFormatWithSDF("11:30", "HH:mm", DateUtil.YODA_TIME_FORMAT);
        System.out.println(time);
    }
}
