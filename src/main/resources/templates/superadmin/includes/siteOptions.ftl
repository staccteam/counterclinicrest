<hr/>

<div class="card">
	<div class="card-header">
		<h4>Available Site Options</h4>
	</div>
	
	<div class="card-body">
		<!-- Available Site Options -->
				<div class="form-group">
					<#if siteOptions??>
						<table class="table table-responsive">
						<#list siteOptions as option> 
								<tr>
									<td>
										<form action="/superadmin/save-option" method="POST">
											<input type="hidden" 
													name="id"
													value="${option.getId()}"
													/>
											<input type="hidden"
												   name="key"
												   value="${option.getKey()}"
											/>
											<input type="text"
													value="${option.getKey()}"
													disabled="true"
													/>
											
											<input type="text" 
													name="val"
													value="${option.getVal()}"
													/>
											<button type="submit" class="btn btn-primary"><i class="fa fa-save" aria-hidden="true"></i></button>
										</form>
									</td>
									<td>
										<form action="/superadmin/delete-option" method="post">
											<input type="hidden"
												   name="optionId"
												   value="${option.getId()}"
											/>
											<button class="btn btn-danger" type="submit"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
										</form>
									</td>
								</tr>
						</#list>
						</table>
					</#if>
				</div>
			
	</div>
</div>