<hr/>
<div class="card">
	<div class="card-header">
		<h4>Add Custom Option</h4>
	</div>
	
	<div class="card-body">
		<form action="/superadmin/save-option" method="POST">
			<div class="form-group">
				<input type="text" 
						name="key"
						class="form-control" 
						placeholder="Option Name" />
			</div>
			<div class="form-group">
				<input type="text" 
						name="val"
						class="form-control" 
						placeholder="Option Value" />
			</div>
			<div class="form-group">
				<button class="btn btn-primary" type="submit" >Add</button>
			</div>
		</form>
	</div>
</div>