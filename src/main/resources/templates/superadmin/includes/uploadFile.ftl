<div class="card">
	<div class="card-header">
		<h4>Upload Website Logo</h4>
	</div>
	<div class="card-body">
		<div class="logo_preview">
			<#if siteLogo.isPresent()>
				<img src="${siteLogo.get().getVal()}" class="img img-responsive" style="width: 300px;"/>
			<#else>
				<img src="http://via.placeholder.com/300x300" class="img img-responsive" />	
			</#if>
		</div>
		<form action="/superadmin/upload-file" method="POST" enctype="multipart/form-data">
			<div class="form-group">
				<input type="file" name="file" class="form-control" /> 
			</div>
			<div class="form-group">
				<button class="btn btn-primary" type="submit">Upload</button>
			</div>
		</form>
	</div>
</div>