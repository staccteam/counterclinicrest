<div class="card">
	<div class="card-header">
		<h4>Register Admin</h4>	
	</div>
	<div class="card-body">
		<form action="/superadmin/register/admin" method="POST" >
			<input type="hidden" name="userType" value="ADMIN" />
			<div class="form-group">
				<label class="form-label">
					Username: 
				</label>
				<input class="form-control" type="text" name="username" autocomplete="off"/>
			</div> 
			<div class="form-group">
				<label class="form-label">
					Password:
				</label>
				<input class="form-control" type="password" name="password" autocomplete="off"/>
			</div>
			<div class="form-group">
				<label class="form-label">First Name:</label>
				<input type="text" name="firstName" class="form-control" autocomplete="off" />
			</div>
			<div class="form-group">
				<label class="form-label">Last Name:</label>
				<input type="text" name="lastName" class="form-control" autocomplete="off" />
			</div>
			<div class="form-group">
				<label class="form-label">Email:</label>
				<input type="email" name="email" class="form-control" autocomplete="off" />
			</div>
			<div class="form-group">
				<label class="form-label">Mobile:</label>
				<input type="text" name="mobile" class="form-control" autocomplete="off" />
			</div>

			<div class="form-group">
				<button class="btn btn-primary" type="submit">Register Admin</button>
			</div>
		</form>
	</div>
</div>
<#include '../includes/footer.ftl'>
