<#if thisUser??>
	<div class="row">
		<div class="col-md-10">
			<h2>Hello, ${thisUser.getUserMetaValue("first_name")} ${thisUser.getUserMetaValue("last_name")}</h2>
		</div>
		<div class="col-md-2">
			<a href="/dashboard/logout" class="btn btn-lg btn-success">Logout</a>
		</div>
	</div>
	<hr/>
</#if>