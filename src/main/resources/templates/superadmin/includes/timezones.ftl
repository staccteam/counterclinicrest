<div class="card">
	<div class="card-header">
		<h4>Add Timezone</h4>
	</div>
	<div class="card-body">
		<form action="/superadmin/save-option" method="POST">
			<input type="hidden" name="key" value="site_timezone" />
			<!-- Timezone Values will be provided from backend -->
			<#if timezones?has_content>
			<div class="form-group">
				<select name="val" class="form-control">
					<option disabled selected>Select...</option>
					<#list timezones as tz>
					<#if siteTimezone.isPresent() && tz == siteTimezone.get().getVal()!''>
					<option selected value="${tz}">${tz}</option>
					<#else>
					<option value="${tz}">${tz}</option>
				</#if>
			</#list>
			</select>
	</div>
</#if>

<div class="form-group">
	<button class="btn btn-primary" type="submit">Save</button>
</div>
</form>
</div>
</div>