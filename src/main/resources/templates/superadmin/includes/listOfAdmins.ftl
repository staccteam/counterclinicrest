<div class="card">
	<div class="card-header">
		<h4>List of all Registered Admins</h4>
	</div>
	<div class="card-body">
		<#if admins?has_content>
			<table class="table table-condensed">
				<thead>
					<th>#</th>
					<th>Admin Name</th>
					<th></th>
				</thead>
				<tbody>
					<#list admins as admin>
						<tr>
							<td>${admin?counter}</td>
							<td>${admin.getFirstName()} ${admin.getLastName()}</td>
							<td>
								<form action="/register/cancel-registration" method="POST">
									<input type="hidden" value="${admin.getId()}" name="id" />
									<input type="hidden" value="${admin.getUsername()}" name="username" />
									<button type="submit" class="btn btn-danger">Cancel Registration</button>
								</form>
							</td>
						</tr>
					</#list>
				</tbody>
			</table>
			
		</#if>
	</div>
</div>