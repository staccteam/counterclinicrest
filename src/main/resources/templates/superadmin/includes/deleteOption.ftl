<hr/>

<div class="card">
	<div class="card-header">
		<h4>Delete Options</h4>
	</div>
	
	<div class="card-body">
		<!-- Available Site Options -->
		<form action="/dashboard/superadmin/delete-option" method="POST">
			<table class="table">
				<tr>
					<td>
						<#if siteOptions??>
							<select name="optionId" class="form-control">
							<#list siteOptions as option> 
								<option value="${option.getId()}">${option.getVal()}</option>
							</#list>
							</select>
						</#if>
					</td>
					<td>
						<button class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>