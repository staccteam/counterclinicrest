<div class="card">
	<div class="card-block">
		<h4 class="card-title">
			Upload Advertisement Picture
		</h4>
		<h6 class="card-subtitle text-muted mb-4">
			This image will be shown on the mobile screen
		</h6>
	</div>
	<div class="card-body">
		<#if adPicture.isPresent()>
			<img src="${adPicture.get().getVal()}" class="img img-responsive" style="width: 300px; display: block !important;"/>
		<#else>
			<img src="http://via.placeholder.com/300x300" class="img img-responsive" style="display: block !important;"/>
		</#if>
		<form action="/superadmin/upload-ad-pic" method="POST" enctype="multipart/form-data">
			<div class="form-group">
				<input type="file" name="file" class="form-control" /> 
			</div>
			<div class="form-group">
				<button class="btn btn-primary" type="submit">Upload</button>
			</div>
		</form>
	</div>
</div>