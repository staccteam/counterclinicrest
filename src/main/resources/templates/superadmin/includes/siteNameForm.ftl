<div class="card">
	<div class="card-header">
		<h4>Update Website Name</h4>
	</div>
	<div class="card-body">
		<form action="/dashboard/superadmin/save-option" method="post">
			<input type="hidden" name="optionName" value="site_name" />
			<div class="form-group">
				<input type="text" 
					name="optionVal" 
					class="form-control"
					placeholder="ex. Counter Clinic" 
					value="${siteName!''}" />
			</div>
			
			<div class="form-group">
				<button class="btn btn-primary" type="submit" >Save</button>
			</div>
		</form> 
	</div>
</div>