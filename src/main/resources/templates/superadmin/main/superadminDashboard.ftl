<#include '../includes/simpleNavbar.ftl'>

<div class="container">
	<#include '../includes/successMessage.ftl'>
	<div class="row">
		<div class="col-md-12">
			<h2>Admin Management</h2>
			<hr/>
		</div>
		<div class="col-md-4">
			<#include '../includes/registerUserForm.ftl'>
			<hr/>
		</div>
		<div class="col-md-8">
			<!-- List of all admins -->
			<#include '../includes/listOfAdmins.ftl'>
			<hr/>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<h2>Site Settings</h2>
			<hr/>
		</div>
		<div class="col-md-4">
			<#include '../includes/timezones.ftl'>
			<hr/>
		</div>
		<div class="col-md-4">
		<!-- Upload website logo -->
			<#include '../includes/uploadFile.ftl'>
		</div>
		<div class="col-md-4">
		<!--  Update WEbsite name form -->
			<#include '../includes/siteNameForm.ftl'>
		</div>
	</div>
	
	<!-- Custom Site Option Form -->
	<div class="row">
		<div class="col-md-4">
			<#include '../includes/addCustomOption.ftl'>
		</div>
		<div class="col-md-8">
			<#include '../includes/siteOptions.ftl'>
		</div>
	</div>
</div>