<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="${siteDescription!'Counter Clinic is an appointment management application.'}">
    <meta name="author" content="">
    <link rel="icon" href="/global/images/logo/${siteLogo!''}" />

    <title>${title!siteName!'Counter Clinic'}</title>

      <!-- Bootstrap core CSS -->
      <link href="/application/dist/css/bootstrap.min.css" rel="stylesheet">
      <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">-->
      <!-- Icons -->
      <link href="/application/css/font-awesome.css" rel="stylesheet">

      <!-- Custom styles for this template -->
      <link href="/application/css/style.css" rel="stylesheet">
      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<#if view.page??>
		<#include view.page + "/css/css.ftl">
	</#if>
  </head>

  	<body>

    <#function avg nums...>
      <#local sum = 0>
      <#list nums as num>
        <#local sum += num>
      </#list>
      <#if nums?size != 0>
        <#return sum / nums?size>
      </#if>
    </#function>

    <#include 'site/include/_common.ftl'  />

	<#include view.page + "/main/" + view.component + ".ftl" >
	
	
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	<#include view.page + "/scripts/scripts.ftl">

	<footer class="footer" style="background-color: black; color: white; margin-top: 8%;">
      <div class="container center">
        <div class="row justify-content-center">
          <a class="nav-link js-scroll-trigger" href="${siteUrl!'https://www.bemyaficionado.com'}">${siteName!'Counter Clinic'} &copy; ${.now?string('yyyy')}</a>
        </div>
      </div>
    </footer>


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
        <script src="/application/dist/js/bootstrap.min.js"></script>

        <script src="/application/js/chart.min.js"></script>
        <script src="/application/js/chart-data.js"></script>
        <script src="/application/js/easypiechart.js"></script>
        <script src="/application/js/easypiechart-data.js"></script>
        <script src="/application/js/bootstrap-datepicker.js"></script>
        <script src="/application/js/custom.js"></script>
        <script>
                var startCharts = function () {
                            var chart1 = document.getElementById("line-chart").getContext("2d");
                            window.myLine = new Chart(chart1).Line(lineChartData, {
                            responsive: true,
                            scaleLineColor: "rgba(0,0,0,.2)",
                            scaleGridLineColor: "rgba(0,0,0,.05)",
                            scaleFontColor: "#c5c7cc "
                            });
                        };
                    window.setTimeout(startCharts(), 1000);
            </script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
	</body>
</html>

