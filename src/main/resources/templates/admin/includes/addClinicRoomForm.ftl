<div class="card">
	<div class="card-header">
		<h4>Add Clinic Room</h4>
	</div>
	<div class="card-body">
		<form action="/admin/add-clinic" method="POST" class="form">
			<div class="form-group">
				<label class="form-label">Room Number</label>
				<input class="form-control" type="text" name="clinicRoomNumber" />
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary">Add New Clinic</button>
			</div>
		</form>
	</div>
</div>