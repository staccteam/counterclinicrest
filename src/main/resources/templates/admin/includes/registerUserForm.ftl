<div class="card">
	<div class="card-header">
		<h4>Register User</h4>	
	</div>
	<div class="card-body">
		<form action="/admin/create-user" method="POST" >
			<div class="form-group">
				<label class="form-label">
					Username: 
				</label>
				<input class="form-control" type="text" name="username" autocomplete="off"/>
			</div> 
			<div class="form-group">
				<label class="form-label">
					Password:
				</label>
				<input class="form-control" type="password" name="password" autocomplete="off"/>
			</div>
			<div class="form-group">
				<label class="form-label">First Name:</label>
				<input type="text" name="firstName" class="form-control" autocomplete="off" />
			</div>
			<div class="form-group">
				<label class="form-label">Last Name:</label>
				<input type="text" name="lastName" class="form-control" autocomplete="off" />
			</div>
			<div class="form-group">
				<label class="form-label">Email:</label>
				<input type="email" name="email" class="form-control" autocomplete="off" />
			</div>
			<div class="form-group">
				<select name="userType" class="form-control">
					<option disabled>select...</option>
					<option value="DOCTOR">Doctor</option>
					<option value="RECEPTION">Reception</option>
				</select>
			</div>
			<div class="form-group">
				<button class="btn btn-primary" type="submit">Register User</button>
			</div>
		</form>
	</div>
</div>
