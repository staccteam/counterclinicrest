<div class="card">
	<div class="card-header">
		<h2>Clinics</h2>
	</div>
	<div class="card-body">
		<#if clinics?has_content>
		<table class="table">
			<thead>
				<tr>
					<th>#</th>
					<th>Clinic Number</th>
				</tr>
			</thead>
			<tbody>
				<#list clinics as clinic>
					<tr>
						<td><strong>${clinic?counter}</strong></td>
						<td>${clinic.getClinicNumber()}</td>
						<td>
							<form action="/admin/remove-clinic" method="POST">
								<input type="hidden" value="${clinic.getId()}" name="clinicId"/>
								<button class="btn btn-danger" type="submit">Delete</button>
							</form>
						</td>
					</tr>
				</#list>
			</tbody>
		</table>
		<#else>
		<span class="alert alert-warning">
			There are no records in the database.
		</span>
		</#if>
	</div>
</div>