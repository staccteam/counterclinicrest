<form class="form-horizontal" method="post" action="/admin/register-slots">
    <div class="form-group">
        <label>Create timeslot for:</label>
        <#list weekdays as weekday>
        <#if weekday?? && weekday != "">
        <div class="checkbox mt-1 mb-2">
            <div class="custom-control custom-checkbox">
                <input type="checkbox"  name="selectedWeekdays" class="custom-control-input" id="selected_weekdays_${weekday?counter}" value="${weekday}">
                <label class="custom-control-label custom-control-description" for="selected_weekdays_${weekday?counter}">${weekday}</label>
                <!--<div class="float-right action-buttons"><a href="#" class="trash"><em class="fa fa-trash"></em></a></div>-->
            </div>
        </div>
        </#if>
        </#list>
    </div>
    <div class="form-group">
        <label for="what_for">What for:</label>
        <select name="whatFor" class="form-control" id="what_for">
            <option value="OPEN">Opening Hours</option>
            <option value="CLOSE">Closing Hours</option>
        </select>
    </div>
    <div class="form-group">
        <label for="start_time">Start Time</label>
        <input id="start_time" type="time"  name="startTime" class="form-control"  value="00:00" step="900">
    </div>
    <div class="form-group">
        <label for="end_time">End Time</label>
        <input id="end_time" type="time" name="endTime" class="form-control" value="23:45" step="900">
        </select>
    </div>
    <div class="form-group">
        <label for="slot_time">Create Slot of (in minutes):</label>
        <input id="slot_time" type="number" name="slotDuration" class="form-control" />
    </div>
    <div class="form-group">
        <button class="btn btn-primary" type="submit">Create Slots</button>
    </div>
</form>