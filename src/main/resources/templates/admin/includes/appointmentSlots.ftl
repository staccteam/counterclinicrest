<#if slotTemplates?has_content>
    <#list slotTemplates as template>
    <div class="col-sm-4">
        <div class="card">
            <div class="card-block">
                <div class="card-title">
                    <h4>Slots for: ${template.getWhatFor()}</h4>
                </div>
                <div class="card-body">
                    <p>Start Time: <strong>${template.getStartTime()}</strong></p>
                    <p>Start Time: <strong>${template.getEndTime()}</strong></p>
                    <p><strong>Weekdays</strong></p>
                    <ul>
                        <#list template.getSelectedWeekdays() as weekday>
                        <li>${weekday}</li>
                    </#list>
                    </ul>
                </div>
                <div class="card-footer">
                    <center>
                        <ul class="list-inline">
                            <li class="list-inline-item">
                                <a href="/admin/edit-appointment-timeslot?slotId=${template.getId()}" class="btn btn-link">Edit</a>
                            </li>
                            <li class="list-inline-item">
                                <a href="/admin/delete-appointment-timeslot?slotId=${template.getId()}" class="btn btn-danger">Delete</a>
                            </li>
                        </ul>
                    </center>
                </div>
            </div>
        </div>
    </div>
</#list>
</#if>