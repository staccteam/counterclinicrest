<div class="card">
	<div class="card-header">
		<h4>Add Custom Message for Different Inquiries</h4>
	</div>
	<div class="card-body">
		<form>
			<div class="form-group">
				<span>Patient queue count is less than </span>
				<input class="form-control" type="number" name="queueCount" />
				<span>should see message </span>
				<textarea class="form-control" name="customMessage"></textarea>
			</div>
			<div class="form-group">
				<button class="btn btn-primary" type="submit">Add Message!</button>
			</div>
		</form>
	</div>
</div>