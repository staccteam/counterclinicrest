<div class="card">
	<div class="card-header">
		<h4>Counter Clinic Information</h4>
	</div>
	<div class="card-body">
		<table class="table table-condensed">
			<#if displayStats?has_content>
				<thead>
					<th>#</th>
					<th>Doctor's Name</th>
					<th>Clinic Number</th>
					<th># of People Waiting</th>
					<th>Current Patient's Count</th>
					<th>Doctor Status</th>
				</thead>
				<#list displayStats as stats>
				<tbody>
				<tr>
					<td><strong>${stats?counter}</strong></td>
					<#list users as user>
					    <#if user.getId() == stats.getDoctorId()>
					        <td>${user.getFirstName()} ${user.getLastName()}</td>
					        <td><#if user.getClinic()??>${user.getClinic().getClinicNumber()}<#else>NOT ASSIGNED</#if></td>
					    </#if>
					</#list>
					<td><#if stats.getQueueSize()??>${stats.getQueueSize()}<#else>0</#if></td>
					<td>${stats.getCurrQueueCount()}</td>
					<td>
						<form action="/api/changeDoctorStatus" method="POST" id="doctor_status_form" class="doctor-status-form">
							<input type="hidden" value="${stats.getDoctorId()}" name="doctorId" />
							<input type="text" id="doctor_status_input" class="form-control" name="doctorStatus" value="${stats.getDoctorStatus()!'Not Set!'}" />
						</form>
					</td>
				</tr>
				</tbody>
				</#list>
			<#else>
				<span class="alert alert-warning">
					There are no records in the database.
				</span>
			</#if>
		</table>
	</div>
	
</div>
