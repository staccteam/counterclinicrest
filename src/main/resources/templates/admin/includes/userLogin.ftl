<div class="card">
	<div class="card-header">
		<h5>User Password Reset</h5>
	</div>
	<div class="card-body">
		<form class="form-signin" role="form" action="/admin/password-update" method="POST">
		    <div class="form-group">
		    	<input type="text" name="username" id="inputEmail" class="form-control" placeholder="Username" required autocomplete="off">
		    </div>
		    <div class="form-group">
		    	<input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
		    </div>
		    <div class="form-group">
		    	<button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Update Password</button>
		    </div>
		    
		</form>
	</div>
</div>