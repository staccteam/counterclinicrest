<div class="card">
	<div class="card-header">
		<h2>Remove User</h2>
	</div>
	<div class="card-body">
		<#if users?has_content>
			<ul class="list-group">
				<#list users as user>
					<li class="list-group-item">
						<form action="/admin/remove-user" method="POST">
							<input name="id" value="${user.getId()}" type="hidden" />
							<input name="username" value="${user.getUsername()}" type="hidden" />
							<label>${user.getFirstName()} ${user.getLastName()}</label>
							<button class="btn btn-danger float-right" type="submit">Remove User</button>
						</form>
					</li>	
				</#list>
			</ul>
		</#if>
	</div>
</div>