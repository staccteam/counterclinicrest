<#if slot??>
<#assign slotTemplate = slot />
<form class="form-horizontal" method="post" action="/admin/register-slots">
    <input type="hidden" name="id" value="${slot.getId()}">
    <div class="form-group">
        <label>Create timeslot for: ${slotTemplate.getWhatFor()}</label>
        <#list weekdays as weekday>
        <#assign flag = false  />
        <#if weekday?? && weekday != "">
            <#list slotTemplate.getSelectedWeekdays() as selectedWeekday>

            <#if weekday == selectedWeekday>
            <#assign flag = true />
                <div class="checkbox mt-1 mb-2">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox"  name="selectedWeekdays" class="custom-control-input" id="selected_weekdays_${weekday?counter}" value="${weekday}" checked>
                        <label class="custom-control-label custom-control-description" for="selected_weekdays_${weekday?counter}">${weekday}</label>
                        <!--<div class="float-right action-buttons"><a href="#" class="trash"><em class="fa fa-trash"></em></a></div>-->
                    </div>
                </div>
            <#break>
            </#if>
            </#list>

            <#if flag == false>
                <div class="checkbox mt-1 mb-2">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox"  name="selectedWeekdays" class="custom-control-input" id="selected_weekdays_${weekday?counter}" value="${weekday}">
                        <label class="custom-control-label custom-control-description" for="selected_weekdays_${weekday?counter}">${weekday}</label>
                        <!--<div class="float-right action-buttons"><a href="#" class="trash"><em class="fa fa-trash"></em></a></div>-->
                    </div>
                </div>
            </#if>

        </#if>
        </#list>
    </div>
    <div class="form-group">
        <label for="what_for">What for:</label>
        <select name="whatFor" class="form-control" id="what_for">
            <#if slotTemplate.getWhatFor() == "OPEN">
                <option value="OPEN" selected>Opening Hours</option>
                <option value="CLOSE" >Closing Hours</option>
            <#else>
                <option value="OPEN" >Opening Hours</option>
                <option value="CLOSE" selected>Closing Hours</option>
            </#if>
        </select>
    </div>
    <div class="form-group">
        <label for="start_time">Start Time</label>
        <input id="start_time" type="time"  name="startTime" class="form-control"  value="${slotTemplate.getStartTime()}" step="900">
    </div>
    <div class="form-group">
        <label for="end_time">End Time</label>
        <input id="end_time" type="time" name="endTime" class="form-control" value="${slotTemplate.getEndTime()}" step="900">
        </select>
    </div>
    <div class="form-group">
        <label for="slot_time">Create Slot of (in minutes):</label>
        <input id="slot_time" type="number" name="slotDuration" class="form-control" value="${slotTemplate.getSlotDuration()}" />
    </div>
    <div class="form-group">
        <button class="btn btn-primary" type="submit">Create Slots</button>
    </div>
</form>
</#if>