<#include '../includes/simpleNavbar.ftl'>
<div class="container">
    <#include '../includes/greetUser.ftl'>
    <#include '../includes/message.ftl'>

    <div class="row">
        <div class="col-sm-4">
            <h4>Create Timeslot Template</h4>
            <form class="form-horizontal" method="post" action="/admin/register-slots">
                <div class="form-group">
                    <label for="selected_weekdays">Create timeslot for:</label>
                    <#list weekdays as weekday>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input id="selected_weekdays" type="checkbox" name="selectedWeekdays" class="form-check-input" value="${weekday}">${weekday}
                        </label>
                    </div>
                    </#list>
                </div>
                <div class="form-group">
                    <label for="what_for">What for:</label>
                    <select name="whatFor" class="form-control" id="what_for">
                        <option value="OPEN">Opening Hours</option>
                        <option value="CLOSE">Closing Hours</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="start_time">Start Time</label>
                    <input id="start_time" type="time"  name="startTime" class="form-control"  value="00:00" step="900">
                </div>
                <div class="form-group">
                    <label for="end_time">End Time</label>
                    <input id="end_time" type="time" name="endTime" class="form-control" value="23:45" step="900">
                    </select>
                </div>
                <div class="form-group">
                    <label for="slot_time">Create Slot of (in minutes):</label>
                    <input id="slot_time" type="number" name="slotDuration" class="form-control" />
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Create Slots</button>
                </div>
            </form>
        </div>
        <div class="col-sm-8">
            <#if slotTemplates??>
                <div class="row">
                    <#list slotTemplates as template>
                        <div class="col-sm-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Slots for: ${template.getWhatFor()}</h4>
                                </div>
                                <div class="card-body">
                                    <p>Start Time: <strong>${template.getStartTime()}</strong></p>
                                    <p>Start Time: <strong>${template.getEndTime()}</strong></p>
                                    <p><strong>Weekdays</strong></p>
                                    <ul>
                                        <#list template.getSelectedWeekdays() as weekday>
                                            <li>${weekday}</li>
                                        </#list>
                                    </ul>
                                </div>
                                <div class="card-footer">
                                    <center>
                                        <ul class="list-inline">
                                            <li class="list-inline-item">
                                                <a href="/admin/register-slots/edit/${template.getId()}" class="btn btn-success">Edit</a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a href="/admin/register-slots/delete/${template.getId()}" class="btn btn-danger">Delete</a>
                                            </li>
                                        </ul>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </#list>
                </div>
            </#if>

        </div>
    </div>
</div>