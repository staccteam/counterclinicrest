<#include '../includes/simpleNavbar.ftl'>
<div class="container">
	<#include '../includes/greetUser.ftl'>
	<#include '../includes/message.ftl'>
	<div class="row">
		<div class="col-md-5"  id="show_clinic_section">
			<!-- Display Clinic Room Numbers -->
			<#include '../includes/showClinics.ftl'>
		</div>
		<div class="col-md-7" id="queue_stats_section">
			<!-- Display Real Time Queue Stats -->
			<#include '../includes/queueStats.ftl'>
		</div>
	</div>
	<hr/>
	<div class="row">
		<div class="col-md-5" id="add_clinic_room_form_section">
			<!-- Add New Clinic Rooms -->
			<#include '../includes/addClinicRoomForm.ftl'>
		</div>
	</div>
	<hr/>
	<div class="row">
		<div class="col-md-12" id="assign_clinic_to_doctor_section">
			<!-- Add clinic to doctors -->
			<#include '../includes/assignClinicToDoctor.ftl' >
		</div>
	</div>
	<hr/>
	<div class="row">
		<div class="col-md-3" id="register_user_form_section">
			<!-- User Registration -->
			<#include '../includes/registerUserForm.ftl'>
		</div>
		<div class="col-md-3" id="user_password_update_section">
			<!-- Update Password -->
			<#include '../includes/userLogin.ftl'>
		</div>
		<div class="col-md-6" id="user_cancel_registration_section">
			<!-- Cancel Registration --> 
			<#include '../includes/cancelRegistration.ftl'>
		</div>
		
	</div>
	<hr/>
</div>
<footer>
	<#include '../includes/footer.ftl'>
</footer>