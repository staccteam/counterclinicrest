<#include '../includes/simpleNavbar.ftl'>

<div class="container">
	<#include '../includes/greetUser.ftl'>
	<#include '../includes/message.ftl'>
	<div class="row">
		<div class="col-sm-12">
			<h2><#if thisUser??>${thisUser.getFirstName()}</#if>'s Dashboard</h2>
			<hr/>
		</div>
	</div>
	<#if isSuccess??>
		<#include "../includes/successMessage.ftl">
	</#if>
	
	<div class="row">
		<#if isOnBreak>
			<div class="col-md-12">
				<!-- Resume after a break screen -->
				<#include '../includes/resumeAfterBreak.ftl'>
			</div>
		<#else>
			<div class="col-md-4">
				<!--  Call Next Patient -->
				<#include '../includes/callNextPatient.ftl'>			
				<hr/>
				<!-- Go To Break Add Time Form -->
				<#include '../includes/goToBreak.ftl'>
			</div>
			<div class="col-md-4">
				<!-- Add Extra Time Form -->
				<!-- <#include '../includes/extendSession.ftl'> -->
				<!-- End Day Form -->
				<#include '../includes/endOfDay.ftl'>
				<hr />
				<#-- <#include '../includes/uploadFile.ftl'> -->
			</div>
			<div class="col-md-4">
				<!-- Queue Stats Table -->
				<#include '../includes/queueStatsTable.ftl'>
				<hr/>
				<!-- Add Average Time OR Start Auto Calculation -->
				<#include '../includes/addAvgTime.ftl'>
				<!-- <div class="card" style="width: 18rem;">
				  <img class="card-img-top" src="/application/images/profile.png" alt="Card image cap">
				  <div class="card-body">
				    <h5 class="card-title">Profile</h5>
				    <p class="card-text">Quickly Edit Your Basic Profile Info</p>
				    <a href="#" class="btn btn-primary">Edit Info</a>
				  </div>
				</div> -->
			</div>
		</#if>
		
	</div>
	
	
</div>