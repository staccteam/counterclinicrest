<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<div class="big_box">
				<div class="big_box__header">
					<h2>Add the approximate break time</h2>
					<p>(In Minutes)</p>
				</div>
				<div class="big_box__content">
					<form class="form" role="form" action="/dashboard/doctor/take-break" method="POST">
						<div class="form-group">
							<input class="form-control big_input" name="breakTime" id="break_time_input"></input>
						</div>
						<div class="form-group">
							<button class="btn btn-danger big_btn" type="submit">Add Time!</button>
						</div>
					</form>
				</div>
				<div class="big_box__footer"></div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
window.onload = function() {
	  document.getElementById("break_time_input").focus();
	};
</script>