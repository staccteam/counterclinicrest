<!-- Show Queue Stats -->
<!-- The Modal -->
<div class="modal fade" id="queue_stats_modal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Queue Stats</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        	<table class="table table-bordered">
        		<tr>
        			<th>Current Queue Statistics</th>
        		</tr>
        		<#if stats??>
        			<tr>
	        			<td>Queue Size</td>
	        			<td>${stats.queueSize}</td>
	        		</tr>
	        		<tr>
	        			<td>Current Patient Number</td>
	        			<td>${stats.currQueueCount}</td>
	        		</tr>
	        		<tr>
	        			<td>Approx. Total Checkup Time</td>
	        			<td>${stats.avgCheckUpTime * stats.queueSize} minutes</td>
	        		</tr>
	        	<#else>
	        		<td>There are no stats to show.</td>
        		</#if>
        	</table>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>