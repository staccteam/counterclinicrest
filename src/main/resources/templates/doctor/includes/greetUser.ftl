<#if thisUser??>
	<div class="row page-header">
		<div class="col-md-10">
			<h2>Hello, ${thisUser.getFirstName()} ${thisUser.getLastName()}</h2>
		</div>
		<div class="col-md-2">
			<a href="/dashboard/logout" class="btn btn-lg btn-success">Logout</a>
		</div>
	</div>
	<hr/>
</#if>