<div class="card">
	<div class="card-block">

		<h3 class="card-title">Upload Display Picture</h3>
		<div class="card-body">
			<div class="logo_preview">
				<img src="/global/images/${displayPicture}" class="img img-responsive" style="width: 300px;"/>
		</div>
		<form action="/doctor/upload-display-picture" method="POST" enctype="multipart/form-data">
			<div class="form-group">
				<input type="file" name="file" class="form-control" />
			</div>
			<div class="form-group">
				<button class="btn btn-primary" name="btnClk" type="submit">Upload</button>
			</div>
			<div class="form-group">
				<input class="btn btn-danger" value="Remove" name="btnClk" type="submit">
			</div>
		</form>
	</div>
</div>