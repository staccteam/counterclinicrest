<div class="card">
	<div class="card-body">
		<h5 class="card-title">Go to Break!</h5>
		<h6 class="card-subtitle mb-2 text-muted">In case you want to take a break, then use this form to add Break Time!</h6>
		<form action="/doctor/take-break" method="POST">
			<div class="form-group">
				<input class="form-control" name="breakTime" id="break_time_input"></input>
			</div>
			<div class="form-group">
				<button class="btn btn-default" type="submit">Add Time!</button>
			</div>
		</form>
	</div>
</div>