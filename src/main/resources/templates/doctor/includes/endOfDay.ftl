<div class="card">
  <div class="card-body">
    <h5 class="card-title">End Day</h5>
 			<h6 class="card-subtitle mb-2 text-muted">You have done your part. Tell your remaining patients to take appointment for some other day.</h6>
    <form class="form" action="/doctor/end-of-day" method="POST" id="end_of_day_form">
    	<div class="form-group">
    		<button class="btn btn-danger" type="submit">End Day's Work!</button>
    	</div>
    </form>
  </div>
</div>