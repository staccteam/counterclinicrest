<#if isSucess?? || isError??>
	<div class="row">
		<div class="col-sm-12">
			<#if isSuccess??>
				<div class="alert alert-success">
					${isSuccess}
				</div>
			</#if>
			<#if isError??>
				<div class="alert alert-danger">
					${isError}
				</div>
			</#if>
		</div>
	</div>
</#if>