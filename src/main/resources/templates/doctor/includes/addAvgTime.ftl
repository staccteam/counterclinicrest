<#if stats.getAverageTimeCalculationLogic() == 'MANUAL'>
	<#assign isEnabled="true">
</#if>
<div class="card-body">
	<h5 class="card-title">Add Average Time</h5>
	<h6 class="card-subtitle mb-2 text-muted">In case you want to explicitly mention the average time, otherwise, system will automatically calculate it.</h6>
	<form class="form" role="form" action="/doctor/add-avg-time" method="POST">
		<div class="form-group">
			<input type="number" class="form-control" name="avgTime" value="${stats.getAvgCheckupTime()!'0'}" id="avg_time_input" <#if !isEnabled??>disabled</#if>></input>
		</div>
		<div class="form-group">
			<input class="form-check-input" type="checkbox" name="switchAvgTimeCalc" id="switch_avg_time_checkbox" <#if !isEnabled??>checked</#if>/>
			<label class="form-label">Calculate Average Time Automatically</label>
		</div>
		<div class="form-group">
			<button class="btn btn-danger" id="avg_time_btn" type="submit" <#if !isEnabled??>disabled</#if>>Add Time!</button>
		</div>
	</form>
</div>