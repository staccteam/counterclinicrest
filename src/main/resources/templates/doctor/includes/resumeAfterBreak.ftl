<div class="card">
	<div class="card-block">
			<h2 class="card-title">You are on a break!</h2>
			<h6 class="card-subtitle mb-2 text-muted">Click on the resume button whenever you are ready.</h6>
			<form action="/doctor/resume-after-break" method="POST">
				<button class="btn btn-success" type="submit">Resume</button>
			</form>
		</div>
		<div class="card-body">
			<h4>Meanwhile, take a look at your stats.</h4>
			<#include './queueStatsTable.ftl'>
		</div>
	</div>
</div>


