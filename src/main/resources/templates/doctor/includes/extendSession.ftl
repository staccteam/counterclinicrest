<div class="card">
  <div class="card-body">
    <h5 class="card-title">Extend Session!</h5>
 	<h6 class="card-subtitle mb-2 text-muted">If it is taking more time than average then use this option to intimate other patients.</h6>
    <form class="form" action="/doctor/doctor-on-break" method="POST" id="extend_time_form">
    	<div class="form-group">
    		<label class="form-label">Extend Time:</label>
    		<input type="number" class="form-control" name="extendTime" id="extend_time_input" value="${(stats.pauseTime)!0}"/>
    	</div>
    	<div class="form-group">
    		<button class="btn btn-primary" type="submit">Extend Time!</button>
    	</div>
    </form>
  </div>
</div>