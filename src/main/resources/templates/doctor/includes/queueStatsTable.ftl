<#assign approxTotalCheckupTime = ((stats.getAvgCheckupTime() * (stats.getQueueSize() - stats.getCurrQueueCount())) / 1000)/60>
<#if  stats.getCurrQueueCount()  == 0>
	<#assign currPatientInTime = '--:--'>
<#else>
	<#assign currPatientInTime = stats.getPatientInTimeStamp()?number_to_time>
</#if>
<div class="queue-stats">
	<table class="table table-bordered">
      		<thead>
      			<tr>
      				<th>Current Queue </th>
      				<th>Statistics</th>
      			</tr>
      		</thead>
      			<tr>
       			<td><i class="fa fa-user" aria-hidden="true"></i> Queue Size</td>
       			<td><span id="queue_size">${stats.getQueueSize()}</span></td>
       		</tr>
       		<tr>
       			<td><i class="fa fa-hand-o-right" aria-hidden="true"></i> Current Patient Number</td>
       			<td><span id="curr_queue_count">${stats.getCurrQueueCount()}</span></td>
       		</tr>

   			<tr>
				<td><i class="fa fa-plus-square-o" aria-hidden="true"></i> Avg. Session Time (approx.)</td>
				<td><span id="avg_session_time">${(stats.getAvgCheckupTime()/1000)/60}</span> minutes</td>
			</tr>
       		<tr>
       			<td><i class="fa fa-plus-square-o" aria-hidden="true"></i> Approx. Total Checkup Time</td>
       			
       			<td><span id="total_checkup_time">${approxTotalCheckupTime}</span> minutes</td>
       			
       		</tr>
       		<tr>
       			<#if stats.getPatientInTimeStamp()??>
	       			<td><i class="fa fa-clock-o" aria-hidden="true"></i> Current Patient In-time</td>
	       			<td><span id="curr_patient_in_time">${currPatientInTime}</span></td>
	       		<#else>
	       			<td>Current Patient In-time</td>
	       			<td>--:--</td>
       			</#if>
       		</tr>
       		<tr>
       			<td><i class="fa fa-clock-o" aria-hidden="true"></i> Current Session Duration </td>
       			<td><span class="badge" id="session_counter">0</span></td>
       		</tr>
      	</table>
</div>