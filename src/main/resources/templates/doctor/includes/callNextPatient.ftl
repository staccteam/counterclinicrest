<div class="card">
  <div class="card-body">
    <h5 class="card-title">Next Patient</h5>
    <p class="card-text">Use this button once you are done checking with the current patient.</p>
    <form action="/doctor/call-next-patient"  id="call_next_patient_form" class="form" method="POST">
    	<input type="hidden" name="currSessId" value="<#if stats??>${stats.getId()}</#if>" />
    	<input type="hidden" name="currSessDuration" value="" id="curr_sess_duration" />
    	<button class="btn btn-success" type="submit" id="call_next_patient_btn">NEXT PATIENT</button>
    </form>
  </div>
</div>