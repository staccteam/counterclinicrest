<!-- add time modal -->
<!-- The Modal -->
<div class="modal fade" id="add_time_modal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Add the approximate break time</h4>
        <p>(In Minutes)</p>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        	<form class="form" role="form" action="/dashboard/doctor/take-break" method="POST">
				<div class="form-group">
					<input class="form-control big_input" name="breakTime" id="break_time_input"></input>
				</div>
				<div class="form-group">
					<button class="btn btn-danger big_btn" type="submit">Add Time!</button>
				</div>
			</form>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>