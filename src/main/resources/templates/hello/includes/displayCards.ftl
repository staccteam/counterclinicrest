<#if displayStats?has_content>
	<div class="row">
		<#list displayStats as stats>
			<#if stats.clinicNumber??>
				<#assign displayPicture = stats.displayPicture!''>
				<div class="col-md-4">
					<div class="card <#if stats?is_even_item>bg-light <#else> bg-light</#if>">
							<#if displayPicture?has_content>
								<img class="card-img-top" style="height: 300px;" src="/global/images/doctor/${displayPicture}" alt="Card image">
							</#if>
						 	<div class="card-body">
							    <h4 class="card-title">${stats.name?upper_case}</h4>
							    <table class="table table-responsive">
							    	<tr>
							    		<td>Clinic #</td>
							    		<td><strong><#if stats.clinicNumber??>${stats.clinicNumber}<#else>YET TO DECIDE</#if></strong></td>
							    	</tr>
							    	<tr>
							    		<td>Total Patients</td>
							    		<td><strong>${stats.queueSize}</strong></td> 
							    	</tr>
							    	<tr>
							    		<td>Current Number</td>
							    		<td><strong>${stats.currQueueCount}</strong></td>
							    	</tr>
								    <tr>
								    	<td>Status</td>
								    	<td><strong>${stats.doctorStatus}</strong></td>
								    </tr>
							    </table>
						  	</div>
					</div>
					<hr/>
				</div>
			</#if>
		</#list>	
	</div>
	
</#if>