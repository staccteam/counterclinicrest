<div class="row">
	<div class="col-md-12 col-sm-12">
		<table class="table table-condensed">
		<#if displayStats?has_content>
			<thead>
				<th>#</th>
				<th>Doctor's Name</th>
				<th>Clinic Number</th>
				<th># of People Waiting</th>
				<th>Current Patient's Count</th>
				<th>Doctor Status</th>
			</thead>
			<#list displayStats as stats>
			<tbody>
			<tr>
				<td><strong>${stats?counter}</strong></td>
				<td>${stats.name}</td>
				<td><#if stats.clinicNumber??>${stats.clinicNumber}<#else>YET TO DECIDE</#if></td>
				<td>${stats.queueSize}</td>
				<td>${stats.currQueueCount}</td>
				<td>${stats.doctorStatus}</td>
			</tr>
			</tbody>
			</#list>
		<#else>
			<span class="alert alert-warning">
				There are no records in the database.
			</span>
		</#if>
	</table>
	</div>
</div>