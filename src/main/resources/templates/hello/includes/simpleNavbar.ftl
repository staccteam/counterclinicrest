<!-- Image and text -->
<nav class="navbar navbar-light">
  <a class="navbar-brand" style="margin: 0px auto;" href="#">
    <img src="/global/images/logo/${siteLogo}" height="150" class="d-inline-block align-top" alt="">
    <br/>
    ${siteName!'Counter Clinic'}
  </a>
</nav>
<!-- A grey horizontal navbar that becomes vertical on small screens -->
<nav class="navbar navbar-expand-sm navbar-light" style="display: none;">
	<div class="container">
	    <ul class="navbar-nav">
	    	<li class="nav-item ${isHomePageActive!''}"><a class="nav-link" href="${contactUrl!'/'}">Display Board</a></li>
	    	<#if dashboardUrl??>
	    		<li class="nav-item ${isDashboardPageActive!''}"><a class="nav-link" href="${dashboardUrl}">Dashboard</a></li>
	    	</#if>
	      	<li class="nav-item ${isContactPageActive!''}"><a class="nav-link" href="${contactUrl!'/support'}">Support</a></li>
	      	<#if dashboardUrl??>
	    		<li class="nav-item"><a class="nav-link" href="${logoutUrl!'/dashboard/logout'}">Logout</a></li>
	    	</#if>
	    </ul>
	</div>
</nav>
