<#include '../includes/simpleNavbar.ftl'>



<div class="container" id="wrapper">

	<div class="row">
		<div class="col-md-12">
			<center>
				<h2 class="display-board-header">Clinic Information</h2>
				<p class="display-board-text"><i>Note: For any additional information, feel free to contact reception.</i></p>
				<hr/>
			</center>
		</div>
	</div>
	
	
	<#include '../includes/message.ftl'>	
	<#include '../includes/displayCards.ftl'>
	

</div>