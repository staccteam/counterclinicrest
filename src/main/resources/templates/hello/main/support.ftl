<#include '../includes/simpleNavbar.ftl'>

<style>
	.contact-info {
		font-size: x-large;
	}
</style>

<div class="container">
	<#include '../includes/message.ftl'>
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="card">
				<div class="card-body">
					<h2 class="card-title">Contact US!</h2>
					<h5 class="card-subtitle mb-2 text-muted">We are here to help you 24x7</h5>
				</div>
				<div class="card-footer">
					<div class="contact-info">
						<div class="form-group">
							<label class="form-label"><i class="fa fa-lg fa-envelope" aria-hidden="true"></i></label>
							<span id="contact_info_email_address"></span>
						</div>
						<div class="form-group">
							<label class="form-label"><i class="fa fa-lg fa-phone-square" aria-hidden="true"></i></label>
							<span id="contact_info_phone_number"></span>
						</div>
					</div>
				</div>
				
			</div>
		</div>
		
		<div class="col-md-2"></div>
	</div>
	
</div>