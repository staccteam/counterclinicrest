<#assign siteIcon = 'https://www.bemyaficionado.com/wp-content/uploads/2017/04/bma-temp-logo.png'/>
<div class="container">
    <div class="card card-container">
    	<#include '../../site/include/_alerts.ftl'/>
        <!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
        <#if siteLogo.isPresent()>
        <img id="profile-img" class="profile-img-card" src="${siteLogo.get().getVal()}" />
        <#else>
        <img id="profile-img" class="profile-img-card" src="${siteIcon}" />
        </#if>
        <!-- <img id="profile-img" class="profile-img-card" src="https://i0.wp.com/www.bemyaficionado.com/wp-content/uploads/2017/04/cropped-bma-temp-logo_64x64.png?fit=32%2C32&ssl=1" /> -->
        <p id="profile-name" class="profile-name-card"></p>
        <form class="form-signin" role="form" action="/login/user" method="POST">
            <span id="reauth-email" class="reauth-email"></span>
            <input type="text" name="username" id="inputEmail" class="form-control" placeholder="Username" required autofocus>
            <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
            <div id="remember" class="checkbox">
                <label>
                    <input type="checkbox" value="remember-me"> Remember me
                </label>
            </div>
            <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Sign in</button>
        </form><!-- /form -->
        <a href="/login/forgot-password" class="forgot-password">
            Forgot the password?
        </a>
    </div><!-- /card-container -->
</div><!-- /container -->
