<div class="container">
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					<h4>Contact Admin</h4>
				</div>
				<div class="card-body">
				<div class="contact-info">
					<div class="form-group">
						<label class="form-label"><i class="fa fa-lg fa-envelope" aria-hidden="true"></i></label>
						<span id="contact_info_email_address" class="contact_info_email_address"></span>
					</div>
					<div class="form-group">
						<label class="form-label"><i class="fa fa-lg fa-phone-square" aria-hidden="true"></i></label>
						<span id="contact_info_phone_number" class="contact_info_phone_number">+9 982798376</span>
					</div>
				</div>
				</div>
				<div class="card-footer">
					<span class="alert alert-warning">
						In case the above contact does not work, you may need to contact admin personally!
					</span>
				</div>
			</div>
		</div>
		<div class="col-md-2"></div>
	</div>
</div>