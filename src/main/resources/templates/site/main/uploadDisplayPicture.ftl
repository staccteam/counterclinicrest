<#if userRole == "DOCTOR">
<div class="container-fluid" id="wrapper">
    <div class="row">
        <#include '../include/_navbar.ftl'/>
        <main class="col-xs-12 col-sm-8 col-lg-9 col-xl-10 pt-3 pl-4 ml-auto">
            <#include '../include/_header.ftl'/>
            <#include '../include/_alerts.ftl'/>
            <section class="row">
                <div class="col-md-3"></div>
                <div class="col-sm-12 col-md-6">
                    <#include '../../doctor/includes/uploadFile.ftl' />
                </div>
                <div class="col-md-3"></div>
            </section>
        </main>
    </div>
</div>
<#elseif userRole == "SUPER_ADMIN">
<div class="container-fluid" id="wrapper">
    <div class="row">
        <#include '../include/_navbar.ftl'/>
        <main class="col-xs-12 col-sm-8 col-lg-9 col-xl-10 pt-3 pl-4 ml-auto">
            <#include '../include/_header.ftl'/>
            <#include '../include/_alerts.ftl'/>
            <section class="row">
                <div class="col-sm-12 col-md-6">
                    <#include '../../superadmin/includes/uploadFile.ftl' />
                </div>
                <div class="col-sm-12 col-md-6">
                    <#include '../../superadmin/includes/uploadAdPic.ftl' />
                </div>
            </section>
        </main>
    </div>
</div>

<#else>
<div class="alert-warn">
        <span>
            You are not authorized!
        </span>
</div>
</#if>