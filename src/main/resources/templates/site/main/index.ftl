<div class="container-fluid" id="wrapper">
    <div class="row">
        <#include '../include/_navbar.ftl'/>
        <main class="col-xs-12 col-sm-8 col-lg-9 col-xl-10 pt-3 pl-4 ml-auto">
            <#include '../include/_header.ftl'/>
            <#include '../include/_alerts.ftl'/>


            <#if userRole  == 'ADMIN' || userRole == 'SUPER_ADMIN'>
            <!-- List of Clinics -->
            <section class="row">
                <div class="col-sm-6">
                    <section class="row">
                        <div class="card">
                            <div class="card-block">
                                <div class="card-title">
                                    <h3>List of available Clinics</h3>
                                </div>
                                <#include '../../admin/includes/listOfClinics.ftl'/>
                            </div>
                        </div>
                    </section>
                </div>
            </section>
            <div class="divider"></div>
            <!-- Doctor's Queue Stats -->
            <section id="doctor_queue_stats" class="row">
                <div class="col-lg-12 col-md-12">
                    <#include '../../admin/includes/queueStats.ftl'/>
                </div>
            </section>
            <div class="divider"></div>

            <!-- Appointment slots -->
            <section class="row" id="appointment_slots">
                <#include '../../admin/includes/appointmentSlots.ftl'/>
            </section>
            </#if>

            <#if userRole == 'DOCTOR' >
            <section class="row">
                <!-- call next patient form -->
                <div class="col-sm-12 col-md-4">
                    <#include '../../doctor/includes/callNextPatient.ftl'/>
                    <div class="divider"></div>
                    <#include '../../doctor/includes/goToBreak.ftl'  />
                </div>
                <!-- end of day form -->
                <div class="col-sm-12 col-md-4">
                    <#include '../../doctor/includes/endOfDay.ftl'/>
                    <div class="divider"></div>
                    <#include '../../doctor/includes/addAvgTime.ftl'/>
                </div>
                <!-- doctor queue stats -->
                <div class="col-sm-12 col-md-4">
                    <#include '../../doctor/includes/queueStatsTable.ftl'/>
                </div>
            </section>
            </#if>

            <div class="divider"></div>

            <#if userRole == 'SUPER_ADMIN' >
            <section class="row" id="list_of_admins">
                <div class="col-sm-12 col-md-6">
                    <#include '../../superadmin/includes/listOfAdmins.ftl'/>
                </div>
            </section>
            <section class="row" id="site_options">
                <div class="col-sm-12 col-md-6">
                    <#include '../../superadmin/includes/siteOptions.ftl'/>
                </div>
            </section>
            </#if>
        </main>
    </div>
</div>
