<#if userRole == "DOCTOR">
<div class="container-fluid" id="wrapper">
    <div class="row">
        <#include '../include/_navbar.ftl'/>
        <main class="col-xs-12 col-sm-8 col-lg-9 col-xl-10 pt-3 pl-4 ml-auto">
            <#include '../include/_header.ftl'/>
            <#include '../include/_alerts.ftl'/>
            <section class="row">
                <div class="col-md-3"></div>
                <div class="col-sm-12 col-md-6">
                    <div class="card">
                        <div class="card-block">
                            <div class="card-title">Online Appointments</div>
                            <ul class="timeline">
                                <#list onlineAppointments?keys as appointmentDate>
                                <li>
                                    <div class="timeline-badge">
                                        <em class="fa fa-address-card-o"></em>
                                    </div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h5 class="timeline-title mt-2">
                                                ${appointmentDate}
                                            </h5>
                                        </div>
                                        <div class="timeline-body">
                                            <#list onlineAppointments[appointmentDate] as onlineAppointment>
                                                <p>Patient Name: ${onlineAppointment.getPatientName()}</p>
                                                <p>Appointment Date: ${onlineAppointment.getAppointmentDate()}</p>
                                                <p>Appointment Time: ${onlineAppointment.getAppointmentTime()}</p>
                                            <div class="divider"></div>
                                            </#list>
                                        </div>
                                    </div>
                                </li>

                                </#list>
                            </ul>
                        </div>
                    </div>


                </div>
                <div class="col-md-3"></div>
            </section>
        </main>
    </div>
</div>
<#else>
<div class="alert-warn">
        <span>
            You are not authorized!
        </span>
</div>
</#if>