<div class="container-fluid" id="wrapper">
    <div class="row">
        <#include '../include/_navbar.ftl'/>
        <#include '../include/_variables.ftl'/>
        <main class="col-xs-12 col-sm-8 col-lg-9 col-xl-10 pt-3 pl-4 ml-auto">
            <div class="row">
                <#if doctors?has_content>
            <#list doctors as user>
                <div class="card col-md-4">
                    <div class="card-block">
                        <h3 class="card-title">${user.getFirstName()} ${user.getLastName()}</h3>
                        <#if user.getDisplayImageName()??>
                            <img src="/global/images/${user.getDisplayImageName()}" class="img img-responsive" style="width: 250px;"/>
                        <#else>
                            <img src="/global/images/${displayPicture}" class="img img-responsive" style="width: 250px;"/>
                        </#if>

                        <#if user.getClinic()??>
                        <div class="row">
                            <div class="col-sm-6">Clinic #</div>
                            <div class="col-sm-6">${user.getClinic().getClinicNumber()}</div>
                        </div>
                        </#if>

                        <div class="divider"></div>
                        <div class="row">
                            <div class="col-sm-6">Patients In-Queue</div>
                            <div class="col-sm-6">${user.getAppointmentQueue().getQueueSize()}</div>
                        </div>
                        <div class="divider"></div>
                        <div class="row">
                            <div class="col-sm-6">Current Patient</div>
                            <div class="col-sm-6">${user.getAppointmentQueue().getCurrQueueCount()}</div>
                        </div>
                        <div class="divider"></div>
                        <div class="row">
                            <div class="col-sm-6">Doctor Status</div>
                            <div class="col-sm-6">${user.getAppointmentQueue().getDoctorStatus()}</div>
                        </div>
                    </div>
                </div>
            </#list>
            </#if>
            </div>
        </main>
    </div>
</div>
