<#if userRole == "ADMIN" || userRole == "SUPER_ADMIN">
<div class="container-fluid" id="wrapper">
    <div class="row">
        <#include '../include/_navbar.ftl'/>
        <main class="col-xs-12 col-sm-8 col-lg-9 col-xl-10 pt-3 pl-4 ml-auto">
            <#include '../include/_header.ftl'/>
            <#include '../include/_alerts.ftl'/>
            <div class="card">
                <div class="card-block">
                    <h3 class="card-title">Create Appointment Slot</h3>
                    <#include '../../admin/includes/createAppointmentTimeSlotForm.ftl' />
                </div>
            </div>
        </main>
    </div>
</div>
<#else>
    <div class="alert-warn">
        <span>
            You are not authorized!
        </span>
    </div>
</#if>