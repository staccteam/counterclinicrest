<#if userRole == "DOCTOR">
<div class="container-fluid" id="wrapper">
    <div class="row">
        <#include '../include/_navbar.ftl'/>
        <main class="col-xs-12 col-sm-8 col-lg-9 col-xl-10 pt-3 pl-4 ml-auto">
            <#include '../include/_header.ftl'/>
            <section class="row">
                <div class="col-md-3"></div>
                <div class="col-sm-12 col-md-6">
                    <#include '../include/_alerts.ftl'/>
                    <div class="divider"></div>
                    <#include '../../doctor/includes/resumeAfterBreak.ftl' />
                </div>
                <div class="col-md-3"></div>
            </section>
        </main>
    </div>
</div>
<#else>
<div class="alert-warn">
        <span>
            You are not authorized!
        </span>
</div>
</#if>