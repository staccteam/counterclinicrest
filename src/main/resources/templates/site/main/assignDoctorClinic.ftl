<div class="container-fluid" id="wrapper">
    <div class="row">
        <#include '../include/_navbar.ftl'/>
        <main class="col-xs-12 col-sm-8 col-lg-9 col-xl-10 pt-3 pl-4 ml-auto">
            <#include '../include/_header.ftl'/>
            <#include '../include/_alerts.ftl'/>
            <#include '../../admin/includes/assignClinicToDoctor.ftl' />
        </main>
    </div>
</div>