<!--  Navbar Conditions -->
<#if isDashboardPageActive??>
<#assign isDashboardPageActive = "active"/>
<#else>
<#assign isDashboardPageActive = "" />
</#if>

<#if isHomePageActive??>
<#assign isHomePageActive = "active"/>
<#else>
<#assign isHomePageActive = "" />
</#if>

<#if isAddNewUserPageActive??>
<#assign isAddNewUserPageActive = "active"/>
<#else>
<#assign isAddNewUserPageActive = "" />
</#if>

<#if isAddNewClinicPageActive??>
<#assign isAddNewClinicPageActive = "active"/>
<#else>
<#assign isAddNewClinicPageActive = "" />
</#if>

<#if isContactPageActive??>
<#assign isContactPageActive = "active" />
<#else>
<#assign isContactPageActive = "" />
</#if>

<#if isSupportPageActive??>
<#assign isSupportPageActive = "active" />
<#else>
<#assign isSupportPageActive = "" />
</#if>

<#if isOnlineAppointmentPageActive??>
<#assign isOnlineAppointmentPageActive = "active" />
<#else>
<#assign isOnlineAppointmentPageActive = "" />
</#if>

<#if isOnlineAppointmentsPageActive??>
<#assign isOnlineAppointmentsPageActive = "active" />
<#else>
<#assign isOnlineAppointmentsPageActive = "" />
</#if>

<#if isAssignClinicPageActive??>
<#assign isAssignClinicPageActive = "active" />
<#else>
<#assign isAssignClinicPageActive = "" />
</#if>

<#if isUpdatePasswordPageActive??>
<#assign isUpdatePasswordPageActive = "active" />
<#else>
<#assign isUpdatePasswordPageActive = "" />
</#if>

<#if isUploadDisplayPictureActive??>
<#assign isUploadDisplayPictureActive = "active" />
<#else>
<#assign isUploadDisplayPictureActive = "" />
</#if>

<#if isRegisterAdminPageActive??>
<#assign isRegisterAdminPageActive = "active" />
<#else>
<#assign isRegisterAdminPageActive = "" />
</#if>

<#if isCustomOptionPageActive??>
<#assign isCustomOptionPageActive = "active" />
<#else>
<#assign isCustomOptionPageActive = "" />
</#if>

<#if isSiteTimeZonePageActive??>
<#assign isSiteTimeZonePageActive = "active" />
<#else>
<#assign isSiteTimeZonePageActive = "" />
</#if>

<#if isUploadLogoPageActive??>
<#assign isUploadLogoPageActive = "active" />
<#else>
<#assign isUploadLogoPageActive = "" />
</#if>

<!-- Alert Conditions -->
<#if alertSuccess??>
    <!-- alert success is not null -->
<#else>
    <#assign alertSuccess = false />
    <#assign alertSuccessMsg = false />
</#if>

<#if alertDanger??>
<!-- alert danger is not null -->
<#else>
<#assign alertDanger = false />
<#assign alertDangerMsg = false />
</#if>