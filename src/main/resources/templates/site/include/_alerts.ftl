<#if alertSuccess == true>
<div class="alert-success">
	<span>
		${alertSuccessMsg}
	</span>
</div>
<#elseif alertDanger == true>
<div class="alert-danger">
	<span>
		${alertDangerMsg}
	</span>
</div>
</#if>