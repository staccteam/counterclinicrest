<#if thisUser??>
<#assign userRole = thisUser.getRoles()[0].getName() />
<#assign username = thisUser.getUsername()  />
<#assign displayPicture = thisUser.getDisplayImageName()!'profile.png'  />

<#else>
<#assign userRole = "" />
<#assign username = ""  />
<#assign displayPicture = "profile.png"  />
</#if>

<#setting time_zone = siteTimezone!'Asia/Kuwait'>
