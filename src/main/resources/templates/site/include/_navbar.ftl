<#if userRole == "ADMIN">
<nav class="sidebar col-xs-12 col-sm-4 col-lg-3 col-xl-2">
    <h1 class="site-title"><a href="#"><!-- <em class="fa fa-rocket"></em> --> ${siteName}</a></h1>

    <a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><em class="fa fa-bars"></em></a>
    <ul class="nav nav-pills flex-column sidebar-nav">
        <li class="nav-item"><a class="nav-link ${isDashboardPageActive}" href="/admin"><em class="fa fa-dashboard"></em> Dashboard <span class="sr-only">(current)</span></a></li>
        <li class="nav-item"><a class="nav-link ${isAddNewUserPageActive}" href="/admin/add-new-user"><em class="fa fa-plus"></em> Add New User</a></li>
        <li class="nav-item"><a class="nav-link ${isAddNewClinicPageActive}" href="/admin/add-new-clinic"><em class="fa fa-pagelines"></em> Add New Clinic</a></li>
        <li class="nav-item"><a class="nav-link ${isAssignClinicPageActive}" href="/admin/assign-clinic"><em class="fa fa-pencil-square-o"></em> Assign Clinic</a></li>
        <li class="nav-item"><a class="nav-link ${isUpdatePasswordPageActive}" href="/admin/update-user-password"><em class="fa fa-hand-o-up"></em> Update User Password</a></li>
        <li class="nav-item"><a class="nav-link ${isOnlineAppointmentPageActive}" href="/admin/create-new-appointment-timeslot"><em class="fa fa-clone"></em>Create Appointment Slots</a></li>
    </ul>
    <a href="/dashboard/logout" class="logout-button"><em class="fa fa-power-off"></em> Signout</a>
</nav>

<#elseif userRole == "DOCTOR">
<nav class="sidebar col-xs-12 col-sm-4 col-lg-3 col-xl-2">
    <h1 class="site-title"><a href="#"><!-- <em class="fa fa-rocket"></em> --> ${siteName}</a></h1>

    <a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><em class="fa fa-bars"></em></a>
    <ul class="nav nav-pills flex-column sidebar-nav">
        <li class="nav-item"><a class="nav-link ${isDashboardPageActive}" href="/doctor"><em class="fa fa-dashboard"></em> Dashboard <span class="sr-only">(current)</span></a></li>
        <li class="nav-item"><a class="nav-link ${isUploadDisplayPictureActive}" href="/doctor/upload-display-picture"><em class="fa fa-picture-o"></em> Upload Display Picture</a></li>
        <li class="nav-item"><a class="nav-link ${isOnlineAppointmentsPageActive}" href="/doctor/show-online-appointments"><em class="fa fa-picture-o"></em> Show Online Appointments</a></li>
    </ul>
    <a href="/dashboard/logout" class="logout-button"><em class="fa fa-power-off"></em> Signout</a>
</nav>

<#elseif userRole == "SUPER_ADMIN">
<nav class="sidebar col-xs-12 col-sm-4 col-lg-3 col-xl-2">
    <h1 class="site-title"><a href="#"><!-- <em class="fa fa-rocket"></em> --> ${siteName}</a></h1>

    <a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><em class="fa fa-bars"></em></a>
    <ul class="nav nav-pills flex-column sidebar-nav">
        <li class="nav-item"><a class="nav-link ${isDashboardPageActive}" href="/superadmin"><em class="fa fa-dashboard"></em> Dashboard <span class="sr-only">(current)</span></a></li>
        <li class="nav-item"><a class="nav-link ${isRegisterAdminPageActive}" href="/superadmin/register/admin"><em class="fa fa-user-plus"></em> Register Admin <span class="sr-only">(current)</span></a></li>
        <li class="nav-item"><a class="nav-link ${isCustomOptionPageActive}" href="/superadmin/add-option"><em class="fa fa-filter"></em>  Add Custom Option  <span class="sr-only">(current)</span></a></li>
        <li class="nav-item"><a class="nav-link ${isSiteTimeZonePageActive}" href="/superadmin/add-timezone"><em class="fa fa-clock-o"></em>  Add Site TimeZone <span class="sr-only">(current)</span></a></li>
        <li class="nav-item"><a class="nav-link ${isUploadLogoPageActive}" href="/superadmin/upload-file"><em class="fa fa-clock-o"></em>  Upload Site Logo <span class="sr-only">(current)</span></a></li>
    </ul>
    <a href="/dashboard/logout" class="logout-button"><em class="fa fa-power-off"></em> Signout</a>
</nav>
<#else>


</#if>