<div class="card">
	<div class="card-header">
		<h4>Assign Clinics to Doctors</h4>
	</div>
	<div class="card-body">
		<form action="/reception/assign-clinic-doctor" method="POST">
			<span>Assign Clinic: &nbsp;</span>
			<#if clinics?has_content>
				<select name="clinicId" style="width: 30%;">
					<option disabled selected>select...</option>
					<#list clinics as clinic>
					<option value="${clinic.getId()}">${clinic.getClinicNumber()}</option>
					</#list>
				</select>
			<#else>
				<span class="alert alert-warning">
					There are no records in the database.
				</span>
			</#if>
			<!-- Doctors select box -->
			<span> to doctor: &nbsp;</span>
			<#if users?has_content>
				<select name="doctorId" style="width: 30%;">
					<option disabled selected>select...</option>
					<#list users as user>
						<option value="${user.getId()}">${user.getFirstName()} ${user.getLastName()}</option>
					</#list>
				</select>
			<#else>
				<span class="alert alert-warning">
					There are no records in the database.
				</span>
			</#if>
			<button class="btn btn-sm btn-default">Assign</button>
		</form>
	</div>
</div>