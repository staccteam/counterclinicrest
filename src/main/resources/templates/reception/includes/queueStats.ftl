<div class="card" id="queue_stats">
	<div class="card-header">
		<h4>Counter Clinic Information</h4>
	</div>
	<div class="card-body">
		<form action="/reception/init-empty-queue" method="POST">
			<div class="form-group">
				<button class="btn btn-warning" type="submit">Initialize <i class="fa fa-lg fa-refresh" aria-hidden="true"></i></button>
			</div>
		</form>
		<form action="/reception/reset-all-queue" method="POST">
			<div class="form-group">
				<button class="btn btn-primary" type="submit">Reset Queue <i class="fa fa-lg fa-refresh" aria-hidden="true"></i></button>
			</div>
		</form>
		<table class="table table-condensed">
			<#if displayStats?has_content>
				<thead>
					<th>#</th>
					<th>Doctor's Name</th>
					<th>Clinic Number</th>
					<th># of People Waiting</th>
					<th>Current Patient's Count</th>
					<th>Doctor Status</th>
				</thead>
				<#list displayStats as stats>
				<tbody>
				<tr>
					<td><strong>${stats?counter}</strong></td>
					<#list users as user>
					    <#if user.getId() == stats.getDoctorId()>
					        <td>${user.getFirstName()} ${user.getLastName()}</td>
					        <td><#if user.getClinic()??>${user.getClinic().getClinicNumber()}<#else>NOT ASSIGNED</#if></td>
					    </#if>
					</#list>
					<td><#if stats.getQueueSize()??>${stats.getQueueSize()}<#else>0</#if></td>
					<td>${stats.getCurrQueueCount()}</td>
					<td>
						<form action="/api/changeDoctorStatus" method="POST" id="doctor_status_form" class="doctor-status-form">
							<input type="hidden" value="${stats.getDoctorId()}" name="doctorId" />
							<input type="text" id="doctor_status_input" class="form-control" name="doctorStatus" value="${stats.getDoctorStatus()!'Not Set!'}" />
						</form>
					</td>
					<td>
						<form action="/reception/init-queue-for-doctor" method="POST">
							<input type="hidden" value="${stats.getDoctorId()}" name="doctorId" />
							<button class="btn btn-primary" type="submit">Reset Queue <i class="fa fa-lg fa-refresh" aria-hidden="true"></i></button>
						</form>
					</td>
				</tr>
				</tbody>
				</#list>
			<#else>
				<span class="alert alert-warning">
					There are no records in the database.
				</span>
			</#if>
		</table>
	</div>
	
</div>
