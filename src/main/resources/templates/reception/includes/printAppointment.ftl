<style>

table {
	font-size: xx-large;
	font-weight: bold;
	text-align: center;
}
</style>
<div class="card">
	<img src="${appointment.getQrCodeImageUrl()}"
		 class="card-img-top" />
		<table class="table print-format">
			<thead>
				<th>Appointment Details</th>
			</thead>
			<tbody>
				<tr>
					<td><label  class="form-label">Appointment Id</label></td>
					<td>${appointment.getId() }</td>
				</tr>
				<tr>
					<td><label class="form-label">Patient's Name:</label></td>
					<td>${appointment.getPatientName()}</td>
				</tr>
				<tr>
					<td><label class="form-label">Doctor's Name</label></td>
					<td>${appointment.getDoctorName()}</td>
				</tr>
				<tr>
					<td><label class="form-label">Clinic Number:</label></td>
					<td>
					    <#if appointment.getClinic()??>
                            ${appointment.getClinic().getClinicNumber()}
                        <#else>
                            Not Assigned
                        </#if>
					</td>
				</tr>
				<tr>
					<td><label class="form-label">Your Number:</label></td>
					<td>${appointment.getAppointmentNumber()}</td>
				</tr>
				<tr>
					<td><label class="form-label">Appointment Date:</label></td>
					<td>${appointment.getAppointmentTimeStamp()?number_to_datetime}</td>
				</tr>
			</tbody>
		</table>
</div>
