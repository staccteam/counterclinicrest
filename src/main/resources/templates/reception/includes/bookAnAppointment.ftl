<div class="card">
	<div class="card-header">
		<h4>Book an Appointment</h4>
	</div>
	<div class="card-body">
		<form class="form" action="/reception/make-appointment" method="POST">

			<div class="form-group">
				<label class="form-label">Patient's Name</label>
				<input class="form-control" type="text" name="patientName" autocomplete="off" />
				<input type="hidden" name="appointmentType" value="WALK_IN"  />
			</div>
			<!-- Multi Tab for Appointment Options -->
			<div class="form-group">
				<div class="form-group">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
					  <li class="nav-item">
						<a class="nav-link active" id="doctor-tab" data-toggle="tab" href="#doctors_tab" role="tab" aria-controls="doctor" aria-selected="true">Appointment by Doctor</a>
					  </li>
					  <li class="nav-item">
						<a class="nav-link" id="clinics-tab" data-toggle="tab" href="#clinics_tab" role="tab" aria-controls="clinics" aria-selected="false">Appointment by Clinics</a>
					  </li>
					</ul>
					<!-- Doctors List -->
					<div class="tab-content" id="appointment_tab_content">
					  <div class="tab-pane fade show active" id="doctors_tab" role="tabpanel" aria-labelledby="home-tab">
							<div class="form-group">
								<#if users?has_content>
								<select name="doctorId" class="form-control">
								<!-- Iterate through list of doctors and form select -->
									<option disabled selected>Select Doctor...</option>
									<#list users as user>
									    <option value="${user.getId()}">${user.getFirstName()} ${user.getLastName()}</option>
									</#list>
								</select>
								</#if>
							</div>
					  </div>
					  <!-- Clinic Room Numbers -->
					  <div class="tab-pane fade" id="clinics_tab" role="tabpanel" aria-labelledby="clinics-tab">
							<div class="form-group">
								<#if clinics?has_content>
									<select name="clinicId" class="form-control">
										<option disabled selected>Select Clinic...</option>
										<#list clinics as clinic>
										<option value="${clinic.id}">${clinic.clinicNumber}</option>
										</#list>
									</select>
								<#else>
									<span class="alert alert-warning">
										There are no records in the database.
									</span>
								</#if>
							</div>
					  </div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<button class="btn btn-default">Make Appointment</button>
			</div>
		</form>
	</div>
</div>