<!-- Image and text -->
<nav class="navbar navbar-light bg-light">
  <a class="navbar-brand" style="margin: 0px auto;" href="#">
    <img src="/global/images/logo/${siteLogo!''}" height="150" class="d-inline-block align-top" alt="">
    <br/>
    ${siteName!'Counter Clinic'}
  </a>
</nav>
<!-- A grey horizontal navbar that becomes vertical on small screens -->
<nav class="navbar navbar-expand-sm navbar-light ">
	<div class="container">
	    <ul class="navbar-nav">
	    	<li class="nav-item ${isHomePageActive!''}"><a class="nav-link" href="${contactUrl!'/'}">Display Board</a></li>
	      	<li class="nav-item ${isDashboardPageActive!''}"><a class="nav-link" href="${dashboardUrl!'/reception'}">Dashboard</a></li>
	      	<li class="nav-item ${isContactPageActive!''}"><a class="nav-link" href="${contactUrl!'/support'}">Support</a></li>
			<li class="nav-item"><a class="nav-link" href="${logoutUrl!'/dashboard/logout'}">Logout</a></li>
	    </ul>
	</div>
</nav>
