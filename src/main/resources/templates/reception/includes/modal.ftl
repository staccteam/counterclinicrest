<div class="modal" tabindex="-1" role="dialog" id="${modalId}">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">${modalTitle}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ${modalBody}
      </div>
      <div class="modal-footer">
        <#if modalBtnText?has_content>
        	<button type="button" class="btn btn-primary">${modalBtnText}</button>
        </#if>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>