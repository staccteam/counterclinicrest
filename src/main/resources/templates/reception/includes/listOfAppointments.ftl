<div class="card" id="list_of_appointments">
	<div class="card-header">
		<h4>List of All Appointments</h4>
	</div>
	<div class="card-body">
		<form class="form" action="/reception/delete-all-appointments" method="POST">
			<div class="form-group">
				<input type="submit" class="btn btn-danger" value="Delete All Appointments" />
			</div>
		</form>
		<#if appointments?has_content>
			<table class="table table-bordered table-responsive">
				<thead>
					<th>#</th>
					<th>Patient's Name</th>
					<th>Appointed Doctor</th>
					<th>Clinic Number</th>
					<th>Time of Appointment</th>
					<th>QR Code</th>
				</thead>
				<tbody>
				<#list appointments as a>
					<tr>
						<td>${a?counter}</td>
						<td>${a.patientName}</td>
						<td>${a.doctorName}</td>
						<td>${a.clinicRoomNumber!'Not Assigned'}</td>
						<td>${a.appointmentTimeStamp?number_to_datetime}</td>
						<td>
							<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_id_${a?counter}">
								Show QR Code
							</button>
							<#assign modalId="modal_id_${a?counter}">
							<#assign modalTitle="Patient: ${a.patientName}">
							<#assign modalBody="<center><img style='height:300px;' src='${a.getQrCodeImageUrl()}' class='img img-responsive' /></center>">
							<#include '../includes/modal.ftl'>
						</td>
						<td>
							<a class="btn btn-default" href="/reception/getAppointmentInfo/${a.getId()}">View Appointment</a>
						</td>
						<td>
							<form action="/reception/delete-appointment" method="POST">
								<input type="hidden" value="${a.getId()}" name="appointmentId" />
								<button type="submit" class="btn btn-danger">Delete</button>
							</form>
						</td>
					</tr>
				</#list>
				</tbody>
			</table>
		<#else>
			<span class="alert alert-warning">
				There are no records in the database.
			</span>
		</#if>
		</div>
	</div>
</div>