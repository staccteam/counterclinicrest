<#include '../includes/simpleNavbar.ftl'>
<div class="container">
    <#include '../includes/greetUser.ftl'>
    <#include '../includes/message.ftl'>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="card">
                <div class="card-header">
                    <h2>Online Appointments</h2>
                </div>
                <div class="card-body">

                    <#if appointments??>
                    <#list appointments as event>
                        <div class="media">
                            <div class="media-body">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>${event.getSummary()}</h5>
                                    </div>
                                    <div class="card-body">
                                        <p>Patient Name: ${event.getPatient().getFirstName()} ${event.getPatient().getLastName() }</p>
                                        <p>Appointed Doctor: Dr. ${event.getDoctor().getFirstName()} ${event.getDoctor().getLastName()} </p>
                                        <p>Appointment Date: ${event.getStartTime()?number_to_date}</p>
                                        <p>Appointment Time: ${event.getStartTime()?number_to_time}</p>
                                    </div>
                                    <div class="card-footer">
                                        <ul class="list-inline">
                                            <li class="list-inline-item">
                                                <a href="edit/appointments/${event.getPatient().getCalendar().getCalendarId()}/${event.getEventId()}" class="btn btn-success">Edit</a>
                                            </li>
                                            <li class="list-inline-item">
                                                <form action="appointments/delete" method="post">
                                                    <input type="hidden" value="${event.getPatient().getCalendar().getCalendarId()}" class="hidden"/>
                                                    <input type="hidden" value="${event.getEventId()}" class="hidden"/>
                                                    <button class="btn btn-danger" type="submit">Delete</button>
                                                </form>
                                            </li>
                                        </ul>
                                    </div>
                                </div>


                            </div>
                        </div>
                    <hr/>
                    </#list>
                    </#if>
                </div>
            </div>
        </div>
    </div>
</div>


