<#include '../includes/simpleNavbar.ftl'>
<div class="container">
    <#include '../includes/greetUser.ftl'>
    <#include '../includes/message.ftl'>

    <div class="row">
        <div class="col-sm-3"></div>
        <div class="col-sm-6">
            <div class="card">
                <div class="card-header"><h3>Edit Appointment</h3></div>
                <div class="card-body">
                    <form action="/reception/edit/appointments" method="post">
                        <input type="hidden" name="calendarId" value="${event.getPatient().getCalendar().getCalendarId()}" class="hidden">
                        <input type="hidden" name="eventId" value="${event.getEventId()}" class="hidden">
                        <div class="form-group">
                            <label for="summary">Appointment Summary</label>
                            <input type="text" name="summary" class="form-control" value="${event.getSummary()}"/>
                        </div>
                        <div class="form-group">
                            <label for="startTime">Appointment Date and Time</label>
                            <!--<input type="text" name="startTime" class="form-control" value="${slots.getStartTime()?number_to_date}" />-->
                            <input class="form-control" type="datetime-local" value="${event.getStartTime()?number_to_date?iso_utc_nz}T${event.getStartTime()?number_to_time?iso_utc_nz}" name="startDateTime" />
                        </div>
                        <div class="form-group">
                            <label for="patientEmail">Patient Email</label>
                            <input class="form-control" type="email" name="patientEmail" value="${event.getPatient().getEmail()}">
                        </div>
                        <div class="form-group">
                            <label for="doctorEmail">Appointed Doctor</label>
                            <#if doctors??>
                            <select name="doctorEmail" id="doctors" class="form-control">
                                <#list doctors as doctor>
                                <#if doctor.getEmail() == event.getDoctor().getEmail() >
                                    <option value="${doctor.email}" selected="true">Dr. ${doctor.getFirstName()} ${doctor.getLastName()}</option>
                                <#else>
                                    <option value="${doctor.email}">Dr. ${doctor.getFirstName()} ${doctor.getLastName()}</option>
                                </#if>
                                </#list>
                            </select>
                            </#if>
                        </div>
                        <div class="form-group">
                            <button class="btn-primary btn" type="submit">Modify Appointment</button>
                        </div>
                    </form>
                </div>
                <div class="card-footer"></div>
            </div>
        </div>
        <div class="col-sm-3"></div>
    </div>
</div>