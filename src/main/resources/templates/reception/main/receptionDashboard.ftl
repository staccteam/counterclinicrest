<#include '../../site/include/_variables.ftl'/>

<#if userRole == 'RECEPTION'>
<#include '../includes/simpleNavbar.ftl'>

<div class="container">
	<#include '../includes/greetUser.ftl'>
	<#include '../includes/message.ftl'>

	<div class="row">
		<div class="col-sm-12">
			<!-- Make Appointments for Doctors -->
			<#include '../includes/bookAnAppointment.ftl'>
			<hr/>
		</div>
		<div class="col-sm-12">
			<!-- Queue Stats -->
			<#include '../includes/queueStats.ftl'>
			<hr/>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<#include '../includes/assignClinicToDoctor.ftl'>
			<hr/>
		</div>

	</div>
	<div class="row">
		<div class="col-md-12">
			<!-- List of all appointments -->
			<#include '../includes/listOfAppointments.ftl'>
			<hr/>
		</div>
	</div>
</div>
<#else>
	<h3 class="text-muted">You are not authorized!</h3>
</#if>
