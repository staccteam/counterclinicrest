<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<a class="btn btn-default" href="javascript:window.print()">Print</a>
			<hr/>

			<#include '../includes/printAppointment.ftl'>

		</div>
	</div>
</div>