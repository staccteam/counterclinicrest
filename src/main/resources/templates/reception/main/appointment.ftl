<#include '../includes/simpleNavbar.ftl'>
<style>
.print-format table, .print-format tr, .print-format td, .print-format div,
	.print-format p {
	font-family: Monospace;
	line-height: 200%;
	vertical-align: middle;
}

@media screen {
	.print-format {
		//width: 3.7in;
		padding: 0.25in;
		min-height: 7in;
	}
}

table tr td {
	font-size: x-large;
}
</style>

<div class="container">
	<div class="row">
		<div class="col-md-6">
			<div class="card">
				<div class="card-header">
					<h3>Appointment Confirmation</h3>
				</div>
				<div class="card-body">
					<table class="table print-format table-responsive">
						<thead>
							<th>Appointment Details</th>
							<th><a href="/reception">Go back</a></th>
						</thead>
						<tbody>
							<tr>
								<td align="center" colspan="2"><img
									style="width: 350px;"
									src="${appointment.getQrCodeImageUrl()}"
									class="img img-responsive" /></td>
							</tr>
							<tr>
								<td><label>Appointment ID</label></td>
								<td>${appointment.getId()}</td>
							</tr>
							<tr>
								<td><label class="form-label">Patient's Name:</label></td>
								<td>${appointment.getPatientName()}</td>
							</tr>
							<tr>
								<td><label class="form-label">Doctor's Name</label></td>
								<td>${appointment.getDoctorName()}</td>
							</tr>
							<tr>
								<td><label class="form-label">Clinic Number:</label></td>
								<td>
								    <#if appointment.getClinic()??>
								        ${appointment.getClinic().getClinicNumber()}
								    <#else>
								        Not Assigned
								    </#if>


								 </td>
							</tr>
							<tr>
								<td><label class="form-label">Your Number:</label></td>
								<td>${appointment.getAppointmentNumber()}</td>
							</tr>
							<tr>
								<td><label class="form-label">Appointment Date:</label></td>
								<td>${appointment.getAppointmentTimeStamp()?number_to_datetime}</td>
							</tr>
							<tr>
								<td><a onclick="return !window.open('/reception/print-screen/${appointment.getId()}', '${title!siteName}', 'width=600,height=900')"
    target="_blank" class="btn btn-default" href="/reception/print-screen/${appointment.getId()}">Print</a></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>