var DoctorDashboard = function() {
	
	// Refreshes the Queue Stats table of the doctor
	this.refreshDoctorStats = function() {
		console.log("Refreshing Doctor Stats");
		var url = "/api/appointmentQueue/getDoctorStatus";
		$.ajax({
			url: url,
			type: 'GET',
			success: function(response) {
//			    console.log(response);
				$('#queue_size').text(response.queueSize);
				$('#curr_queue_count').text(response.currQueueCount);
			},
			error: function(error) {
				console.log("Error: " + error)
			}
			
		});
	};
	
	
}

$(document).ready(function(){
	var doctorDashboard = new DoctorDashboard();
	var refreshInterval;
	// ON Window Load
	if (location.pathname.includes("doctor"))
	{
	    console.log("refreshing doctor stats");
	    refreshInterval = setInterval(doctorDashboard.refreshDoctorStats, 10000);
	}
	else
	    clearInterval(refreshInterval);
	
	
	
	
	
	
	
	
	
	
	
	
	
});

