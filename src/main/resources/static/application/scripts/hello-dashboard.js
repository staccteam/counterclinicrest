var HelloDashboard = function() {
	
	 var makeEmailAddressLink = function(emailAddress) {
		 var preText = '<a href="mailto:';
		 var middleText = '">';
		 var postText = '</a>';
		 return preText + emailAddress + middleText + emailAddress + postText;
	 }
	 
	 var makeTelephoneLink = function (telephoneNumber) {
		 var preText = '<a href="tel:';
		 var middleText = '">';
		 var postText = '</a>';
		 return preText + telephoneNumber + middleText + telephoneNumber;
	 }
	
	this.fetchEmailAddress = function() {
		console.log("fetching email address");
		var $emailAddress = $('#contact_info_email_address');
		$.ajax({
			url: "/api/option/contact_email",
			type: 'GET',
			success: function(response) {
				console.log(response);
				if (response.val == null ){
					// set default email address
					var defaultEmail = "taherkhalil52@gmail.com";
					$emailAddress.html(makeEmailAddressLink(defaultEmail));
					return;
				} 
				var emailAddressData = response.val;
				$emailAddress.html(makeEmailAddressLink(emailAddressData));
			},
			error: function(error) {
				console.log(error);
			}
		});
	}
	
	this.fetchPhoneNumber = function() {
		console.log("fetching phone number");
		var $phoneNumber = $('#contact_info_phone_number');
		$.ajax({
			url: "/api/option/contact_phone",
			type: 'GET',
			success: function(response) {
				console.log(response);
				if (response.val == null ){
					// set default email address
					var defaultPhoneNumber = "+965-6653-8910";
					$phoneNumber.html(makeTelephoneLink(defaultPhoneNumber));
					return;
				} 
				var phoneNumberData = response.val;
				$phoneNumber.html(makeTelephoneLink(phoneNumberData));
			},
			error: function(error) {
				console.log(error);
			}
		});
	}
}
$(document).ready(function() {
	var hello = new HelloDashboard();
	hello.fetchEmailAddress();
	hello.fetchPhoneNumber();
	
	var pageRefreshInterval = setInterval(function(){
		console.log("page refresh: " + location);
		if (location.pathname == "/")
		    location.reload();
	}, 5000);
});