/*jQuery('#extend_time_form')
    .submit(function (e) {
    e.preventDefault();
    var formUrl = jQuery(this).attr('action');
    var formData = jQuery(this).serialize();
    jQuery.ajax({
        url: formUrl,
        method: 'POST',
        data: formData,
        success: function (res) {
            console.log(res);
        },
        error: function(xhr, status, res) {
        	console.log("Error: " + xhr.responseText);
        }
    });
});
*/
var CommonMaths = function() {
	this.divideIntegers = function(x,y) {
		return Math.floor(x/y);
	};
	
	this.findModulus = function(x,y) {
		return Math.floor(x%y);
	};
	
	this.calculateAvgTime = function(queueSize, currQueueCount, minutesLapsed, avgTime) {
		var pLeft = queueSize - currQueueCount;
		var totalTimeLeft = (pLeft * avgTime);
		totalTimeLeft = totalTimeLeft == 0 ? minutesLapsed : totalTimeLeft;
		return Math.floor(totalTimeLeft/pLeft);
	};
	
	this.calculateTotalTimeLeft = function(queueSize, currQueueCount, minutesLapsed, avgTime) {
		var newTotalTime = ((avgTime * (queueSize - currQueueCount)) + (avgTime - minutesLapsed));
		if (newTotalTime < 0) {
			$("#total_checkup_time").html("5");
			newTotalTime = 5;
		}
		return newTotalTime;
	};
} 

// starts counter from the total time elapsed.
var avgTime = parseInt($('#avg_session_time').html());
var avgTotalCheckupTime = parseInt($("#total_checkup_time").html());
var queueSize = parseInt($("#queue_size").html());
var currQueueCount = parseInt($("#curr_queue_count").html());
var counter = 0;
var seconds = 0;
var minutes = 0;
var hours = 0;
var sessionDuration;
var minutesLapsed = 0;
function startCounter() {
	var commonMaths = new CommonMaths();
	// increment counter and seconds
	counter++;
	minutesLapsed = Math.floor(counter/60);
	seconds = commonMaths.findModulus(counter, 60);
	// find minutes and hours
	minutes = Math.floor(counter / 60);
	hours = Math.floor(minutes / 60);

	// if minutes is greater than 1 then print minutes text
	// else print only seconds text
	if (minutes > 0) {
		sessionDuration = minutes + " MIN ";
		if (commonMath.findModulus(counter, 60) === 0) {
			console.log("divided successfully!")
			seconds = 0;
		}
		sessionDuration += seconds + " SEC";
	} else if (seconds > 0){
		sessionDuration = seconds + " SEC";
	} else {
	    sessionDuration = 0  + " SEC";
	}

	document.getElementById("session_counter").innerHTML = sessionDuration;
	// input text field
	if (document.getElementById("curr_sess_duration"))
	    document.getElementById("curr_sess_duration").value = counter;
	
	if ((queueSize - currQueueCount) > 0) {
		var newAvgTime = commonMath.calculateAvgTime(queueSize, currQueueCount, minutesLapsed, avgTime);
		var newTotalTime = commonMath.calculateTotalTimeLeft(queueSize, currQueueCount, minutesLapsed, newAvgTime);
	}
	$("#avg_session_time").html(newAvgTime);
	$("#total_checkup_time").html(newTotalTime);
}

function setSessionItem(key,value) {
	if (typeof(Storage) !== "undefined") {
		console.log("saving item: " + key + ":" + value);
		localStorage.setItem(key, value);
		return;
	} 
	alert("Please update your browser.");
}

function getSessionItem(key) {
	if (typeof(Storage) !== "undefined") {
		var value = localStorage.getItem(key);
		console.log("Reterieving Item: " + key + ":" + value);
		return value;
	}
	alert("Please update your browser.");
}

// Get Today Date
function getTodayDate() {
	var d = new Date();
	var dd = d.getDate();
	var mm = d.getMonth()+1;
	var yyyy = d.getFullYear();
	if(dd<10){
	    dd='0'+dd;
	} 
	if(mm<10){
	    mm='0'+mm;
	} 
	return dd + "/" + mm + "/" + yyyy; 
}

// Provide array of Hour, Minute, Second
// returns array of Hour, Minute, Second in 24 hour format.
function findTimeLapsedInSeconds(timeParts) {
	console.log("Time Parts" + timeParts);
	var hour = parseInt(timeParts[0]);
	var minute = parseInt(timeParts[1]);
	var second = parseInt(timeParts[2]);
	
	var d = new Date();
	var h = d.getHours();
	var m = d.getMinutes();
	var s = d.getSeconds();
	var timeInSeconds = (hour*60*60) + (minute*60) + second;
	var currTimeInSeconds = (h*60*60) + (m * 60) + s;
	console.log("Patient In Time: " + timeInSeconds);
	console.log("Curr Time: " + currTimeInSeconds);
	return currTimeInSeconds - timeInSeconds;
}

// Pass input of time in the following format: HH:MM:SS AM
// returns time in format: HH:MM:SS
function fetchTimeWithoutPostFix(time) {
	return time.substring(0, time.length - 3);
}

// Pass input of time in the following format: HH:MM:SS AM
// returns time postfix: AM
function fetchTimePostFix(time) {
	var len = time.length;
	return time.substring(len - 2, len);
}

// pass time and its postfix
// returns time array of Hour, Minute, Second in 24 hours format.
function convertTimeTo24HourFormat(time, postFix) {
	var timeParts = time.split(":");
	if (! isNaN(postFix)) { // is a number
		console.log(postFix + " is not a number");
		timeParts.push(postFix);
		return timeParts;
	}
	if ("PM" === postFix && timeParts[0] < 12) {
		timeParts[0] = parseInt(timeParts[0]) + 12;		
	} else {
		if (timeParts[0] == 12) {
			timeParts[0] = parseInt(timeParts[0]) - 12;
		}
	}
	console.log(timeParts);
	return timeParts;
}

var AvgTimeSwitcher = function() {
	var $breakTimeInput = $("#avg_time_input");
	var $breakTimeBtn = $("#avg_time_btn");
	this.toggle = function(checkboxState) {
		console.log("checkbox state: " + checkboxState);
		if (checkboxState === true) {
			// disable input
			$breakTimeInput.prop("disabled", true);
			$breakTimeBtn.prop("disabled", true);
			console.log("Sending GET request");
			$.ajax({
				url: "/api/appointmentQueue/reset-avg-time",
				type: "GET",
				success: function(data) {
					document.location.reload(true);
				},
				error: function(error) {
					console.log(error);
				}
			});
		} else {
			// enable input
			$breakTimeInput.prop("disabled", false);
			$breakTimeBtn.prop("disabled", false);
		}
	}
}

var commonMath = null;
$(document).ready(function(){
	
	// Checkbox Toggler
	var switcher = new AvgTimeSwitcher();
	$("#switch_avg_time_checkbox").change(function(e) {
		var checkboxState = $(this).is(":checked");
		switcher.toggle(checkboxState);
	});
	
	
	
	
	
	
	
	commonMath = new CommonMaths();
	var currPatientInTime = $('#curr_patient_in_time').html();
	console.log(currPatientInTime);
	if (typeof currPatientInTime === "undefined") {
		return;
	}
	// time format: 8:56:41 AM
	var time = fetchTimeWithoutPostFix(currPatientInTime);
	var timePostFix = fetchTimePostFix(currPatientInTime);
	console.log("timePrefix: " + timePostFix);
	var timeLapsed = findTimeLapsedInSeconds(convertTimeTo24HourFormat(time, timePostFix));
	counter = timeLapsed;
	console.log(timeLapsed);
	console.log("minutes passed: " + minutesLapsed);
	
	setInterval(startCounter, 1000);
});


