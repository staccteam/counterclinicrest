jQuery('#extend_time_form')
	.submit(function(e){
		e.preventDefault();
		let formUrl = jQuery(this).attr('action');
		jQuery.ajax({
			url: formUrl,
			method: 'POST',
			data: jQuery(this).serialize(),
			success: function(res) {
				console.log(res);
			}
		})
	});