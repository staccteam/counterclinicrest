var CounterClinic = function() {
	this.changeDoctorStatus = function(form) {
		var formData = $(form).serialize();
		var url = $(form).attr("action");
		$.ajax({
			url: url,
			type: "POST",
			data: formData,
			success: function(data) {
				alert("Doctor Status has been updated Successfully!");
			},
			error: function(error) {
				console.log(error);
			}
		});
	};

	
	// This method refreshes doctor status
	this.refreshDoctorStatus = function() {
		console.log("refreshing doctor status");
		var doctorStatusForms = document.querySelectorAll('.doctor-status-form');
		for (var i=0; i < doctorStatusForms.length; i++) {
			var form = $(doctorStatusForms[i]);
			var doctorStatusInput = $(form).find("input[name=doctorStatus]");
			var doctorId = $(form).find("input[type=hidden]").val();
			var url = "/api/appointmentQueue/getDoctorStatus/" + doctorId;
//			var url = "/api/appointmentQueue/getDoctorStatus";
			$.ajax({
				async: false,
				url: url,
				type: 'GET',
				success: function(response) {
				    console.log("Doctor Status: " + response);
					var doctorStatus = response.doctorStatus;
					if (! $(doctorStatusInput).is(':focus')) {
						doctorStatusInput.val(doctorStatus);
					} else {
						console.log("User making changes!");
					}
					
				},
				error: function(error) {
					console.log("Error:" + error);
				}
			});
		}
	};
}
$(document).ready(function() {
	var clinic = new CounterClinic();
	$('document, body').on("submit", "#doctor_status_form", function(e) {
		e.preventDefault();
		console.log("Form Submitted!");
		clinic.changeDoctorStatus(this);
	});
	
	var refreshInterval = setInterval(clinic.refreshDoctorStatus, 10000);
});