package com.stacc.clinic.counterclinic;

import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.calendar.Calendar;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.CalendarConnect;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Bean
    public Calendar calendar()
    {
        return new Calendar.Builder(new NetHttpTransport(), JacksonFactory.getDefaultInstance(), CalendarConnect.getCredentials())
                .setApplicationName("Counter Clinic")
                .build();
    }
}
