package com.stacc.clinic.counterclinic.config;

import com.google.api.services.calendar.model.Calendar;
import com.stacc.clinic.counterclinic.resources.appointment.*;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.CalendarModel;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.CalendarService;
import com.stacc.clinic.counterclinic.resources.appointment.queue.AppointmentQueueRepository;
import com.stacc.clinic.counterclinic.resources.clinic.Clinic;
import com.stacc.clinic.counterclinic.resources.clinic.ClinicRepository;
import com.stacc.clinic.counterclinic.resources.sitemeta.SiteMetaRepository;
import com.stacc.clinic.counterclinic.resources.sitemeta.SiteOption;
import com.stacc.clinic.counterclinic.resources.user.*;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Log4j
@Component
public class DBSeeder implements CommandLineRunner {

    private UserRepository userRepository;
    private UserService userService;
    private OnlineAppointmentRepository onlineAppointmentRepository;
    private AppointmentRepository appointmentRepository;
    private AppointmentQueueRepository appointmentQueueRepository;
    private ClinicRepository clinicRepository;
    private AppointmentService appointmentService;
    private SiteMetaRepository siteMetaRepository;
    private CalendarService calendarService;

    @Autowired
    public DBSeeder(UserRepository userRepository,
                    AppointmentRepository appointmentRepository,
                    OnlineAppointmentRepository onlineAppointmentRepository,
                    AppointmentQueueRepository appointmentQueueRepository,
                    ClinicRepository clinicRepository,
                    AppointmentService appointmentService,
                    SiteMetaRepository siteMetaRepository,
                    CalendarService calendarService,
                    UserService userService) {
        this.userRepository = userRepository;
        this.userService = userService;
        this.onlineAppointmentRepository = onlineAppointmentRepository;
        this.appointmentRepository  =  appointmentRepository;
        this.clinicRepository = clinicRepository;
        this.appointmentQueueRepository = appointmentQueueRepository;
        this.appointmentService  = appointmentService;
        this.siteMetaRepository = siteMetaRepository;
        this.calendarService = calendarService;
    }

    @Override
    public void run(String... args) {
//        deleteAllData();
//        setupTestUsers();
//        setupTestClinics();
//        setupSiteOptions();
    }

    private void setupTestAppointments(User user, String patientName, AppointmentType appointmentType) {
        log.debug(user);
        AppointmentForm form = new AppointmentForm();
        form.setDoctorId(user.getId());
        form.setPatientName(patientName);
        form.setAppointmentType(appointmentType);
        appointmentService.createNewAppointment(form);
    }

    private void setupUserCalendar(User user)
    {
        Calendar calendar = calendarService.getCalendar("q6biuc95bk9ftmtevaklvervjk@group.calendar.google.com");
        CalendarModel userCalendar = new CalendarModel();
        userCalendar.setCalendarId(calendar.getId());
        userCalendar.setSummary(calendar.getSummary());
        userCalendar.setKind(calendar.getKind());
        userCalendar.setTimezone(calendar.getTimeZone());
        user.setCalendar(userCalendar);
        userRepository.save(user);
    }

    private void deleteAllData() {
        appointmentService.deleteAllAppointments();
        appointmentQueueRepository.deleteAll();
        clinicRepository.deleteAll();
        userService.removeAllUsers();
        userRepository.deleteAll();
        siteMetaRepository.deleteAll();
        onlineAppointmentRepository.deleteAll();
    }

    private void setupTestUsers() {
        User varun = new User("Varun",  "Shrivastava",  "varunshrivastava007@gmail.com", "9960543885", "vslala", "simplepass");
        User gaurav = new User("Gaurav",  "Singh", "gaurav99@gmail.com", "9998877665", "gsingh", "simplepass");
        User priyanka  = new User("Priyanka",  "Yadav", "pvrano@gmail.com", "8876345634", "pvrano", "simplepass");
        User superadmin = new User("Super", "Admin", "superadmin@gmail.com", "9960543885", "superadmin", "simplepass");
        CalendarModel calendarModel = new CalendarModel();
        calendarModel.setCalendarId("q6biuc95bk9ftmtevaklvervjk@group.calendar.google.com");
        calendarModel.setSummary(priyanka.getEmail());
        priyanka.calendar(calendarModel);
        User vaibhav = new User("Vaibhav",  "Shrivastava", "iammagnificient@gmail.com", "9960543885", "almighty", "simplepass");

        provideRoles(varun, UserRoles.ADMIN, "CREATE_NEW_DOCTOR", "CREATE_NEW_RECEPTIONIST");
        provideRoles(gaurav, UserRoles.RECEPTION, "CREATE_NEW_POST");
        provideRoles(priyanka, UserRoles.DOCTOR, "CREATE_NEW_APPOINTMENT");
        provideRoles(vaibhav, UserRoles.RECEPTION, "CREATE_NEW_DOCTOR", "CREATE_NEW_RECEPTIONIST");
        provideRoles(superadmin, UserRoles.SUPER_ADMIN, "CREATE_NEW_DOCTOR", "CREATE_NEW_RECEPTIONIST");

        List<User> users = Arrays.asList(
                varun,
                gaurav,
                priyanka,
                vaibhav,
                superadmin
        );
        List<User> savedUsers = new ArrayList<>();

        for (int i=0; i<users.size(); i++)
        {
            savedUsers.add(userRepository.insert(users.get(i)));
        }

        priyanka = savedUsers.get(2);
        // Setting test appointments
        setupTestAppointments(priyanka, "Random Patient One", AppointmentType.WALK_IN);
        setupTestAppointments(priyanka, "Random Patient Two", AppointmentType.WALK_IN);
        setupTestAppointments(priyanka, "Random Patient Three", AppointmentType.WALK_IN);

        // Setting User Calendars
//        setupUserCalendar(priyanka);
//        setupUserCalendar(varun);
//        setupUserCalendar(gaurav);
//        setupUserCalendar(vaibhav);
//        setupUserCalendar(superadmin);
    }

    void provideRoles(User user, UserRoles role, String... permissions)
    {
        Role newRole  = Role.newInstance(role);
        Arrays.asList(permissions)
                .forEach(p->newRole.addPermission(Permission.newInstance(p)));
        user.setRoles(Arrays.asList(newRole));
    }

    public void setupTestClinics()  {
        Clinic[] clinics = {
                Clinic.newInstance("ROOM-A01 Autopsy"),
                Clinic.newInstance("ROOM-A02 X-Ray"),
                Clinic.newInstance("ROOM-A03 Paranormal")
        };

        Arrays.asList(clinics)
                .forEach(clinic-> clinicRepository.insert(clinic));
    }

    public void setupSiteOptions()
    {
        SiteOption[] siteOptions = {
                SiteOption.newInstance("contact_email", "varun.shrivastava@bemyaficionado.com"),
                SiteOption.newInstance("contact_phone", "9960543885"),
                SiteOption.newInstance("support_email", "taherkhalil53@gmail.com"),
                SiteOption.newInstance("support_phone", "9827442418")
        };

        Arrays.asList(siteOptions)
                .forEach(siteOption -> siteMetaRepository.insert(siteOption));
    }
}
