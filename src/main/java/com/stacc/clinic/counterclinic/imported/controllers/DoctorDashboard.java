package com.stacc.clinic.counterclinic.imported.controllers;

import com.stacc.clinic.counterclinic.imported.controllers.model.View;
import com.stacc.clinic.counterclinic.resources.appointment.AppointmentService;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.CalendarService;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.slots.TimeSlotService;
import com.stacc.clinic.counterclinic.resources.appointment.queue.AppointmentQueue;
import com.stacc.clinic.counterclinic.resources.appointment.queue.AppointmentQueueService;
import com.stacc.clinic.counterclinic.resources.appointment.queue.DoctorStatus;
import com.stacc.clinic.counterclinic.resources.clinic.ClinicService;
import com.stacc.clinic.counterclinic.resources.sitemeta.SiteMetaService;
import com.stacc.clinic.counterclinic.resources.user.User;
import com.stacc.clinic.counterclinic.resources.user.UserRoles;
import com.stacc.clinic.counterclinic.resources.user.UserService;
import com.stacc.clinic.counterclinic.utils.SiteConfig;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.Duration;
import java.util.*;

@Log4j
@Controller
@RequestMapping("/doctor")
public class DoctorDashboard extends DashboardBaseController {

    @Autowired
    public DoctorDashboard(UserService userService, AppointmentQueueService appointmentQueueService, AppointmentService appointmentService, ClinicService clinicService, CalendarService calendarService) {
        super(userService, appointmentQueueService, appointmentService, clinicService, calendarService);
    }

    @Autowired
    @Lazy
    TimeSlotService timeSlotService;

    private SiteMetaService siteMetaService;

    @Autowired
    public void setSiteMetaService(SiteMetaService siteMetaService) {
        this.siteMetaService = siteMetaService;
    }

    @GetMapping
    public ModelAndView loadHomePage(
            @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken
    )
    {
        log.debug("Doctor Dashboard Home Page");
        Optional<User> user = provideUserFromToken(authToken);

        if (!user.isPresent())
        {
            log.debug("Auth Token is empty.\nAuth Token:" + authToken);
            return new ModelAndView("redirect:"
                    + SiteConfig.get(SiteConfig.SITE_ROUTES).get(SiteConfig.LOGIN_ROUTE));
        }

        Optional<AppointmentQueue> appointmentQueue = appointmentQueueService.findByDoctorId(user.get().getId());

        ModelAndView mav = View.getModelAndView(View.MASTER_VIEW, siteMetaService);
        appointmentQueue.ifPresent(aq->{
            log.debug(String.format("Appointment Queue: %s", aq));
            mav.addObject("isOnBreak", aq.getDoctorStatus().equals(DoctorStatus.ON_BREAK));
            mav.addObject("stats", aq);
        });
        mav.addObject("isDashboardPageActive", "active");
        mav.addObject(THIS_USER, user.get());
        mav.addObject(View.TITLE, "Doctor Dashboard");
        mav.addObject(View.VIEW_KEY, new View("site", "index"));
        return mav;
    }

    @GetMapping("/show-online-appointments")
    public ModelAndView loadShowOnlineAppointmentsPage(@CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken)
    {
        timeSlotService.getAllSlots().forEach(timeSlot -> {
            log.debug(timeSlot.getId());
        });
        User user = authenticateUserCredential(authToken, UserRoles.DOCTOR);
        ModelAndView mav = View.getModelAndView(View.MASTER_VIEW);
        mav.addObject("isOnlineAppointmentsPageActive", "active");
        mav.addObject(THIS_USER, user);
        mav.addObject("onlineAppointments", appointmentService.displayOnlineAppointments(user));
        mav.addObject(View.VIEW_KEY, new View("site", "onlineAppointments"));
        return mav;
    }

    @GetMapping("/upload-display-picture")
    public ModelAndView  loadUploadDisplayPicturePage(@CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken)
    {
        User user = authenticateUserCredential(authToken, UserRoles.DOCTOR);
        ModelAndView mav = View.getModelAndView(View.MASTER_VIEW);
        mav.addObject(THIS_USER, user);
        mav.addObject("isUploadDisplayPictureActive", "active");
        mav.addObject(View.VIEW_KEY, new View("site", "uploadDisplayPicture"));
        return mav;
    }

    @PostMapping("/upload-display-picture")
    public ModelAndView uploadDisplayPicture(
            @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken,
            @RequestParam("file") MultipartFile file,
            RedirectAttributes attr)
    {
        User user = authenticateUserCredential(authToken, UserRoles.DOCTOR);

        userService.uploadDisplayPicture(file, (resourceFolder+"/static/global/images"), user)
        .map(uploadedFile->{
            attr.addFlashAttribute(ALERT_SUCCESS, true);
            attr.addFlashAttribute(ALERT_SUCCESS_MSG, "Image uploaded successfully!");
            return uploadedFile;
        }).orElseGet(()->{
            attr.addFlashAttribute(ALERT_DANGER, true);
            attr.addFlashAttribute(ALERT_DANGER_MSG, "Image upload unsuccessful!");
            return null;
        });
        return new ModelAndView("redirect:/doctor/upload-display-picture");
    }

    @PostMapping("/take-break")
    public ModelAndView doctorTakesBreak(
            @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken,
            @RequestParam("breakTime") Integer breakDuration
    )
    {
        User  user = authenticateUserCredential(authToken, UserRoles.DOCTOR);

        Long breakDurationInMillis = Duration.ofMinutes(breakDuration).toMillis();
        Optional<AppointmentQueue> appointmentQueue = appointmentQueueService.takeBreak(user, breakDurationInMillis);
        return new ModelAndView("redirect:/doctor/doctor-on-break");
    }

    @GetMapping("/doctor-on-break")
    public ModelAndView loadDoctorOnBreakPage(@CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken)
    {
        User user = authenticateUserCredential(authToken, UserRoles.DOCTOR);
        ModelAndView mav = View.getModelAndView(View.MASTER_VIEW);
        mav.addObject("isDoctorOnBreakPageActive", "active");
        mav.addObject(THIS_USER, user);
        appointmentQueueService.findByDoctorId(user.getId())
                .ifPresent(aq->{
                    log.debug(String.format("Appointment Queue: %s", aq));
                    mav.addObject("isOnBreak", aq.getDoctorStatus().equals(DoctorStatus.ON_BREAK));
                    mav.addObject("stats", aq);
                });
        mav.addObject(View.VIEW_KEY, new View("site", "doctorOnBreak"));
        return mav;
    }

    @PostMapping("/resume-after-break")
    public ModelAndView doctorResumesAfterBreak(
            @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken
    )
    {
        log.debug("Doctor Dashboard Doctor Taking Break");
        Optional<User> user = provideUserFromToken(authToken);

        if (!user.isPresent())
        {
            log.debug("Auth Token is empty.\nAuth Token:" + authToken);
            return new ModelAndView("redirect:"
                    + SiteConfig.get(SiteConfig.SITE_ROUTES).get(SiteConfig.LOGIN_ROUTE));
        }

        Optional<AppointmentQueue> appointmentQueue = appointmentQueueService.resumeAfterBreak(user.get());
        return new ModelAndView(String.format("redirect:%s", SiteConfig.get(SiteConfig.SITE_ROUTES).get(SiteConfig.DOCTOR_DASHBOARD_ROUTE)));
    }

    @PostMapping("/call-next-patient")
    public ModelAndView callNextPatient(
            @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken
    )
    {
        log.debug("Doctor Dashboard Calling Next Patient In");
        Optional<User> user = provideUserFromToken(authToken);

        if (!user.isPresent())
        {
            log.debug("Auth Token is empty.\nAuth Token:" + authToken);
            return new ModelAndView("redirect:"
                    + SiteConfig.get(SiteConfig.SITE_ROUTES).get(SiteConfig.LOGIN_ROUTE));
        }

        appointmentQueueService.onNextPatientIn(user.get());

        return new ModelAndView(String.format("redirect:%s", SiteConfig.get(SiteConfig.SITE_ROUTES).get(SiteConfig.DOCTOR_DASHBOARD_ROUTE)));
    }

    @PostMapping("/add-avg-time")
    public ModelAndView updateAvgTimeCalculationLogin(
            @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken,
            @RequestParam("avgTime") Long avgTime
    )
    {
        log.debug("Doctor Dashboard Doctor Taking Break");
        Optional<User> user = provideUserFromToken(authToken);

        if (!user.isPresent())
        {
            log.debug("Auth Token is empty.\nAuth Token:" + authToken);
            return new ModelAndView("redirect:"
                    + SiteConfig.get(SiteConfig.SITE_ROUTES).get(SiteConfig.LOGIN_ROUTE));
        }

        Optional<AppointmentQueue> appointmentQueue = appointmentQueueService.addAvgTime(user.get(), avgTime);
        return new ModelAndView(String.format("redirect:%s", SiteConfig.get(SiteConfig.SITE_ROUTES).get(SiteConfig.DOCTOR_DASHBOARD_ROUTE)));
    }

    @PostMapping("/end-of-day")
    public ModelAndView endOfDayForDoctor(
            @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken
    )
    {
        ModelAndView mav = new ModelAndView("redirect:/dashboard/logout");
        mav.addObject(SiteConfig.SITE_AUTH_COOKIE, authToken);
        Optional<User> user = provideUserFromToken(authToken);
        if (! user.isPresent())
        {
            return mav;
        }
        appointmentQueueService.updateDoctorStatus(user.get(), DoctorStatus.LEFT_FOR_DAY);
        return mav;
    }



}
