package com.stacc.clinic.counterclinic.imported.controllers;

import com.stacc.clinic.counterclinic.imported.controllers.model.View;
import com.stacc.clinic.counterclinic.resources.appointment.AppointmentCreationFailedException;
import com.stacc.clinic.counterclinic.resources.appointment.AppointmentService;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.CalendarService;
import com.stacc.clinic.counterclinic.resources.appointment.queue.AppointmentQueueService;
import com.stacc.clinic.counterclinic.resources.authorization.InvalidTokenException;
import com.stacc.clinic.counterclinic.resources.clinic.ClinicService;
import com.stacc.clinic.counterclinic.resources.user.User;
import com.stacc.clinic.counterclinic.resources.user.UserRoles;
import com.stacc.clinic.counterclinic.resources.user.UserService;
import com.stacc.clinic.counterclinic.utils.SiteConfig;
import com.stacc.clinic.counterclinic.utils.TokenManager;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.MissingRequestCookieException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.PostConstruct;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Optional;

@Log4j
@ControllerAdvice
@RequestMapping("/dashboard")
public class DashboardBaseController {
    @Value("${resource.folder}")
    protected String resourceFolder;

    protected static final String THIS_USER = "thisUser";
    protected static final String IS_SUCCESS = "isSuccess";
    protected static final String IS_ERROR = "isError";
    protected static final String ALERT_SUCCESS = "alertSuccess";
    protected static final String ALERT_SUCCESS_MSG = "alertSuccessMsg";
    protected static final String ALERT_DANGER = "alertDanger";
    protected static final String ALERT_DANGER_MSG = "alertDangerMsg";

    protected UserService userService;
    protected AppointmentQueueService appointmentQueueService;
    protected AppointmentService appointmentService;
    protected ClinicService clinicService;
    protected CalendarService calendarService;

    @Autowired
    public DashboardBaseController(
            UserService userService,
            AppointmentQueueService appointmentQueueService,
            AppointmentService appointmentService,
            ClinicService clinicService,
            CalendarService calendarService)
    {
        this.userService = userService;
        this.appointmentQueueService = appointmentQueueService;
        this.appointmentService = appointmentService;
        this.clinicService = clinicService;
        this.calendarService = calendarService;
    }

    @PostConstruct
    public void init()
    {
        log.debug("Global Image Directory: " + resourceFolder);
    }

    protected Optional<User> provideUserFromToken(String jwt)
    {
        if (StringUtils.isEmpty(jwt))
        {
            log.debug("Auth Token is empty.");
            return Optional.empty();
        }
        return userService.getUserByUsername(
                String.valueOf(
                        TokenManager.validateAccessToken(jwt)
                                .getBody().get("username")));
    }

    protected boolean isNull(Object obj)
    {
        return obj == null;
    }

    @GetMapping("/logout")
    public ModelAndView logoutUser(
            @CookieValue(value = SiteConfig.SITE_AUTH_COOKIE, defaultValue = "") String authToken,
            HttpServletResponse response)
    {
        Optional<User> user = provideUserFromToken(authToken);
        if (! user.isPresent())
        {
            return new ModelAndView(String.format("redirect:%s", SiteConfig.get(SiteConfig.SITE_ROUTES).get(SiteConfig.LOGIN_ROUTE)));
        }
        Cookie cookie = new Cookie(SiteConfig.SITE_AUTH_COOKIE, "");
        cookie.setMaxAge(0);
        cookie.setPath("/");
        response.addCookie(cookie);
        return new ModelAndView(String.format("redirect:%s", SiteConfig.get(SiteConfig.SITE_ROUTES).get(SiteConfig.LOGIN_ROUTE)));
    }

    protected User authenticateUserCredential(String authToken, UserRoles... allowedRoles)
    {
        Optional<User> user = TokenManager.provideUserFromToken(authToken, userService);
        if (!user.isPresent())
        {
            log.debug("Auth Token is empty.\nAuth Token:" + authToken);
            throw new InvalidTokenException("User is not authorized to make this request.");
        }

        if (
                user.get().getRoles().stream().anyMatch(role ->
                        Arrays.asList(allowedRoles).contains(role.getName())))
        {
            return user.get();
        }

        log.debug("User is not authenticated to make that request.\nUser: " + user.get());
        throw new InvalidTokenException("User is not authorized to make this request.", user);

    }

    @ExceptionHandler(AppointmentCreationFailedException.class)
    public ModelAndView redirectToDashboard(AppointmentCreationFailedException e)
    {
        return new ModelAndView("redirect:" + SiteConfig.getDashboardUrl(
                Optional.of(e.getUser())));
    }

    @ExceptionHandler(InvalidTokenException.class)
    public ModelAndView redirectToLoginPageOrDashboard(InvalidTokenException  e)
    {
        if (e.getUser().isPresent())
            return new ModelAndView("redirect:" + SiteConfig.getDashboardUrl(
                    e.getUser()));
        return new ModelAndView("redirect:" +
                SiteConfig.get(SiteConfig.SITE_ROUTES).get(SiteConfig.LOGIN_ROUTE));
    }

    @ExceptionHandler(MissingRequestCookieException.class)
    public ModelAndView missingCookieRedirectToLogin(MissingRequestCookieException e, RedirectAttributes attr)
    {
        log.warn("Message: " + e.getMessage(), e);
        attr.addFlashAttribute(ALERT_DANGER, true);
        attr.addFlashAttribute(ALERT_DANGER_MSG, "You session was expired!");
        return new ModelAndView("redirect:" +
                SiteConfig.get(SiteConfig.SITE_ROUTES).get(SiteConfig.LOGIN_ROUTE));
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    public ModelAndView handleError404()
    {
        ModelAndView mav = View.getModelAndView(View.MASTER_VIEW);
        mav.addObject(View.TITLE, "Not Found!");
        mav.addObject(View.VIEW_KEY, new View("site", "404"));
        return mav;
    }

    @ExceptionHandler(Exception.class)
    public ModelAndView handleAllUnknownExceptions(Exception e)
    {
        e.printStackTrace();
        return null;
    }
}
