package com.stacc.clinic.counterclinic.imported.controllers;

import com.stacc.clinic.counterclinic.imported.controllers.model.View;
import com.stacc.clinic.counterclinic.resources.appointment.AppointmentService;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.CalendarService;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.slots.*;
import com.stacc.clinic.counterclinic.resources.appointment.queue.AppointmentQueueService;
import com.stacc.clinic.counterclinic.resources.clinic.Clinic;
import com.stacc.clinic.counterclinic.resources.clinic.ClinicService;
import com.stacc.clinic.counterclinic.resources.user.User;
import com.stacc.clinic.counterclinic.resources.user.UserRegistrationForm;
import com.stacc.clinic.counterclinic.resources.user.UserRoles;
import com.stacc.clinic.counterclinic.resources.user.UserService;
import com.stacc.clinic.counterclinic.utils.DateUtil;
import com.stacc.clinic.counterclinic.utils.SiteConfig;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Log4j
@Controller
@RequestMapping("/admin")
public class AdminDashboard extends DashboardBaseController {

    public AdminDashboard(UserService userService,
                          AppointmentQueueService appointmentQueueService,
                          AppointmentService appointmentService,
                          ClinicService clinicService,
                          CalendarService calendarService) {
        super(userService, appointmentQueueService, appointmentService, clinicService, calendarService);
    }

    @Autowired @Lazy
    private SlotTemplateService slotTemplateService;

    @Autowired
    private TimeSlotService timeSlotService;

    @GetMapping
    public ModelAndView getAdminDashboard(
            @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken,
            RedirectAttributes attr
    ) {
        User user = authenticateUserCredential(authToken, UserRoles.ADMIN, UserRoles.SUPER_ADMIN);

        ModelAndView mav = View.getModelAndView(View.MASTER_VIEW);
        mav.addObject(View.TITLE, "Admin Dashboard");
        mav.addObject("isDashboardPageActive", "active");
        mav.addObject(THIS_USER, user);
        mav.addObject("clinics", clinicService.fetchAllClinics());
        mav.addObject("displayStats", appointmentQueueService.fetchAllQueues());
        mav.addObject("users", userService.getAllUsers()
                // fetch doctors and receptions
                .stream().filter(u->
                        u.getRoles().stream().anyMatch(role ->
                                role.getName().equals(UserRoles.DOCTOR) || role.getName().equals(UserRoles.RECEPTION) )
                ).collect(Collectors.toList()));
        mav.addObject("slotTemplates", slotTemplateService.getAllSlotTemplates());
        mav.addObject(View.VIEW_KEY, new View("site", "index"));

        return mav;
    }

    @GetMapping("/add-new-user")
    public ModelAndView loadAddNewUserPage(
            @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken)
    {
        User user = authenticateUserCredential(authToken, UserRoles.ADMIN, UserRoles.SUPER_ADMIN);
        ModelAndView mav = View.getModelAndView(View.MASTER_VIEW);
        mav.addObject(THIS_USER, user);
        mav.addObject("isAddNewUserPageActive", "active");
        mav.addObject(View.VIEW_KEY, new View("site", "addNewUser"));
        return  mav;
    }

    @PostMapping("/create-user")
    public ModelAndView createNewUser(
            @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken,
            @Valid UserRegistrationForm userRegistrationForm, BindingResult bindingResult,
            RedirectAttributes attr)
    {
        User user = authenticateUserCredential(authToken, UserRoles.ADMIN, UserRoles.SUPER_ADMIN);
        User createdUser = userService.registerNewUser(userRegistrationForm);
        attr.addFlashAttribute(ALERT_SUCCESS, true);
        attr.addFlashAttribute(ALERT_SUCCESS_MSG, "User has been created successfully!");
        ModelAndView mav = new ModelAndView("redirect:/admin/add-new-user");
        return mav;
    }

    @GetMapping("/add-new-clinic")
    public ModelAndView loadAddNewClinicPage(@CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken)
    {
        User user = authenticateUserCredential(authToken, UserRoles.ADMIN, UserRoles.SUPER_ADMIN);
        ModelAndView mav = View.getModelAndView(View.MASTER_VIEW);
        mav.addObject(THIS_USER, user);
        mav.addObject("isAddNewClinicPageActive", "active");
        mav.addObject(View.VIEW_KEY, new View("site", "addNewClinic"));
        return  mav;
    }

    @PostMapping("/add-clinic")
    public ModelAndView addNewClinic(
            @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken,
            @RequestParam("clinicRoomNumber") String clinicRoomNumber,
            RedirectAttributes attr
    )
    {
        User user = authenticateUserCredential(authToken, UserRoles.ADMIN, UserRoles.SUPER_ADMIN);
        Clinic clinic = clinicService.createClinic(Clinic.newInstance(clinicRoomNumber));
        attr.addFlashAttribute(ALERT_SUCCESS, true);
        attr.addFlashAttribute(ALERT_SUCCESS_MSG, String.format("Clinic %s has been added to the database.", clinic.getClinicNumber()));
        ModelAndView mav = new ModelAndView("redirect:/admin/add-new-clinic");
        return mav;
    }

    @PostMapping("/remove-clinic")
    public ModelAndView removeClinic(
            @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken,
            @RequestParam("clinicId") String clinicId,
            RedirectAttributes attr)
    {
        User user = authenticateUserCredential(authToken, UserRoles.ADMIN, UserRoles.SUPER_ADMIN, UserRoles.SUPER_ADMIN);
        clinicService.deleteClinic(clinicId);
        attr.addFlashAttribute(ALERT_SUCCESS_MSG, "Clinic has been deleted successfully!");
        return new ModelAndView("redirect:/admin");
    }

    @GetMapping("/assign-clinic")
    public ModelAndView loadAssignClinicPage(@CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken)
    {
        User user = authenticateUserCredential(authToken, UserRoles.ADMIN, UserRoles.SUPER_ADMIN);
        ModelAndView mav = View.getModelAndView(View.MASTER_VIEW);
        mav.addObject(THIS_USER, user);
        mav.addObject("isAssignClinicPageActive", "active");
        mav.addObject("isAssignClinicPageActive", "active");
        mav.addObject("clinics", clinicService.fetchAllClinics());
        mav.addObject("users", userService.getAllUsers().stream()
                .filter(u-> u.getRoles().stream().anyMatch(role ->
                        role.getName().equals(UserRoles.DOCTOR)))
                .collect(Collectors.toList())
        );
        mav.addObject(View.VIEW_KEY, new View("site", "assignDoctorClinic"));
        return  mav;
    }

    @PostMapping("/assign-clinic-doctor")
    public ModelAndView assignClinicToDoctor(
            @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken,
            @RequestParam("clinicId") String clinicId,
            @RequestParam("doctorId") String doctorId,
            RedirectAttributes attr
    )
    {
        User user = authenticateUserCredential(authToken, UserRoles.ADMIN, UserRoles.SUPER_ADMIN);
        clinicService.fetchClinicById(clinicId)
                .ifPresent(clinic -> {
                    userService.getUserById(doctorId)
                            .ifPresent(doctor->{
                                doctor.setClinic(clinic);
                                userService.updateUser(doctor);
                                attr.addFlashAttribute(ALERT_SUCCESS, true);
                                attr.addFlashAttribute(ALERT_SUCCESS_MSG, String.format(
                                        "Clinic %s has been assigned to Dr. %s %s",
                                        clinic.getClinicNumber(),
                                        doctor.getFirstName(),
                                        doctor.getLastName()
                                ));
                            });
                });
        return new ModelAndView("redirect:/admin/assign-clinic");
    }

    @GetMapping("/update-user-password")
    public ModelAndView loadUpdateUserPasswordPage(@CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken)
    {
        User user = authenticateUserCredential(authToken, UserRoles.ADMIN, UserRoles.SUPER_ADMIN);
        ModelAndView mav = View.getModelAndView(View.MASTER_VIEW);
        mav.addObject(THIS_USER, user);
        mav.addObject("isUpdatePasswordPageActive", "active");
        mav.addObject(View.VIEW_KEY, new View("site", "updateUserPassword"));
        return  mav;
    }

    @PostMapping("/password-update")
    public ModelAndView updateUserPassword(
            @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken,
            @RequestParam("username") String username,
            @RequestParam("password") String password,
            RedirectAttributes attr
    )
    {
        User user = authenticateUserCredential(authToken, UserRoles.ADMIN, UserRoles.SUPER_ADMIN);

        Optional<User> updateUser = userService.getUserByUsername(username);
        updateUser.ifPresent(passwordResetUser -> {
            passwordResetUser.setPassword(password);
            userService.updateUser(passwordResetUser);
            attr.addFlashAttribute(ALERT_SUCCESS, true);
            attr.addFlashAttribute(ALERT_SUCCESS_MSG, "User password have been updated successfully!");
        });

        return new ModelAndView("redirect:/admin/update-user-password");
    }

    @GetMapping("/create-new-appointment-timeslot")
    public ModelAndView loadCreateNewAppointmentTimeSlot(
            @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken)
    {
        User user = authenticateUserCredential(authToken, UserRoles.ADMIN, UserRoles.SUPER_ADMIN);
        ModelAndView mav = View.getModelAndView(View.MASTER_VIEW);
        mav.addObject(THIS_USER, user);
        mav.addObject("isOnlineAppointmentPageActive", "active");
        mav.addObject(View.VIEW_KEY, new View("site",  "addNewAppointmentTimeSlot"));
        mav.addObject("weekdays", DateUtil.getWeekdays());
        mav.addObject("timeslots", this.timeSlotService.getAllSlots());
        return mav;
    }

    @GetMapping("/edit-appointment-timeslot")
    public ModelAndView loadEditAppointmentTimeSlot(
            @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken,
            @RequestParam("slotId")  String slotId
    )
    {
        User user = authenticateUserCredential(authToken, UserRoles.ADMIN, UserRoles.SUPER_ADMIN);
        if (StringUtils.isEmpty(slotId)  ||  slotId.equals("null"))
            return new ModelAndView("redirect:/admin/create-new-appointment-timeslot");
        ModelAndView mav = View.getModelAndView(View.MASTER_VIEW);
        mav.addObject(THIS_USER, user);
        mav.addObject(View.VIEW_KEY, new View("site",  "editAppointmentTimeSlot"));
        mav.addObject("isOnlineAppointmentPageActive", "active");
        mav.addObject("weekdays", DateUtil.getWeekdays());
        mav.addObject("timeslots", this.timeSlotService.getAllSlots());
        mav.addObject("slot", slotTemplateService.getAllSlotTemplates()
                .stream()
                .filter(slotTemplate -> slotTemplate.getId().equals(slotId))
                .findFirst().get());
        return mav;
    }

    @GetMapping("/delete-appointment-timeslot")
    public ModelAndView deleteAppointmentTimeSlot(
            @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken,
            @RequestParam("slotId")  String slotId
    )
    {
        User user = authenticateUserCredential(authToken, UserRoles.ADMIN, UserRoles.SUPER_ADMIN);
        slotTemplateService.deleteTemplateById(slotId);
        return new ModelAndView("redirect:/admin#appointment_slots");
    }


    @PostMapping("/register-slots")
    public ModelAndView registerTimeSlots(
            @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken,
            @Valid SlotTemplate slotTemplate,
            RedirectAttributes attr
    )
    {
        log.debug("Event Meta Info: " + slotTemplate);
        if (!(slotTemplate.getWhatFor().contains("OPEN") || slotTemplate.getWhatFor().contains("CLOSE")))
        {
            throw new InvalidSlotTemplateException("Invalid Slot Type: " + slotTemplate.getWhatFor());
        }
        User user = authenticateUserCredential(authToken, UserRoles.ADMIN, UserRoles.SUPER_ADMIN);
        List<TimeSlot> timeSlots = slotTemplateService.refreshSlotTemplate(slotTemplate);
        log.debug("Time Slots:  " + timeSlots);
        attr.addFlashAttribute(ALERT_SUCCESS, true);
        attr.addFlashAttribute(ALERT_SUCCESS_MSG, "Slot has been created successfully!");
        ModelAndView mav = new ModelAndView("redirect:/admin/edit-appointment-timeslot?slotId="+ slotTemplate.getId() );
        return mav;
    }


    @PostMapping("remove-user")
    public ModelAndView removeUser(
            @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken,
            @RequestParam("id") String userId,
            @RequestParam("username") String username,
            RedirectAttributes attr
    )
    {
        User user = authenticateUserCredential(authToken, UserRoles.ADMIN, UserRoles.SUPER_ADMIN);
        userService.removeUser(userId);
        attr.addFlashAttribute(ALERT_SUCCESS,  true);
        attr.addFlashAttribute(ALERT_SUCCESS_MSG, "User has been removed successfully!");
        return redirectToAdminDashboard();
    }

    private ModelAndView redirectToAdminDashboard() {
        return  new ModelAndView("redirect:" + SiteConfig.get(SiteConfig.SITE_ROUTES).get(SiteConfig.ADMIN_DASHBOARD_ROUTE));
    }


    @GetMapping("/register-slots")
    public ModelAndView registerSlotsForm(
            @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken,
            RedirectAttributes attr
    ) {
        User user = authenticateUserCredential(authToken, UserRoles.ADMIN, UserRoles.SUPER_ADMIN);
        List<SlotTemplate> slotTemplates =  slotTemplateService.getAllSlotTemplates();
        log.debug("Slot Template: " + slotTemplates.size());
        ModelAndView mav = new ModelAndView(View.MASTER_VIEW);
        mav.addObject(View.VIEW_KEY, new View("admin", "adminDashboard_onlineAppointments"));
        mav.addObject(THIS_USER, user);
        mav.addObject(View.TITLE,  "Register Time Slots");
        mav.addObject(View.SITE_NAME,  "Counter Clinic");
        mav.addObject("isOnlineAppointmentPageActive", "active");
        mav.addObject("weekdays", DateUtil.getWeekdays());
        mav.addObject("timeslots", this.timeSlotService.getAllSlots());
        mav.addObject("slotTemplates", slotTemplates);
        return mav;
    }


}
