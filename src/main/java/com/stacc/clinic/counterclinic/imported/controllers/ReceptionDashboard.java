package com.stacc.clinic.counterclinic.imported.controllers;

import com.google.api.services.calendar.model.Event;
import com.stacc.clinic.counterclinic.imported.controllers.model.View;
import com.stacc.clinic.counterclinic.resources.appointment.Appointment;
import com.stacc.clinic.counterclinic.resources.appointment.AppointmentCreationFailedException;
import com.stacc.clinic.counterclinic.resources.appointment.AppointmentForm;
import com.stacc.clinic.counterclinic.resources.appointment.AppointmentService;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.CalendarService;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.CalendarUtil;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.EventModel;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.EventModelOutput;
import com.stacc.clinic.counterclinic.resources.appointment.queue.AppointmentQueue;
import com.stacc.clinic.counterclinic.resources.appointment.queue.AppointmentQueueService;
import com.stacc.clinic.counterclinic.resources.clinic.Clinic;
import com.stacc.clinic.counterclinic.resources.clinic.ClinicService;
import com.stacc.clinic.counterclinic.resources.user.User;
import com.stacc.clinic.counterclinic.resources.user.UserRoles;
import com.stacc.clinic.counterclinic.resources.user.UserService;
import com.stacc.clinic.counterclinic.utils.DateUtil;
import com.stacc.clinic.counterclinic.utils.SiteConfig;
import com.stacc.clinic.counterclinic.utils.TokenManager;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

@Log4j
@Controller
@RequestMapping("/reception")
public class ReceptionDashboard extends DashboardBaseController {

    @Autowired
    public ReceptionDashboard(
            UserService userService,
            AppointmentQueueService appointmentQueueService,
            AppointmentService appointmentService,
            ClinicService clinicService,
            CalendarService calendarService)
    {
        super(userService, appointmentQueueService, appointmentService, clinicService, calendarService);
    }

    @GetMapping
    public ModelAndView getReceptionDashboardHomePage(@CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken) {
        log.debug("Reception Dashboard Home Page");
        User user = authenticateUserCredential(authToken, UserRoles.RECEPTION);

        ModelAndView mav = View.getModelAndView(View.MASTER_VIEW);
        mav.addObject(View.TITLE, "Reception Dashboard");
        mav.addObject(THIS_USER, user);
        mav.addObject("clinics", clinicService.fetchAllClinics());
        mav.addObject("users", userService.getAllUsers().stream()
                .filter(u-> u.getRoles().stream().anyMatch(role ->
                        role.getName(   ).equals(UserRoles.DOCTOR)))
                .collect(Collectors.toList())
        );
        mav.addObject("displayStats", appointmentQueueService.fetchAllQueues());
        mav.addObject("appointments", appointmentService.fetchAppointments());
        mav.addObject(View.VIEW_KEY, new View("reception", "receptionDashboard"));
        return mav;
    }

    @PostMapping("/make-appointment")
    public ModelAndView makeAppointment(
            @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken,
            @Valid AppointmentForm appointmentForm,
            BindingResult binding,
            RedirectAttributes attr)
    {
        log.debug("Reception Dashboard Make Appointment");
        User user = authenticateUserCredential(authToken, UserRoles.RECEPTION);

        Optional<User> doctor = userService.getUserById(appointmentForm.getDoctorId());
        doctor.ifPresent(doc->{
            if  (binding.hasErrors() || isNull(doc.getClinic()))
            {
                log.warn(String.format("Doctor/Clinic is not assigned!"));
                attr.addFlashAttribute(IS_ERROR, "Doctor or Clinic is not assigned.");
                throw new AppointmentCreationFailedException("Doctor/Clinic is not assigned!", user);
            }
        });

        log.info("Creating Appointment with details: " + appointmentForm);

        Optional<Appointment> newAppointment = appointmentService.createNewAppointment(appointmentForm);

        newAppointment.ifPresent(appointment1 -> log.debug(appointment1));
        ModelAndView mav = new ModelAndView(View.MASTER_VIEW);
        mav.addObject(THIS_USER, user);
        mav.addObject(View.TITLE,  "New Appointment Details");
        mav.addObject("appointment", newAppointment.get());
        mav.addObject(View.VIEW_KEY, new View("reception", "appointment"));
        return mav;
    }

    @PostMapping("/assign-clinic-doctor")
    public ModelAndView assignClinicToDoctor(
            @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken,
            RedirectAttributes attr,
            @RequestParam("clinicId") String clinicId,
            @RequestParam("doctorId") String doctorId
    )
    {
        log.debug("Reception Dashboard Assign Clinic to Doctor");

        Optional<User> user = provideUserFromToken(authToken);

        if (!user.isPresent())
        {
            log.debug("Auth Token is empty.\nAuth Token:" + authToken);
            return new ModelAndView("redirect:"
                    + SiteConfig.get(SiteConfig.SITE_ROUTES).get(SiteConfig.LOGIN_ROUTE));
        }

        ModelAndView mav = new ModelAndView(String.format("redirect:%s",
                SiteConfig.get(SiteConfig.SITE_ROUTES).get(SiteConfig.RECEPTION_DASHBOARD_ROUTE)));

        if (StringUtils.isEmpty(clinicId) || StringUtils.isEmpty(doctorId))
        {
            log.debug("Input is empty.");
            attr.addFlashAttribute(IS_ERROR, "Inputs cannot be empty!");
            return mav;
        }

        Optional<Clinic> clinic = clinicService.fetchClinicById(clinicId);
        clinic.ifPresent(c-> {
            Optional<User> doctor = userService.getUserById(doctorId);
            doctor.ifPresent(doc->  {
                doc.setClinic(c);
                User savedUser = userService.updateUser(doc);
                if (! isNull(savedUser))
                    attr.addFlashAttribute(IS_SUCCESS, "Clinic has been assigned to doctor.");
                else
                    attr.addFlashAttribute(IS_ERROR, "There was some problem in the backend.");
            });
        });


        return mav;
    }

    @GetMapping("/getAppointmentInfo/{appointmentId}")
    public ModelAndView getAppointmentInfo(@PathVariable("appointmentId") String appointmentId,
                                           @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken) {
        log.debug("Reception Dashboard Get Appointment Info");

        User user = authenticateUserCredential(authToken, UserRoles.RECEPTION);

        Optional<Appointment> appointmentInfo = appointmentService.fetchAppointment(appointmentId);
        ModelAndView mav = View.getModelAndView(View.MASTER_VIEW);
        mav.addObject(View.TITLE, "Appointment Info");
        mav.addObject(View.SITE_NAME,  "Counter Clinic");
        mav.addObject(THIS_USER, user);
        mav.addObject("appointment", appointmentInfo.get());
        mav.addObject(View.VIEW_KEY, new View("reception", "appointment"));
        return mav;
    }

    @PostMapping("/delete-appointment/{appointmentId}")
    public ModelAndView deleteAppointmentById(
            @PathVariable("appointmentId") String appointmentId,
            @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken,
            RedirectAttributes attr)
    {
        log.debug("Reception Dashboard Get Appointment Info");

        Optional<User> user = provideUserFromToken(authToken);

        if (! user.isPresent())
        {
            log.debug("Auth Token is empty.\nAuth Token:" + authToken);
            return new ModelAndView("redirect:"
                    + SiteConfig.get(SiteConfig.SITE_ROUTES).get(SiteConfig.LOGIN_ROUTE));
        }

        appointmentService.deleteAppointment(appointmentId);
        attr.addAttribute(IS_SUCCESS,
                String.format("Appointment with ID: %s has been deleted successfully!", appointmentId));

        return new ModelAndView(String.format(
                "redirect:%s",
                SiteConfig.get(
                        SiteConfig.SITE_ROUTES).get(
                                SiteConfig.RECEPTION_DASHBOARD_ROUTE)));
    }

    @GetMapping("/print-screen/{appointmentId}")
    public ModelAndView showPrintScreen(
        @PathVariable("appointmentId") String appointmentId,
        @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken
    )
    {
        log.debug("Reception Dashboard Get Appointment Info");

       User user = authenticateUserCredential(authToken, UserRoles.RECEPTION);

        Optional<Appointment> appointmentInfo = appointmentService.fetchAppointment(appointmentId);
        ModelAndView mav = View.getModelAndView(View.MASTER_VIEW);
        mav.addObject(View.TITLE, "Print Appointment Info");
        mav.addObject(THIS_USER, user);
        mav.addObject("appointment",  appointmentInfo.get());
        mav.addObject(View.VIEW_KEY, new View("reception", "appointmentPrintScreen"));
        return mav;
    }

    @GetMapping("/appointments")
    public ModelAndView getPatientsAppointmentPage(
            @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken
    )
    {
        Optional<User> user = TokenManager.provideUserFromToken(authToken, userService);
        if (!user.isPresent())
        {
            log.debug("Auth Token is empty.\nAuth Token:" + authToken);
            return new ModelAndView("redirect:"
                    + SiteConfig.get(SiteConfig.SITE_ROUTES).get(SiteConfig.LOGIN_ROUTE));
        }

        // fetch all appointments from all calendars
        List<EventModelOutput> appointments = new ArrayList<>();
        calendarService.getAllCalendars().forEach(calendarListEntry -> {
            appointments.addAll(
                    CalendarUtil.convertListOfEventsToEventModelOutput(
                            calendarService.getAllEvents(calendarListEntry.getId()), userService));
        });

        ModelAndView mav = View.getModelAndView(View.MASTER_VIEW);
        mav.addObject(View.TITLE, "Online Patients");
        mav.addObject(THIS_USER, user.get());
        mav.addObject("appointments", appointments);
        mav.addObject(View.VIEW_KEY, new View("reception", "onlineAppointments"));
        return mav;
    }

    @GetMapping("/edit/appointments/{calendarId}/{eventId}")
    public ModelAndView modifyEventDetailsPage(
            @PathVariable("calendarId") String calendarId,
            @PathVariable("eventId") String eventId,
            @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken)
    {
        log.debug("Reception Dashboard Get Appointment Info");

        Optional<User> user = provideUserFromToken(authToken);

        if (! user.isPresent())
        {
            log.debug("Auth Token is empty.\nAuth Token:" + authToken);
            return new ModelAndView("redirect:"
                    + SiteConfig.get(SiteConfig.SITE_ROUTES).get(SiteConfig.LOGIN_ROUTE));
        }

        Event event = calendarService.getEvent(calendarId, eventId);
        List<EventModelOutput> eventOutputModel = CalendarUtil.convertListOfEventsToEventModelOutput(Arrays.asList(event), userService);

        ModelAndView mav = View.getModelAndView(View.MASTER_VIEW);
        mav.addObject(THIS_USER, user.get());
        mav.addObject(View.TITLE, "Edit Appointment");
        mav.addObject("slots", eventOutputModel.stream().findFirst().get());
        mav.addObject("doctors",
                userService.getAllUsers().stream()
                        .filter(u-> u.getRoles().stream().anyMatch(role ->
                                role.getName().equals(UserRoles.DOCTOR)))
                        .collect(Collectors.toList())
        );
        mav.addObject(View.VIEW_KEY, new View("reception",  "onlineAppointmentEdit"));
        return mav;
    }

    @PostMapping("/edit/appointments")
    public ModelAndView modifyAppointment(
            @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken,
            @Valid EventModel inputEventModel
    )
    {
        log.debug("Reception Dashboard Edit Appointment Info Post");

        Optional<User> user = provideUserFromToken(authToken);
        if (! user.isPresent())
        {
            log.debug("Auth Token is empty.\nAuth Token:" + authToken);
            return new ModelAndView("redirect:"
                    + SiteConfig.get(SiteConfig.SITE_ROUTES).get(SiteConfig.LOGIN_ROUTE));
        }

        Long startTime = DateUtil.convertISO8601LocalToMilliseconds(inputEventModel.getStartDateTime());
        inputEventModel.setStartTime(startTime);
        inputEventModel.setEndTime(DateUtil.addMinutesToTime(startTime, 15));
        Event modifiedEvent = calendarService.updateEvent(inputEventModel);
        log.debug(inputEventModel);

        log.debug("Modified Event Successfully:  " + modifiedEvent);

        ModelAndView mav = new ModelAndView(
                String.format("redirect:%s",
                        SiteConfig.get(SiteConfig.SITE_ROUTES).get(SiteConfig.MODIFY_EVENT_DETAILS_PAGE_ROUTE)
                            .replaceAll("\\{calendarId\\}", inputEventModel.getCalendarId())
                                .replaceAll("\\{eventId\\}", inputEventModel.getEventId())
                ));
        mav.addObject(IS_SUCCESS, "Appointment Modified Successfully!");
        return mav;
    }

    @PostMapping("/reset-all-queue")
    public ModelAndView resetAllQueues(@CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken)
    {
        User user = authenticateUserCredential(authToken, UserRoles.RECEPTION);
        appointmentQueueService.resetAllQueues();
        return new ModelAndView("redirect:/reception");
    }

    @PostMapping("/delete-appointment")
    public ModelAndView deleteAppointment(@CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken,
                                          @RequestParam("appointmentId") String appointmentId,
                                          RedirectAttributes attr)
    {
        User user = authenticateUserCredential(authToken, UserRoles.RECEPTION);
        appointmentService.deleteAppointment(appointmentId);
        attr.addFlashAttribute(ALERT_SUCCESS, true);
        attr.addFlashAttribute(ALERT_SUCCESS_MSG, "Appointment delete successfully!");
        return new ModelAndView("redirect:/reception#list_of_appointments");
    }

    @PostMapping("/delete-all-appointments")
    public ModelAndView deleteAllAppointments(@CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken)
    {
        User user = authenticateUserCredential(authToken, UserRoles.RECEPTION);
        appointmentService.deleteAllAppointments();
        appointmentQueueService.resetAllQueues();
        return new ModelAndView("redirect:/reception#list_of_appointments");
    }

    @PostMapping("/init-queue-for-doctor")
    public ModelAndView initQueueForDoctor(@CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken,
                                           @RequestParam("doctorId") String doctorId)
    {
        User user = authenticateUserCredential(authToken, UserRoles.RECEPTION);
        appointmentQueueService.findByDoctorId(doctorId)
                .ifPresent(queue->{
                    appointmentQueueService.resetQueue(queue.getId(), doctorId);
                    userService.getUserById(doctorId)
                            .ifPresent(doc->{
                                doc.setAppointmentQueue(queue);
                                userService.updateUser(doc);
                            });
                });
        return new ModelAndView("redirect:/reception#queue_stats");
    }
}
