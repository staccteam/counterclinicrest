package com.stacc.clinic.counterclinic.imported.controllers.model;

import com.stacc.clinic.counterclinic.resources.sitemeta.SiteMetaService;
import com.stacc.clinic.counterclinic.utils.SiteConfig;
import org.springframework.web.servlet.ModelAndView;

public class View {

    public static final String MASTER_VIEW = "masterTemplate";
    public static final String TITLE = "title";
    public static final String SITE_LOGO = "siteLogo";
    public static final String SITE_NAME = "siteName";
    public static final String SITE_TIMEZONE = "siteTimezone";
    public static final String VIEW_KEY = "view";
    private String page;
    private String component;

    public View(String page, String component) {
        this.page = page;
        this.component = component;
    }

    public String getPage() {
        return page;
    }
    public void setPage(String page) {
        this.page = page;
    }
    public String getComponent() {
        return component;
    }
    public void setComponent(String component) {
        this.component = component;
    }

    public static ModelAndView getModelAndView(String masterView) {
        ModelAndView mav = new ModelAndView(masterView);
        mav.addObject(View.SITE_LOGO, SiteConfig.get(SiteConfig.SITE_SETTINGS).get(SiteConfig.SITE_LOGO));
        mav.addObject(View.SITE_NAME, SiteConfig.get(SiteConfig.SITE_SETTINGS).get(SiteConfig.SITE_NAME));
        mav.addObject(View.TITLE, SiteConfig.get(SiteConfig.SITE_SETTINGS).get(SiteConfig.SITE_NAME));
        mav.addObject(View.SITE_TIMEZONE, SiteConfig.get(SiteConfig.SITE_SETTINGS).get(SiteConfig.SITE_TIMEZONE));
        return mav;
    }

    public static ModelAndView getModelAndView(String masterView, SiteMetaService siteMetaService)
    {
        ModelAndView mav = getModelAndView(masterView);
        mav.addObject(View.SITE_TIMEZONE, SiteConfig.newInstance(siteMetaService).getSiteTimezone());
        return mav;
    }
}
