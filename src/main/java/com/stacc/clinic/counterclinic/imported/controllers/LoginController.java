package com.stacc.clinic.counterclinic.imported.controllers;

import com.stacc.clinic.counterclinic.imported.controllers.model.View;
import com.stacc.clinic.counterclinic.resources.appointment.queue.AppointmentQueueService;
import com.stacc.clinic.counterclinic.resources.appointment.queue.DoctorStatus;
import com.stacc.clinic.counterclinic.resources.authorization.AuthorizationService;
import com.stacc.clinic.counterclinic.resources.authorization.AuthorizationServiceImpl;
import com.stacc.clinic.counterclinic.resources.authorization.LoginUserInput;
import com.stacc.clinic.counterclinic.resources.sitemeta.SiteMetaService;
import com.stacc.clinic.counterclinic.resources.user.User;
import com.stacc.clinic.counterclinic.resources.user.UserRoles;
import com.stacc.clinic.counterclinic.utils.SiteConfig;
import com.stacc.clinic.counterclinic.utils.TokenManager;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Optional;

@Log4j
@Controller
@RequestMapping("/login")
public class LoginController {

    private AuthorizationService authService;
    private AppointmentQueueService appointmentQueueService;
    private SiteMetaService siteMetaService;

    @Autowired
    public LoginController(
            AuthorizationService authService,
            AppointmentQueueService  appointmentQueueService) {
        this.appointmentQueueService = appointmentQueueService;
        this.authService = authService;
    }

    @Autowired
    public void setSiteMetaService(SiteMetaService siteMetaService)
    {
        this.siteMetaService = siteMetaService;
    }

    @GetMapping("/user")
    public ModelAndView  loadLoginPage() {
        log.debug("Login Page Must Be Rendered!");
        ModelAndView mav = View.getModelAndView(View.MASTER_VIEW);
        String title = "Login Page";
        mav.addObject(View.TITLE, title);
        mav.addObject("siteLogo", siteMetaService.fetchSiteOptionByKey("site_logo"));
        mav.addObject(View.VIEW_KEY, new View("login", "login"));
        return mav;
    }

    @PostMapping("/user")
    public ModelAndView loginUser(HttpServletResponse response,
                                  RedirectAttributes attr,
                                  @Valid LoginUserInput loginUserInput,
                                  BindingResult binding) {
        log.debug("Login User Request!\n"+loginUserInput);
        if (binding.hasErrors()) {
            log.info("Binding has Error for the provided input");
            return new ModelAndView("redirect:".concat(SiteConfig.get(SiteConfig.SITE_ROUTES).get(SiteConfig.LOGIN_ROUTE)));
        }

        Optional<User> user = authService.loginUser(loginUserInput);

        if (!user.isPresent()) {
            log.info("User is Not Authentic!");
            attr.addFlashAttribute("isError", "Username or password do not match!");
            return new ModelAndView("redirect:".concat(SiteConfig.get(SiteConfig.SITE_ROUTES).get(SiteConfig.LOGIN_ROUTE)));
        }

        // Generate JWT Token and add it to response.
        String jwtToken = TokenManager.createAccessToken(user.get());
        response.addCookie(dropCookie(SiteConfig.SITE_AUTH_COOKIE, jwtToken));

        attr.addFlashAttribute("isLogin", true);
        attr.addFlashAttribute("isSuccess", "Login Successful!");

        UserRoles userType = user.get().getRoles().stream().findFirst().get().getName();

        /*
        If doctor appointment queue is present
        then update the doctor status to LOGGED_IN.
         */
        if (user.get().getRoles().stream().anyMatch(role-> role.getName().equals(UserRoles.DOCTOR))) {
            appointmentQueueService.updateDoctorStatus(user.get(), DoctorStatus.LOGGED_IN);
        }

        return View.getModelAndView("redirect:" + SiteConfig.getDashboardUrl(user));
    }

    private Cookie dropCookie(String cookieName, String jwtToken) {
        Cookie cookie = new Cookie(cookieName, jwtToken);
        cookie.setMaxAge(12 * 3600);
        cookie.setPath("/");
        return cookie;
    }

    @GetMapping("/forgot-password")
    public ModelAndView renderForgotPasswordPage(HttpServletRequest request) {
        ModelAndView mav = View.getModelAndView(View.MASTER_VIEW);
        String title  = "Forgot Password";
        mav.addObject("title", title);
        mav.addObject(View.VIEW_KEY, new View("login", "forgotPassword"));
        return mav;
    }
}
