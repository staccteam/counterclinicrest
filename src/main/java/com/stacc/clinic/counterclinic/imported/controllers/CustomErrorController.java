package com.stacc.clinic.counterclinic.imported.controllers;

import com.stacc.clinic.counterclinic.imported.controllers.model.View;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

@Controller
public class CustomErrorController implements ErrorController {

    @RequestMapping("/error")
    public ModelAndView handleError(HttpServletRequest request)
    {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

        ModelAndView mav = View.getModelAndView(View.MASTER_VIEW);
        if (status.equals(HttpStatus.NOT_FOUND.value()))
        {
            mav.addObject(View.TITLE,"Not Found!");
            mav.addObject(View.VIEW_KEY,  new View("site", "404"));
            return mav;
        }
        mav.addObject(View.TITLE,"Error!");
        mav.addObject(View.VIEW_KEY,  new View("site", "globalError"));
        return mav;
    }
    /**
     * Returns the path of the error page.
     *
     * @return the error path
     */
    @Override
    public String getErrorPath() {
        return "/error";
    }
}
