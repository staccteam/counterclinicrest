package com.stacc.clinic.counterclinic.imported.controllers;

import com.stacc.clinic.counterclinic.imported.controllers.model.View;
import com.stacc.clinic.counterclinic.resources.appointment.AppointmentService;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.CalendarService;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.slots.SlotTemplateService;
import com.stacc.clinic.counterclinic.resources.appointment.queue.AppointmentQueueService;
import com.stacc.clinic.counterclinic.resources.clinic.ClinicService;
import com.stacc.clinic.counterclinic.resources.sitemeta.SiteMetaService;
import com.stacc.clinic.counterclinic.resources.sitemeta.SiteOption;
import com.stacc.clinic.counterclinic.resources.user.*;
import com.stacc.clinic.counterclinic.utils.FileUploadUtil;
import com.stacc.clinic.counterclinic.utils.SiteConfig;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.File;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

@Log4j
@Controller
@RequestMapping("/superadmin")
public class SuperAdminDashboard  extends DashboardBaseController {
    private SiteMetaService siteMetaService;
    private SlotTemplateService slotTemplateService;

    public SuperAdminDashboard(UserService userService, AppointmentQueueService appointmentQueueService, AppointmentService appointmentService, ClinicService clinicService, CalendarService calendarService) {
        super(userService, appointmentQueueService, appointmentService, clinicService, calendarService);
    }

    @Autowired
    public void setSlotTemplateService(SlotTemplateService slotTemplateService)
    {
        this.slotTemplateService = slotTemplateService;
    }

    @Autowired
    public void setSiteMetaService(SiteMetaService siteMetaService)
    {
        this.siteMetaService = siteMetaService;
    }

    @GetMapping
    public ModelAndView getSuperAdminDashboard(
            @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken) {
        User user = authenticateUserCredential(authToken, UserRoles.SUPER_ADMIN);
        ModelAndView mav = View.getModelAndView(View.MASTER_VIEW);
        mav.addObject("isDashboardPageActive", "active");
        mav.addObject(THIS_USER, user);
        mav.addObject("timezones", Arrays.asList(TimeZone.getAvailableIDs()));
        mav.addObject("clinics", clinicService.fetchAllClinics());
        mav.addObject("displayStats", appointmentQueueService.fetchAllQueues());
        mav.addObject("slotTemplates", slotTemplateService.getAllSlotTemplates());
        mav.addObject("users", userService.getAllUsers()
                // fetch doctors and receptions
                .stream().filter(u->
                        u.getRoles().stream().anyMatch(role ->
                                role.getName().equals(UserRoles.DOCTOR) || role.getName().equals(UserRoles.RECEPTION) )
                ).collect(Collectors.toList()));
        mav.addObject("admins", userService.getAllUsers().stream()
                .filter(u->
                    u.getRoles().stream().anyMatch(role->
                        role.getName().equals(UserRoles.ADMIN)))
                .collect(Collectors.toList())); // fetch all admin users
        mav.addObject("siteOptions", siteMetaService.fetchAllSiteOptions());
        mav.addObject(View.VIEW_KEY, new View("site", "index"));
        return mav;
    }

    @GetMapping("/add-option")
    public ModelAndView loadAddOptionPage( @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken)
    {
        User user = authenticateUserCredential(authToken, UserRoles.SUPER_ADMIN);
        ModelAndView mav = View.getModelAndView(View.MASTER_VIEW);
        mav.addObject(THIS_USER, user);
        mav.addObject("isCustomOptionPageActive", "active");
        mav.addObject(View.TITLE, "Add custom option");
        mav.addObject(View.VIEW_KEY, new View("site", "addCustomOption"));
        return mav;
    }


    @PostMapping("/save-option")
    public ModelAndView saveOption(
            @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken,
            @Valid SiteOption option, BindingResult binding) {
        User user = authenticateUserCredential(authToken, UserRoles.SUPER_ADMIN);
        ModelAndView mav = new ModelAndView("redirect:" + SiteConfig.get(SiteConfig.SITE_ROUTES).get(SiteConfig.SUPER_ADMIN_DASHBOARD_ROUTE) + "#site_options");
        SiteOption siteOption = siteMetaService.createNewSiteMetaOption(option);
        return mav;
    }

    @PostMapping("/delete-option")
    public ModelAndView deleteOption(
            @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken,
            @RequestParam("optionId") String optionId) {
        User user = authenticateUserCredential(authToken, UserRoles.SUPER_ADMIN);
        ModelAndView mav = new ModelAndView("redirect:" + SiteConfig.get(SiteConfig.SITE_ROUTES).get(SiteConfig.SUPER_ADMIN_DASHBOARD_ROUTE));
        siteMetaService.deleteSiteMetaOption(optionId);
        return mav;
    }

    @GetMapping("/register/admin")
    public ModelAndView loadRegisterAdminPage(@CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken)
    {
        User user = authenticateUserCredential(authToken, UserRoles.SUPER_ADMIN);
        ModelAndView mav = View.getModelAndView(View.MASTER_VIEW);
        mav.addObject(THIS_USER, user);
        mav.addObject("isRegisterAdminPageActive",  "active");
        mav.addObject(View.TITLE, "Register Admin");
        mav.addObject(View.VIEW_KEY, new View("site", "addNewAdmin"));
        return mav;
    }

    @PostMapping("/register/admin")
    public ModelAndView registerNewUser(@CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken,
                                        @Valid UserRegistrationForm userRegistrationForm, BindingResult bindingResult,
                                        RedirectAttributes attr)
    {
        User user = authenticateUserCredential(authToken, UserRoles.SUPER_ADMIN);
        if (bindingResult.hasErrors())
        {
            attr.addFlashAttribute(ALERT_DANGER, true);
            attr.addFlashAttribute(ALERT_DANGER_MSG, "Invalid Input!");
            attr.addFlashAttribute("errors", bindingResult.getAllErrors());
            bindingResult.getAllErrors().forEach(e->{
                log.debug(e.getObjectName());
            });
            return new ModelAndView("redirect:/superadmin/register/admin");
        }

        User registeredUser = userService.registerNewUser(userRegistrationForm);
        attr.addFlashAttribute(ALERT_SUCCESS, true);
        attr.addFlashAttribute(ALERT_SUCCESS_MSG, "Admin User has been created!");
        return new ModelAndView("redirect:/superadmin#list_of_admins");

    }

    @GetMapping("/add-timezone")
    public ModelAndView loadAddSiteTimeZonePage(@CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken)
    {
        User user = authenticateUserCredential(authToken, UserRoles.SUPER_ADMIN);
        ModelAndView mav = View.getModelAndView(View.MASTER_VIEW);
        mav.addObject(THIS_USER, user);
        mav.addObject("isSiteTimeZonePageActive", "active");
        mav.addObject(View.TITLE, "Add Site TimeZone");
        mav.addObject("timezones", TimeZone.getAvailableIDs());
        mav.addObject("siteTimezone", siteMetaService.fetchSiteOptionByKey("site_timezone"));
        mav.addObject(View.VIEW_KEY, new View("site", "addSiteTimeZone"));
        return mav;
    }

    @GetMapping("/upload-file")
    public ModelAndView loadUploadLogoPage(@CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken)
    {
        User user = authenticateUserCredential(authToken, UserRoles.SUPER_ADMIN);
        ModelAndView mav = View.getModelAndView(View.MASTER_VIEW);
        mav.addObject(THIS_USER, user);
        mav.addObject("isUploadLogoPageActive", "active");
        mav.addObject("siteLogo", siteMetaService.fetchSiteOptionByKey("site_logo"));
        mav.addObject("adPicture", siteMetaService.fetchSiteOptionByKey("ad_picture"));
        mav.addObject(View.VIEW_KEY, new View("site", "uploadDisplayPicture"));
        return mav;
    }

    @PostMapping("/upload-file")
    public ModelAndView uploadFile(
            @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken,
            @RequestParam("file") MultipartFile file)
    {
        User user = authenticateUserCredential(authToken, UserRoles.SUPER_ADMIN);
        ModelAndView mav = new ModelAndView("redirect:" + SiteConfig.get(SiteConfig.SITE_ROUTES).get(SiteConfig.SUPER_ADMIN_DASHBOARD_ROUTE));
        File uploadedFile = FileUploadUtil.uploadFile(file, resourceFolder + "/static/global/images/logo");
        siteMetaService.createNewSiteMetaOption(new SiteOption("site_logo", "/global/images/logo/"+uploadedFile.getName()));
        return mav;
    }

    @PostMapping("/upload-ad-pic")
    public ModelAndView uploadAdPic(@CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken,
                                    RedirectAttributes attr,
                                    @RequestParam("file") MultipartFile file)
    {
        User user = authenticateUserCredential(authToken, UserRoles.SUPER_ADMIN);
        siteMetaService.fetchSiteOptionByKey("ad_picture").ifPresent(siteOption -> {
            FileUploadUtil.removeFile(siteOption.getVal());
        });

        final String ADVERTISEMENT_DIRECTORY = resourceFolder + "/static/global/images/advVSCODEertisement";
        FileUploadUtil.cleanDirectory(Paths.get(ADVERTISEMENT_DIRECTORY).toFile());
        File uploadedFile = FileUploadUtil.uploadFile(file, ADVERTISEMENT_DIRECTORY);
        siteMetaService.createNewSiteMetaOption(
                new SiteOption("ad_picture",
                        "/global/images/advertisement/"+uploadedFile.getName()));
        attr.addFlashAttribute(ALERT_SUCCESS, true);
        attr.addFlashAttribute(ALERT_SUCCESS_MSG, "Ad image has been uploaded successfully!");
        return new ModelAndView("redirect:/superadmin/upload-file");
    }

    @PostMapping("/cancel-registration")
    public ModelAndView cancelRegistration(@CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken,
                                           RedirectAttributes attr,
                                           @RequestParam("id") String userId)
    {
        User user = authenticateUserCredential(authToken, UserRoles.SUPER_ADMIN);
        userService.removeUser(userId);
        return new ModelAndView("redirect:/superadmin#cancel_registration");
    }

}
