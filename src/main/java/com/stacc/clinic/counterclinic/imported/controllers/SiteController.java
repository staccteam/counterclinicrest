package com.stacc.clinic.counterclinic.imported.controllers;

import com.stacc.clinic.counterclinic.imported.controllers.model.View;
import com.stacc.clinic.counterclinic.resources.appointment.AppointmentService;
import com.stacc.clinic.counterclinic.resources.appointment.queue.AppointmentQueue;
import com.stacc.clinic.counterclinic.resources.appointment.queue.AppointmentQueueService;
import com.stacc.clinic.counterclinic.resources.sitemeta.SiteMetaService;
import com.stacc.clinic.counterclinic.resources.user.User;
import com.stacc.clinic.counterclinic.resources.user.UserRoles;
import com.stacc.clinic.counterclinic.resources.user.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/")
public class SiteController {

    private SiteMetaService siteMetaService;
    private UserService userService;
    private AppointmentQueueService appointmentQueueService;
    private AppointmentService appointmentService;

    public SiteController(SiteMetaService siteMetaService, UserService userService, AppointmentQueueService appointmentQueueService, AppointmentService appointmentService) {
        this.siteMetaService = siteMetaService;
        this.userService = userService;
        this.appointmentQueueService = appointmentQueueService;
        this.appointmentService = appointmentService;
    }

    @GetMapping
    public ModelAndView loadHomePage()
    {
        List<User> doctors = userService.getAllUsers()
                .stream().filter(user -> user.getRoles().stream().anyMatch(role -> role.getName().equals(UserRoles.DOCTOR)))
                .collect(Collectors.toList());
        ModelAndView mav = View.getModelAndView(View.MASTER_VIEW);
        mav.addObject("isHomePageActive", "active");
        mav.addObject(View.VIEW_KEY, new View("site", "homePage"));
        mav.addObject("doctors", doctors);
        return mav;
    }

    @GetMapping("/support")
    public ModelAndView loadSupportPage()
    {
        ModelAndView mav = View.getModelAndView(View.MASTER_VIEW);
        mav.addObject(View.TITLE, "Counter Clinic");
        mav.addObject("siteOptions", siteMetaService.fetchAllSiteOptions());
        mav.addObject(View.VIEW_KEY, new View("hello", "support"));
        return mav;
    }
}
