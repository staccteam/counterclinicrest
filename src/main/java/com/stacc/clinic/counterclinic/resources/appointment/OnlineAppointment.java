package com.stacc.clinic.counterclinic.resources.appointment;

import com.stacc.clinic.counterclinic.resources.appointment.calendar.slots.TimeSlot;
import com.stacc.clinic.counterclinic.resources.user.User;
import lombok.Data;
import lombok.extern.log4j.Log4j;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;

@Log4j
@Data
@Document(collection = "online_appointments")
public class OnlineAppointment {

    @Id
    private String id;
    private String patientId;
    @NotNull
    private String selectedDoctorId;
    @NotNull
    private String selectedTimeSlotId;
    @NotNull
    private String selectedDate;
    private long timestamp;

    @Data
    public class Expand
    {
        private User patient;
        private User doctor;
        private TimeSlot slot;
        private String date;

        public Expand patient(User patient)
        {
            this.patient = patient;
            return this;
        }

        public Expand doctor(User doctor)
        {
            this.doctor = doctor;
            return this;
        }

        public Expand timeSlot(TimeSlot timeSlot)
        {
            this.slot = timeSlot;
            return this;
        }

        public Expand date(String date)
        {
            this.date = date;
            return this;
        }
    }
}
