package com.stacc.clinic.counterclinic.resources.appointment.actions;

import com.stacc.clinic.counterclinic.resources.appointment.Appointment;
import lombok.extern.log4j.Log4j;

import java.io.File;

@Log4j
public class OnDeleteAction implements AppointmentAction<Appointment> {

    private Appointment appointmentToDelete;

    public OnDeleteAction(Appointment appointmentToDelete) {
        this.appointmentToDelete = appointmentToDelete;
    }

    public static OnDeleteAction newInstance(Appointment appointmentToDelete) {
        return new OnDeleteAction(appointmentToDelete);
    }

    @Override
    public Appointment execute() {
        File file = new File(appointmentToDelete.getQrCodeImagePath());
        if (file.delete())
            log.debug(String.format("QRCode Image File has been delete for Appointment ID: %s", appointmentToDelete.getId()));
        else
            log.debug(String.format("There is some problem deleting QRCode image for Appointment ID: %s [Image Path=%s]",
                    appointmentToDelete.getId(), appointmentToDelete.getQrCodeImagePath()));
        return appointmentToDelete;
    }
}
