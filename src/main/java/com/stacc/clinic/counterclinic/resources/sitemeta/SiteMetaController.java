package com.stacc.clinic.counterclinic.resources.sitemeta;

import lombok.extern.log4j.Log4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.ws.Response;
import java.util.Optional;

@Log4j
@RestController
@RequestMapping("/api/option")
public class SiteMetaController {

    private SiteMetaService siteMetaService;

    public SiteMetaController(SiteMetaService siteMetaService) {
        this.siteMetaService = siteMetaService;
    }

    @GetMapping
    public ResponseEntity fetchAllSiteOptions()
    {
        return ResponseEntity.ok(siteMetaService.fetchAllSiteOptions());
    }

    @GetMapping("/{key}")
    public ResponseEntity fetchSiteOption(@PathVariable("key")  String key)
    {
        Optional<SiteOption> siteMetaOptional = siteMetaService.fetchSiteOptionByKey(key);
        if  (siteMetaOptional.isPresent())
        {
            log.debug(siteMetaOptional.get());
            return ResponseEntity.ok(siteMetaOptional.get());
        }


        return ResponseEntity.notFound().build();
    }
}
