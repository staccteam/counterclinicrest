package com.stacc.clinic.counterclinic.resources.appointment.actions;

import com.stacc.clinic.counterclinic.resources.appointment.AppointmentCreationFailedException;
import com.stacc.clinic.counterclinic.resources.appointment.OnlineAppointment;
import com.stacc.clinic.counterclinic.resources.appointment.OnlineAppointmentRepository;
import com.stacc.clinic.counterclinic.utils.DateUtil;
import lombok.extern.log4j.Log4j;

@Log4j
public class SaveOnlineAppointment<T> implements AppointmentAction {
    private final OnlineAppointment onlineAppointment;
    private final OnlineAppointmentRepository onlineAppointmentRepository;

    public SaveOnlineAppointment(OnlineAppointment onlineAppointment, OnlineAppointmentRepository onlineAppointmentRepository) {
        this.onlineAppointment = onlineAppointment;
        this.onlineAppointmentRepository = onlineAppointmentRepository;
    }

    @Override
    public OnlineAppointment execute() {
        onlineAppointment.setSelectedDate(DateUtil.convertDateFormatWithSDF(onlineAppointment.getSelectedDate(), DateUtil.ISO8601_FORMAT, DateUtil.YODA_DATE_FORMAT));
        OnlineAppointment createdOnlineAppointment = onlineAppointmentRepository.insert(onlineAppointment);
        if (null == createdOnlineAppointment)
        {
            throw new AppointmentCreationFailedException("Appointment have not been created!");
        }

        return createdOnlineAppointment;
    }
}
