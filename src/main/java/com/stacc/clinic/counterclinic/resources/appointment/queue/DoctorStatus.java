package com.stacc.clinic.counterclinic.resources.appointment.queue;

public enum DoctorStatus {
    INIT,
    LOGGED_IN,
    LOGGED_OUT,
    IN_PROGRESS,
    ON_BREAK,
    NOT_IN_OFFICE,
    ON_LEAVE,
    ARRIVING,
    LEFT_FOR_DAY
    ;

}
