package com.stacc.clinic.counterclinic.resources.appointment;

import com.mongodb.lang.Nullable;
import com.stacc.clinic.counterclinic.resources.clinic.Clinic;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;

@Data
@Document(collection = "appointments")
public class Appointment {
    @Id
    private String id;
    private String appointmentQueueId;
    private Clinic clinic;
    private String doctorId;
    @NotNull
    private String patientName;
    private String doctorName;
    private Long appointmentNumber;
    private String qrCodeName;
    private String qrCodeImagePath;
    private String qrCodeImageUrl;
    private String uniqueToken;
    private String appointmentTime;
    @NotNull
    private AppointmentType appointmentType;
    private Long appointmentTimeStamp;

    public Appointment() {
    }

    public Appointment(Builder builder) {
        this.clinic  = builder.clinic;
        doctorId = builder.doctorId;
        this.patientName  = builder.patientName;
        this.doctorName  =  builder.doctorName;
        this.appointmentType = builder.appointmentType;
        this.appointmentNumber =  builder.appointmentNumber;
        this.qrCodeName = builder.qrCodeName;
        this.qrCodeImagePath = builder.qrCodeImagePath;
        this.qrCodeImageUrl = builder.qrCodeImageUrl;
        this.uniqueToken = builder.uniqueToken;
        this.appointmentTime = builder.appointmentTime;
        this.appointmentTimeStamp  = builder.appointmentTimeStamp;
    }

    public static class Builder  {
        private Clinic clinic;
        private String doctorId;
        private String patientName;
        private String doctorName;
        private Long appointmentNumber;
        private String qrCodeName;
        private String qrCodeImagePath;
        private String qrCodeImageUrl;
        private String uniqueToken;
        private String appointmentTime;
        private AppointmentType appointmentType;
        private Long appointmentTimeStamp;

        public Builder(String doctorId, String patientName,
                       String doctorName, AppointmentType appointmentType)
        {
            this.doctorId  = doctorId;
            this.patientName  =  patientName;
            this.doctorName = doctorName;
            this.appointmentType =  appointmentType;
        }

        public Builder clinic(String clinicId)
        {
            this.clinic = clinic;
            return this;
        }

        public Builder appointmentNumber(Long appointmentNumber)
        {
            this.appointmentNumber = appointmentNumber;
            return this;
        }

        public Builder qrCodeName(String qrCodeName)
        {
            this.qrCodeName  = qrCodeName;
            return this;
        }

        public Builder qrCodeImagePath(String qrCodeImagePath)
        {
            this.qrCodeImagePath = qrCodeImagePath;
            return this;
        }

        public Builder qrCodeImageUrl(String qrCodeImageUrl)
        {
            this.qrCodeImageUrl = qrCodeImageUrl;
            return this;
        }

        public Builder uniqueToken(String uniqueToken)
        {
            this.uniqueToken = uniqueToken;
            return this;
        }

        public Builder appointmentTime(String appointmentTime)
        {
            this.appointmentTime = appointmentTime;
            return this;
        }

        public Builder appointmentTimeStamp(Long appointmentTimeStamp)
        {
            this.appointmentTimeStamp = appointmentTimeStamp;
            return this;
        }

        public Appointment build()
        {
            return new Appointment(this);
        }
    }
}
