package com.stacc.clinic.counterclinic.resources.appointment.queue;

import com.stacc.clinic.counterclinic.resources.appointment.Appointment;
import com.stacc.clinic.counterclinic.resources.appointment.AppointmentForm;
import com.stacc.clinic.counterclinic.resources.appointment.actions.*;
import com.stacc.clinic.counterclinic.resources.user.User;
import com.stacc.clinic.counterclinic.resources.user.UserRepository;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Log4j
@Service
public class AppointmentQueueServiceImpl implements AppointmentQueueService {

    private AppointmentQueueRepository appointmentQueueRepository;
    private UserRepository userRepository;

    @Autowired
    public AppointmentQueueServiceImpl(AppointmentQueueRepository appointmentQueueRepository, UserRepository userRepository) {
        this.appointmentQueueRepository = appointmentQueueRepository;
        this.userRepository = userRepository;
    }

    @Override
    public Appointment onCreateAppointment(AppointmentForm latestAppointmentRequest) {
        log.debug("Appointment Request: " + latestAppointmentRequest);
        Optional<User> user = userRepository.findById(latestAppointmentRequest.getDoctorId());
        List<AppointmentAction> onCreateActions = new ArrayList<>();
        onCreateActions.add(new OnCreateAction(appointmentQueueRepository, latestAppointmentRequest));
        Appointment appointment = (Appointment)  onCreateActions.get(0).execute(); // execute OnCreateAction

        AppointmentAction.getAppointmentQueue(appointmentQueueRepository, user.get())
                .ifPresent(appointmentQueue -> {
                    log.debug("Found appointment queue: " + appointmentQueue.getId());
                    onCreateActions.add(new CalculateAverageCheckupTimeAction(appointmentQueue));
                    onCreateActions.add(new CalculateTotalCheckupTimeAction(appointmentQueue));
                });
        AppointmentQueue appointmentQueue = null;
        for (int i=1; i<onCreateActions.size(); i++)
            appointmentQueue = (AppointmentQueue)  onCreateActions.get(i).execute();

        appointmentQueueRepository.save(appointmentQueue);
        return appointment;
    }

    /**
     * When doctor calls next patient in
     * updates the doctor queue accordingly
     *
     * @param doctor
     * @return
     */
    @Override
    public AppointmentQueue onNextPatientIn(User doctor) {
        List<AppointmentAction> actions = new ArrayList<>();
        AppointmentAction.getAppointmentQueue(appointmentQueueRepository, doctor)
                .ifPresent(appointmentQueue -> {
                    actions.add(new IncrementCurrentQueueCount(appointmentQueue));
                    actions.add(new CalculateAverageCheckupTimeAction(appointmentQueue));
                    actions.add(new CalculateTotalCheckupTimeAction(appointmentQueue));
                    actions.add(new UpdatePatientInTimeAction(appointmentQueue));
                });

        AppointmentQueue appointmentQueue = executeActions(actions);

        appointmentQueueRepository.save(appointmentQueue);

        return appointmentQueue;
    }

    @Override
    public List<AppointmentQueue> fetchAllQueues() {
        return appointmentQueueRepository.findAll();
    }

    @Override
    public Optional<AppointmentQueue> findByDoctorId(String doctorId) {
        Optional<AppointmentQueue> appointmentQueue = appointmentQueueRepository.findByDoctorId(doctorId)
                .stream().findFirst();
        if (appointmentQueue.isPresent())
        {
            AppointmentQueue queueStatus = appointmentQueue.get();
            if (queueStatus.getQueueSize() == queueStatus.getCurrQueueCount())
            {
                queueStatus.setAvgCheckupTime(0L);
            }
            return Optional.of(queueStatus);
        }
        return Optional.empty();
    }

    /**
     * Updates the status of the doctor
     * @param user
     * @param doctorStatus
     * @return
     */
    @Override
    public Optional<AppointmentQueue> updateDoctorStatus(User user, DoctorStatus doctorStatus) {
        Optional<AppointmentQueue> appointmentQueue = findByDoctorId(user.getId());
        appointmentQueue.ifPresent(queue-> {
            queue.setDoctorStatus(doctorStatus);
            appointmentQueueRepository.save(queue);
        });
        return appointmentQueue;
    }

    @Override
    public Optional<AppointmentQueue> takeBreak(User doctor, Long duration) {
        List<AppointmentAction> actions = new ArrayList<>();
        AppointmentAction.getAppointmentQueue(appointmentQueueRepository, doctor)
                .ifPresent(appointmentQueue -> {
                    actions.add(new OnBreakAction(appointmentQueue, duration));
                    actions.add(new CalculateTotalCheckupTimeAction(appointmentQueue));
                    actions.add(new CalculateAverageCheckupTimeAction(appointmentQueue));
                });


        AppointmentQueue appointmentQueue = executeActions(actions);
        appointmentQueueRepository.save(appointmentQueue);
        return Optional.of(appointmentQueue);
    }

    /**
     * Doctor comes back after the break
     * Sets extended time to 0
     *
     * @param doctor
     * @return
     */
    @Override
    public Optional<AppointmentQueue> resumeAfterBreak(User doctor) {
        List<AppointmentAction> actions = new ArrayList<>();

        AppointmentAction.getAppointmentQueue(appointmentQueueRepository, doctor)
                .ifPresent(appointmentQueue -> {
                    actions.add(new OnResumeAction(appointmentQueue));
                    actions.add(new CalculateTotalCheckupTimeAction(appointmentQueue));
                    actions.add(new CalculateAverageCheckupTimeAction(appointmentQueue));
                });

        AppointmentQueue appointmentQueue = executeActions(actions);

        appointmentQueueRepository.save(executeActions(actions));

        return Optional.of(appointmentQueue);
    }

    /**
     * Doctor Clicks on the logout button/end-of-day button
     * change doctor status to LOGGED_OUT
     *
     * @param doctor
     * @return
     */
    @Override
    public Optional<AppointmentQueue> endOfDay(User doctor) {
        List<AppointmentAction> actions = new ArrayList<>();
        AppointmentAction.getAppointmentQueue(appointmentQueueRepository, doctor)
                .ifPresent(appointmentQueue -> {
                    actions.add(new OnEndOfDayAction(appointmentQueue));
                });

        AppointmentQueue appointmentQueue = executeActions(actions);

        appointmentQueueRepository.save(executeActions(actions));

        return Optional.of(appointmentQueue);
    }

    /**
     * Adds average time  to for the doctors queue.
     *
     * @param user
     * @return
     */
    @Override
    public Optional<AppointmentQueue> addAvgTime(User user, Long avgTime) {
        List<AppointmentAction> actions = new ArrayList<>();

        AppointmentAction.getAppointmentQueue(appointmentQueueRepository, user)
                .ifPresent(appointmentQueue -> {
                    actions.add(new AddAverageTimeAction(appointmentQueue, avgTime));
                });

        AppointmentQueue appointmentQueue = executeActions(actions);

        appointmentQueueRepository.save(appointmentQueue);

        return Optional.of(appointmentQueue);
    }

    /**
     * Fetches appointment queue for the given id
     *
     * @param appointmentQueueId
     * @return
     */
    @Override
    public Optional<AppointmentQueue> getAppointmentQueueById(String appointmentQueueId) {
        return appointmentQueueRepository.findById(appointmentQueueId);
    }

    @Override
    public void resetAllQueues() {
        fetchAllQueues()
                .forEach(appointmentQueue -> {
                    appointmentQueue.setAvgCheckupTime(0L);
                    appointmentQueue.setQueueSize(0);
                    appointmentQueue.setCurrQueueCount(0);
                    appointmentQueue.setExtendedTime(0L);
                    appointmentQueue.setTotalCheckupTime(0L);
                    appointmentQueueRepository.save(appointmentQueue);
                });
    }

    @Override
    public void resetQueue(String appointmentQueueId, String doctorId) {
        getAppointmentQueueById(appointmentQueueId)
                .ifPresent(storedQueue->{
                    AppointmentQueue initializedQueue = AppointmentQueue.initForDoctor(doctorId);
                    initializedQueue.setId(storedQueue.getId());
                    appointmentQueueRepository.save(initializedQueue);
                });
    }

    private AppointmentQueue executeActions(List<AppointmentAction> actions) {
        AppointmentQueue appointmentQueue = null;
        for (AppointmentAction action : actions) {
            appointmentQueue = (AppointmentQueue) action.execute();
        }
        return appointmentQueue;
    }
}
