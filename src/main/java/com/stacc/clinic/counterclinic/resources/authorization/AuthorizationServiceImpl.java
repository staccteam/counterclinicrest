package com.stacc.clinic.counterclinic.resources.authorization;

import com.stacc.clinic.counterclinic.resources.user.User;
import com.stacc.clinic.counterclinic.resources.user.UserRepository;
import com.stacc.clinic.counterclinic.utils.TokenManager;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Log4j
@Service
public class AuthorizationServiceImpl implements AuthorizationService {
    private UserRepository userRepository;

    public AuthorizationServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Optional<User> loginUser(LoginUserInput user) {
        log.debug("Trying to search user with credentials: "  + user);
        List<User> savedUsers;
        if (user.isUsernameEmpty()) {
            if (user.isEmailEmpty())
                throw new InvalidLoginCredentialException("Either login with email address of username.");
            else
               savedUsers = userRepository.findByEmail(user.getEmail());
        } else {
            log.debug("Searching user with username" + user.getUsername());
            savedUsers = userRepository.findByUsername(user.getUsername());
        }

        if (savedUsers.isEmpty())
            return Optional.empty();

        log.debug("saved user"  +  savedUsers.get(0).getPassword());
        if (! savedUsers.get(0).getPassword().equals(user.getPassword()))
            return Optional.empty();

        return Optional.of(savedUsers.get(0));
    }

    @Override
    public Optional<User> authorizeUser(String accessToken) {
        Jws<Claims> claims = TokenManager.validateAccessToken(accessToken);
        List<User> users = userRepository.findByUsername(String.valueOf(claims.getBody().get("username")));
        if (users.isEmpty() || users.size() > 1)
            return Optional.empty();
        return Optional.of(users.get(0));
    }
}
