package com.stacc.clinic.counterclinic.resources.appointment.actions;

import com.stacc.clinic.counterclinic.resources.appointment.queue.AppointmentQueue;
import com.stacc.clinic.counterclinic.resources.appointment.queue.DoctorStatus;

public class OnEndOfDayAction implements AppointmentAction<AppointmentQueue> {

    private final AppointmentQueue aq;

    public OnEndOfDayAction(AppointmentQueue aq)
    {
        this.aq = aq;
    }

    @Override
    public AppointmentQueue execute() {
        aq.setDoctorStatus(DoctorStatus.LOGGED_OUT);
        return aq;
    }
}
