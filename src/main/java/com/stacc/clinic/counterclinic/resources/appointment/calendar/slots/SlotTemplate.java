package com.stacc.clinic.counterclinic.resources.appointment.calendar.slots;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "slots_template")
public class SlotTemplate {
    @Id
    private String id;
    private String whatFor;
    private String[] selectedWeekdays;
    private String startTime;
    private String endTime;
    private int slotDuration;
}
