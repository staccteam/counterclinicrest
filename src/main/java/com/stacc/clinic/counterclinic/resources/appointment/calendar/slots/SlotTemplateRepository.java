package com.stacc.clinic.counterclinic.resources.appointment.calendar.slots;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SlotTemplateRepository extends MongoRepository<SlotTemplate, String> {
    Optional<SlotTemplate> findById(String eventMetaId);

    @Override
    List<SlotTemplate> findAll(Sort sort);

    /**
     * Deletes the entity with the given id.
     *
     * @param eventMetaId must not be {@literal null}.
     * @throws IllegalArgumentException in case the given {@code id} is {@literal null}
     */
    @Override
    void deleteById(String eventMetaId);

    /**
     * Deletes all entities managed by the repository.
     */
    @Override
    void deleteAll();

    /**
     * Inserts the given entity. Assumes the instance to be new to be able to apply insertion optimizations. Use the
     * returned instance for further operations as the save operation might have changed the entity instance completely.
     * Prefer using {@link #save(Object)} instead to avoid the usage of store-specific API.
     *
     * @param entity must not be {@literal null}.
     * @return the saved entity
     * @since 1.7
     */
    @Override
    SlotTemplate insert(SlotTemplate entity);

    /**
     * Saves a given entity. Use the returned instance for further operations as the save operation might have changed the
     * entity instance completely.
     *
     * @param entity must not be {@literal null}.
     * @return the saved entity will never be {@literal null}.
     */
    @Override
    SlotTemplate save(SlotTemplate entity);

    /**
     * Find the slot template by whatFor field
     * @param whatFor
     * @return
     */
    List<SlotTemplate> findByWhatFor(String whatFor);

    @Override
    List<SlotTemplate> findAll();
}
