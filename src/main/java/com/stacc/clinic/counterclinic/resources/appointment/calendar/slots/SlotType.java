package com.stacc.clinic.counterclinic.resources.appointment.calendar.slots;

public enum SlotType {
    OPEN_SLOTS,
    CLOSED_SLOTS
}
