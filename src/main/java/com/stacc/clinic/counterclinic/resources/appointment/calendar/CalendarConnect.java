package com.stacc.clinic.counterclinic.resources.appointment.calendar;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.services.calendar.CalendarScopes;

import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class CalendarConnect {
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final Set<String> SCOPES = CalendarScopes.all();

    public static Credential getCredentials()
    {
        InputStream in;
        GoogleClientSecrets clientSecrets;
        Credential credential = null;
        try {
            in = new ClassPathResource("counterclinic-1bf102464598.json").getInputStream();
            credential = GoogleCredential
                    .fromStream(in)
                    .createScoped(SCOPES);
            credential.refreshToken();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return credential;
    }
}
