package com.stacc.clinic.counterclinic.resources.authorization;

import com.stacc.clinic.counterclinic.resources.user.User;
import com.stacc.clinic.counterclinic.utils.TokenManager;
import jdk.nashorn.internal.parser.Token;
import lombok.extern.log4j.Log4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.Option;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.stacc.clinic.counterclinic.utils.TokenManager.createAccessToken;

@Log4j
@RestController
@CrossOrigin
@RequestMapping("/api/auth")
public class AuthController {

    private AuthorizationService authService;

    public AuthController(AuthorizationService authService) {
        this.authService = authService;
    }

    @PostMapping("/login")
    public ResponseEntity checkUserCredentialsAndGenerateAuthToken(@RequestBody @Valid LoginUserInput user,
                                                                   BindingResult  binding)  {

        if (binding.hasErrors())
            return ResponseEntity.badRequest().body("Invalid request!");

        Optional<User> authUser = authService.loginUser(user);

        if  (!authUser.isPresent())
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid Username or Password.");

        Map<String, Object> accessTokenAndRefreshToken = new HashMap<>();
        accessTokenAndRefreshToken.put("accessToken", createAccessToken(authUser.get()));
        accessTokenAndRefreshToken.put("refreshToken", TokenManager.createRefreshToken(authUser.get()));
        return ResponseEntity.ok(accessTokenAndRefreshToken);
    }

    @GetMapping()  //  [/auth]
    public ResponseEntity<User> authUser(@RequestParam("access_token") String accessToken) {
        log.debug(String.format("Access Token: %s", accessToken));

        if (StringUtils.isEmpty(accessToken))
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);

        Optional<User> user = authService.authorizeUser(accessToken);

        if (! user.isPresent())
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);

        return ResponseEntity.ok(user.get());
    }
}
