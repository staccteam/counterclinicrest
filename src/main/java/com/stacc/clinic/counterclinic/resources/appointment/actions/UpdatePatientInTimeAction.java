package com.stacc.clinic.counterclinic.resources.appointment.actions;

import com.stacc.clinic.counterclinic.resources.appointment.queue.AppointmentQueue;

public class UpdatePatientInTimeAction implements AppointmentAction<AppointmentQueue> {

    private AppointmentQueue appointmentQueue;

    public UpdatePatientInTimeAction(AppointmentQueue appointmentQueue) {
        this.appointmentQueue = appointmentQueue;
    }

    @Override
    public AppointmentQueue execute() {
        appointmentQueue.setPatientInTimeStamp(System.currentTimeMillis());
        return appointmentQueue;
    }
}
