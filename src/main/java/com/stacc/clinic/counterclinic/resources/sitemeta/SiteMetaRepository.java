package com.stacc.clinic.counterclinic.resources.sitemeta;

import com.stacc.clinic.counterclinic.resources.user.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SiteMetaRepository extends MongoRepository<SiteOption, String> {

    @Override
    List<SiteOption> findAll();

    @Override
    SiteOption insert(SiteOption siteOption);

    @Override
    SiteOption save(SiteOption siteOption);

    @Override
    void deleteAll();

    List<SiteOption> findByKey(String key);
}
