package com.stacc.clinic.counterclinic.resources.appointment.calendar;

import com.google.api.services.calendar.model.Calendar;
import com.google.api.services.calendar.model.Event;
import com.stacc.clinic.counterclinic.resources.user.UserService;
import com.stacc.clinic.counterclinic.utils.DateUtil;
import com.stacc.clinic.counterclinic.utils.SiteConfig;
import lombok.extern.log4j.Log4j;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

@Log4j
public class CalendarUtil {

    /**
     * Converts List of Event to EventModelOutput List
     * @param events
     * @param userService
     * @return
     */
    public static List<EventModelOutput> convertListOfEventsToEventModelOutput(List<Event> events, UserService userService)
    {
        List<EventModelOutput> outputs = new ArrayList<>();
        events.forEach(event -> {
            EventModelOutput eventModelOutput = new EventModelOutput();
            eventModelOutput.setEventId(event.getId());
            eventModelOutput.setSummary(event.getSummary());
            eventModelOutput.setStartTime(event.getStart().setTimeZone(TimeZone.getTimeZone(SiteConfig.get(SiteConfig.SITE_SETTINGS).get(SiteConfig.SITE_TIMEZONE)).getID()).getDateTime().getValue());
            eventModelOutput.setEndTime(event.getEnd().setTimeZone(TimeZone.getTimeZone(SiteConfig.get(SiteConfig.SITE_SETTINGS).get(SiteConfig.SITE_TIMEZONE)).getID()).getDateTime().getValue());
            eventModelOutput.setPatient(userService.getUserByEmail(event.getAttendees().get(1).getEmail()).get());
            eventModelOutput.setDoctor(userService.getUserByEmail(event.getAttendees().get(0).getEmail()).get());
            eventModelOutput.setEventUrl(event.getHtmlLink());
            log.debug(String.format("Event Details\n%s",eventModelOutput));
            outputs.add(eventModelOutput);
        });

        return outputs;
    }

    public static CalendarModel convertGoogleCalendarToCalendarModel(Calendar gCalendar)
    {
        CalendarModel calendar = new CalendarModel();
        calendar.setSummary(gCalendar.getSummary());
        calendar.setKind(gCalendar.getKind());
        calendar.setCalendarId(gCalendar.getId());
        return calendar;
    }
}
