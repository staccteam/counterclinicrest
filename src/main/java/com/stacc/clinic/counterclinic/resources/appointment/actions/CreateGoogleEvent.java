package com.stacc.clinic.counterclinic.resources.appointment.actions;

import com.google.api.services.calendar.model.Event;
import com.stacc.clinic.counterclinic.resources.appointment.AppointmentCreationFailedException;
import com.stacc.clinic.counterclinic.resources.appointment.OnlineAppointment;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.CalendarService;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.EventModel;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.slots.TimeSlot;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.slots.TimeSlotService;
import com.stacc.clinic.counterclinic.resources.user.User;
import com.stacc.clinic.counterclinic.resources.user.UserService;
import com.stacc.clinic.counterclinic.utils.DateUtil;

import java.util.Optional;

public class CreateGoogleEvent<T> implements AppointmentAction {

    private final OnlineAppointment onlineAppointment;
    private final TimeSlotService timeSlotService;
    private final UserService userService;
    private final CalendarService calendarService;

    public CreateGoogleEvent(OnlineAppointment onlineAppointment, TimeSlotService timeSlotService, UserService userService, CalendarService calendarService) {
        this.onlineAppointment = onlineAppointment;
        this.timeSlotService = timeSlotService;
        this.userService = userService;
        this.calendarService = calendarService;
    }

    @Override
    public Event execute() {

        Optional<TimeSlot> timeSlot = timeSlotService.getTimeSlot(onlineAppointment.getSelectedTimeSlotId());
        if (!timeSlot.isPresent())
            throw new AppointmentCreationFailedException("No Time Slot available for the given id" + onlineAppointment.getSelectedTimeSlotId());

        Optional<User> patient = userService.getUserById(onlineAppointment.getPatientId());
        Optional<User> doctor = userService.getUserById(onlineAppointment.getSelectedDoctorId());

        String date = DateUtil.convertDateFormatWithSDF(onlineAppointment.getSelectedDate(), DateUtil.ISO8601_FORMAT, DateUtil.YODA_DATE_FORMAT);
        String time = DateUtil.convertDateFormatWithSDF(timeSlot.get().getStartTime(), DateUtil.TIMESLOT_DATETIME_FORMAT, DateUtil.YODA_TIME_FORMAT);
        String startDateTime = date + " " +  time;
        String endDateTime = date + " " + DateUtil.convertDateFormatWithSDF(timeSlot.get().getEndTime(), DateUtil.TIMESLOT_DATETIME_FORMAT, DateUtil.YODA_TIME_FORMAT);

        // create appointment in google calendar
        Event event = calendarService.createEvent(
                new EventModel()
                        .calendarId(patient.get().getCalendar().getCalendarId())
                        .startTime(DateUtil.parseDate(startDateTime, DateUtil.YODA_DATE_TIME_FORMAT_24Hour).getTime())
                        .endTime(DateUtil.parseDate(endDateTime, DateUtil.YODA_DATE_TIME_FORMAT_24Hour).getTime())
                        .summary(String.format("Counter Clinic Appointment Alert!"))
                        .description(String.format("Appointment Details\nDoctor Name: %s %s\nAppointment Date: %s\nAppointment Time: %s",
                                doctor.get().getFirstName(),
                                doctor.get().getLastName(),
                                date,
                                time
                        ))
                        .patientEmail(patient.get().getEmail())
                        .doctorEmail(doctor.get().getEmail())
        );
        return event;
    }
}
