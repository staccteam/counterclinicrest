package com.stacc.clinic.counterclinic.resources.authorization;

import com.stacc.clinic.counterclinic.resources.user.User;
import lombok.Getter;

import java.util.Optional;

public class InvalidTokenException extends RuntimeException {

    @Getter private Optional<User> user;

    public InvalidTokenException()
    {
        super();
    }

    public InvalidTokenException(String message)
    {
        super(message);
    }

    public InvalidTokenException(String msg, Optional<User> user) {
        super(msg);
        this.user = user;
    }
}
