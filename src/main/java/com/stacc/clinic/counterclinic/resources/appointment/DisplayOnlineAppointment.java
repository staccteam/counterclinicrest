package com.stacc.clinic.counterclinic.resources.appointment;

import lombok.Data;

@Data
public class DisplayOnlineAppointment {
    private String patientName;
    private String doctorName;
    private String appointmentDate;
    private String appointmentTime;
}
