package com.stacc.clinic.counterclinic.resources.appointment;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OnlineAppointmentRepository extends MongoRepository<OnlineAppointment, String> {
    Optional<OnlineAppointment> findById(String appointmentId);

    @Override
    List<OnlineAppointment> findAll(Sort sort);

    List<OnlineAppointment> findAllByOrderByTimestamp();

    OnlineAppointment insert(OnlineAppointment appointment);

    OnlineAppointment save(OnlineAppointment appointment);

    /**
     * Deletes the entity with the given id.
     *
     * @param appointmentId must not be {@literal null}.
     * @throws IllegalArgumentException in case the given {@code id} is {@literal null}
     */
    @Override
    void deleteById(String appointmentId);

    /**
     * Deletes all entities managed by the repository.
     */
    @Override
    void deleteAll();

    List<OnlineAppointment> findBySelectedDoctorId(String doctorId);

    List<OnlineAppointment> findByPatientId(String userId);

}
