package com.stacc.clinic.counterclinic.resources.appointment.queue;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface AppointmentQueueRepository  extends MongoRepository<AppointmentQueue, String> {

    Optional<AppointmentQueue> findById(String id);

    List<AppointmentQueue> findByDoctorId(String doctorId);

    AppointmentQueue insert(AppointmentQueue appointmentQueue);

    AppointmentQueue save(AppointmentQueue appointmentQueue);
}
