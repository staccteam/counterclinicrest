package com.stacc.clinic.counterclinic.resources.appointment;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class AppointmentForm {
    private String clinicId;
    private String doctorId;
    @NotNull
    private String patientName;
    @NotNull
    private AppointmentType appointmentType;
}
