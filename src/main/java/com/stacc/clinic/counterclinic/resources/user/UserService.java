package com.stacc.clinic.counterclinic.resources.user;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;
import java.util.Optional;

/**
 * @author Varun Shrivastava
 * 13th October, 2018
 */
@Service
public interface UserService {
    /**
     * Retrieves all user information from the database
     * @return List of all users.
     */
    List<User> getAllUsers();

    /**
     * Fetches the user information based on theid passed
     * @param userId userid of the user
     * @return Fetch user by the given user id
     */
    Optional<User> getUserById(String userId);

    /**
     * Performs logic to create new user in the database
     * @param user create new user entry into the database
     */
    User createNewUser(User user);

    /**
     * Delets user from the database
     * @param userId
     */
    void removeUser(String userId);
    /**
     * Performs the logic to update the user details into the database
     * @param user updates user information into the database
     */
    User updateUser(User user);

    /**
     * Retrieve the user on the basis of username
     * @param username
     * @return
     */
    Optional<User> getUserByUsername(String username);

    /**
     * Fetch the user object by its email
     * @param doctorEmail
     * @return
     */
    Optional<User> getUserByEmail(String doctorEmail);

    /**
     * Creates new  user from the registered information
     * @param userRegistrationForm
     * @return
     */
    User registerNewUser(UserRegistrationForm userRegistrationForm);

    Optional<File> uploadDisplayPicture(MultipartFile file, String uploadFolder, User user);

    void removeAllUsers();
}
