package com.stacc.clinic.counterclinic.resources.dashboard;

import com.stacc.clinic.counterclinic.resources.appointment.queue.AppointmentQueue;
import com.stacc.clinic.counterclinic.resources.user.User;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface DashboardService {

    List<User> getAllDoctors();

    /**
     * Fetches the queue information
     * of all the doctors that is
     * stored in the database
     * @return
     */
    List<AppointmentQueue> getQueueStatus();
}
