package com.stacc.clinic.counterclinic.resources.dashboard;

import com.stacc.clinic.counterclinic.resources.appointment.queue.AppointmentQueue;
import com.stacc.clinic.counterclinic.resources.appointment.queue.AppointmentQueueService;
import com.stacc.clinic.counterclinic.resources.user.User;
import com.stacc.clinic.counterclinic.resources.user.UserRoles;
import com.stacc.clinic.counterclinic.resources.user.UserService;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Log4j
@Service
public class DashboardServiceImpl implements DashboardService {

    private UserService userService;
    private AppointmentQueueService appointmentQueueService;

    @Autowired
    public DashboardServiceImpl(UserService userService, AppointmentQueueService  appointmentQueueService) {
        this.userService = userService;
        this.appointmentQueueService = appointmentQueueService;
    }

    @Override
    public List<User> getAllDoctors() {
        List<User> users = userService.getAllUsers();
        return users.stream().filter( user ->
                user.getRoles().stream().anyMatch( role ->
                        role.getName().equals(UserRoles.DOCTOR))).collect(Collectors.toList());
    }

    /**
     * Fetches the queue information
     * of all the doctors that is
     * stored in the database
     *
     * @return
     */
    @Override
    public List<AppointmentQueue> getQueueStatus() {
        return appointmentQueueService.fetchAllQueues();
    }
}
