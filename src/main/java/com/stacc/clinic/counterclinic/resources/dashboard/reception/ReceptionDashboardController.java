package com.stacc.clinic.counterclinic.resources.dashboard.reception;

import com.stacc.clinic.counterclinic.resources.appointment.queue.AppointmentQueue;
import com.stacc.clinic.counterclinic.resources.dashboard.DashboardService;
import com.stacc.clinic.counterclinic.resources.user.User;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.ws.Response;
import java.util.List;

@Log4j
@RestController
@CrossOrigin
@RequestMapping("/api/reception")
public class ReceptionDashboardController {

    private DashboardService dashboardService;

    @Autowired
    public ReceptionDashboardController(DashboardService dashboardService) {
        this.dashboardService = dashboardService;
    }

    @GetMapping("/all-doctors")
    public ResponseEntity getAllDoctors() {
        List<User> doctors = dashboardService.getAllDoctors();

        return ResponseEntity.ok(doctors);
    }

    @GetMapping("/all-doctors-queue")
    public ResponseEntity getDoctorsQueueInfo() {
        List<AppointmentQueue> queues = dashboardService.getQueueStatus();
        return ResponseEntity.ok(queues);
    }
}
