package com.stacc.clinic.counterclinic.resources.sitemeta;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "site_options")
public class SiteOption {
    @Id
    private String id;
    private String key;
    private String val;

    public SiteOption() {}

    public SiteOption(String key, String val) {
        this.key = key;
        this.val = val;
    }

    public static SiteOption newInstance(String key, String val)
    {
        return new SiteOption(key, val);
    }
}
