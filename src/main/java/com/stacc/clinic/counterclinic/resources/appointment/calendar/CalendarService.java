package com.stacc.clinic.counterclinic.resources.appointment.calendar;

import com.google.api.services.calendar.model.Calendar;
import com.google.api.services.calendar.model.CalendarListEntry;
import com.google.api.services.calendar.model.Event;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.slots.TimeSlot;
import com.stacc.clinic.counterclinic.resources.user.User;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface CalendarService {

    public Calendar createCalendar(Calendar calendar);

    public boolean deleteCalendar(String calendarId);

    public Calendar getCalendar(String calendarId);

    public List<CalendarListEntry> getAllCalendars();

    public Event createEvent(String calendarId, Event event);

    /**
     * Creates appointment from the user end.
     * @param event
     * @return
     */
    public Event createEvent(EventModel event);

    public boolean deleteEvent(String calendarId, String eventId);

    public Event getEvent(String calendarId, String eventId);

    public List<Event> getAllEvents(String calendarId);

    /**
     * Update/modify existing slots from user request
     * @param event
     * @return
     */
    public Event updateEvent(EventModel event);

    /**
     * Fetch all the available time slots for the doctor
     * @param doctor
     * @param iso8601Date
     * @return
     */
    List<TimeSlot> fetchEmptySlots(Optional<User> doctor, String iso8601Date);
}
