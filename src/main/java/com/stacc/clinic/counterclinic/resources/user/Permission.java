package com.stacc.clinic.counterclinic.resources.user;

import lombok.Data;

@Data
public class Permission {
    private String name;

    public Permission(){}

    public Permission(String name) {
        this.name = name;
    }

    public static Permission newInstance(String name)  {
        return new Permission(name);
    }
}
