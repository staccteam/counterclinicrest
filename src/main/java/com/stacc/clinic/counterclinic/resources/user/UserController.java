package com.stacc.clinic.counterclinic.resources.user;

import lombok.extern.log4j.Log4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.xml.ws.Response;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Varun Shrivastava
 * 13th Oct, 2018
 */
@Log4j
@RestController
@RequestMapping("/api/user")
public class UserController {

    private UserService service;

    public UserController(UserService service) {
        this.service = service;
    }

    @GetMapping
    public List<User> getAllUsers()  {
        return service.getAllUsers();
    }

    @GetMapping("/doctors")
    public List<User> getAllDoctors()
    {
        List<User> doctors = service.getAllUsers().stream()
                .filter(u-> u.getRoles().stream().anyMatch(role ->
                        role.getName().equals(UserRoles.DOCTOR)))
                .collect(Collectors.toList());
        return doctors;
    }
    @GetMapping("/{id}")
    public User getUserById(@PathVariable("id") String id)  {
        Optional<User> user =  service.getUserById(id);
        return user.orElse(User.newInstance());
    }

    @PostMapping
    public ResponseEntity<User> insertUserIntoDatabase(@RequestBody @Valid User user, BindingResult binding)  {
        if (binding.hasErrors())
            ResponseEntity.badRequest().body("Invalid Data!");

        return ResponseEntity.ok(service.createNewUser(user));
    }

    @PutMapping
    public ResponseEntity<User> updateUserIntoDatabase(@RequestBody User user) {
        return ResponseEntity.ok(service.updateUser(user));
    }

}
