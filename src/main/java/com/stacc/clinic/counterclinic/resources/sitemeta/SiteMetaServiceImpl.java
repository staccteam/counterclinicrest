package com.stacc.clinic.counterclinic.resources.sitemeta;

import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Optional;

@Log4j
@Service
public class SiteMetaServiceImpl  implements SiteMetaService {

    private SiteMetaRepository siteMetaRepository;

    public SiteMetaServiceImpl(SiteMetaRepository siteMetaRepository) {
        this.siteMetaRepository = siteMetaRepository;
    }

    @Override
    public List<SiteOption> fetchAllSiteOptions() {
        return siteMetaRepository.findAll();
    }

    /**
     * Find Site Option by the provide key
     *
     * @param key
     */
    @Override
    public Optional<SiteOption> fetchSiteOptionByKey(String key) {
        List<SiteOption> siteOptions = siteMetaRepository.findByKey(key);
        if (siteOptions.isEmpty())
            return  Optional.empty();

        siteOptions.forEach(siteOption -> {
            log.debug("Site Options: " + siteOption.getKey()  +  ":" + siteOption.getVal());
        });
        return  siteOptions.stream().findFirst();
    }

    @Override
    public SiteOption createNewSiteMetaOption(SiteOption option) {
        SiteOption savedOption = fetchSiteOptionByKey(option.getKey())
                .map(siteOption -> {
                    siteOption.setVal(option.getVal());
                    return siteMetaRepository.save(siteOption);
                })
                .orElseGet(()->{
                    return siteMetaRepository.insert(option);
                });
        return savedOption;
    }

    @Override
    public void deleteSiteMetaOption(String optionId) {
        siteMetaRepository.deleteById(optionId);
    }
}
