package com.stacc.clinic.counterclinic.resources.appointment.calendar;

import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.model.*;
import com.google.api.services.calendar.model.Calendar;
import com.stacc.clinic.counterclinic.resources.appointment.AppointmentService;
import com.stacc.clinic.counterclinic.resources.appointment.OnlineAppointment;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.slots.TimeSlot;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.slots.TimeSlotService;
import com.stacc.clinic.counterclinic.resources.user.User;
import com.stacc.clinic.counterclinic.resources.user.UserDoesNotExistsException;
import com.stacc.clinic.counterclinic.resources.user.UserService;
import com.stacc.clinic.counterclinic.utils.DateUtil;
import com.stacc.clinic.counterclinic.utils.SiteConfig;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.util.*;

@Log4j
@Service
public class CalendarServiceImpl implements CalendarService {

    private final AppointmentService appointmentService;
    private com.google.api.services.calendar.Calendar gCalendarService;
    private UserService userService;
    private TimeSlotService timeSlotService;

    @Autowired
    public CalendarServiceImpl(com.google.api.services.calendar.Calendar gCalendarService, UserService userService, TimeSlotService timeSlotService, AppointmentService appointmentService) {
        this.gCalendarService = gCalendarService;
        this.userService = userService;
        this.timeSlotService = timeSlotService;
        this.appointmentService = appointmentService;
    }

    @Override
    public Calendar createCalendar(Calendar calendar) {
        Calendar createdCalendar = null;
        try {
            createdCalendar = gCalendarService.calendars().insert(calendar).execute();
        } catch (IOException e) {
            throw new CalendarException("Error creating calendar");
        }
        return createdCalendar;
    }

    @Override
    public boolean deleteCalendar(String calendarId) {
        boolean flag = true;
        try {
            gCalendarService.calendars().delete(calendarId).execute();
        } catch (IOException e) {
            flag = false;
            throw new CalendarException("Error Deleting Calendar");
        }
        return flag;
    }

    @Override
    public Calendar getCalendar(String calendarId) {
        Calendar calendar = null;
        try {
            calendar = gCalendarService.calendars()
                    .get(calendarId)
                    .execute()
                    .setTimeZone(TimeZone.getTimeZone(SiteConfig.get(SiteConfig.SITE_SETTINGS).get(SiteConfig.SITE_TIMEZONE)).getID());
        } catch (IOException e) {
            throw new CalendarException(String.format("Error getting calendar (%s).\nError Message: %s",
                    calendarId,
                    e.getMessage()));
        }
        return calendar;
    }

    @Override
    public List<CalendarListEntry> getAllCalendars() {
        List<CalendarListEntry> calendarListEntries;
        try {
            calendarListEntries = gCalendarService.calendarList().list().execute().getItems();
        } catch (IOException e) {
            throw new CalendarException(String.format("\"Error Listing Calendars.\nError  Message: %s", e.getMessage()));
        }
        return calendarListEntries;
    }

    @Override
    public Event createEvent(String calendarId, Event event) {
        Event createEvent = null;
        try {
            createEvent = gCalendarService.events().insert(calendarId, event).execute();
        } catch (IOException e) {
            throw new CalendarException(
                    String.format("Error creating slots into calendar with ID(%s).\nError Message: %s",
                            calendarId, e.getMessage()));
        }
        return createEvent;
    }

    /**
     * Creates appointment from the user end.
     *
     * @param event
     * @return
     */
    @Override
    public Event createEvent(EventModel event) {
        Optional<User> optDoctor = userService.getUserByEmail(event.getDoctorEmail());
        if (!optDoctor.isPresent())
            throw new UserDoesNotExistsException(
                    String.format("User with email (%s) does not exists.", event.getDoctorEmail()));

        User doctor = optDoctor.get();

        Event gEvent = new Event()
                .setSummary(String.format("Counter Clinic Appointment with Dr. %s %s",
                        doctor.getFirstName(), doctor.getLastName()))
                .setDescription(event.getDescription())
                .set("doctor", doctor)
                .setStart(new EventDateTime()
                    .setDateTime(
                            new DateTime(
                                    DateUtil.convertDateToIsoOffsetFormat(
                                            Instant.ofEpochMilli(event.getStartTime()).atZone(ZoneId.systemDefault()).toLocalDateTime())
                            )
                    ).setTimeZone(event.getTimezone())
                )
                .setEnd(new EventDateTime()
                    .setDateTime(
                            new DateTime(
                                    DateUtil.convertDateToIsoOffsetFormat(
                                            Instant.ofEpochMilli(event.getEndTime()).atZone(ZoneId.systemDefault()).toLocalDateTime())
                            )
                    ).setTimeZone(event.getTimezone())
                )
                .setAttendees(Arrays.asList(
                        new EventAttendee[]{
                                new EventAttendee().setEmail(event.getPatientEmail()),
                                new EventAttendee().setEmail(event.getDoctorEmail())
                        }
                ))
                .setReminders(new Event.Reminders()
                        .setUseDefault(false)
                        .setOverrides(Arrays.asList(
                                new EventReminder[]{
                                        new EventReminder().setMethod("email").setMinutes(24*60),
                                        new EventReminder().setMethod("popup").setMinutes(10)
                                }
                        ))
                )
                .setGuestsCanInviteOthers(false)
                .setGuestsCanModify(false)
                .setOrganizer(new Event.Organizer().setEmail(doctor.getEmail()));
        Event createdEvent = null;

        log.debug(gEvent);
        try {
            createdEvent = gCalendarService.events().insert(event.getCalendarId(), gEvent).execute();
        } catch (IOException e) {
            log.error("Error Creating event in gCalendar.",  e);
            throw new CalendarException(
                    String.format("Error creating slots with calendar ID: %s", event.getCalendarId()));
        }

        return createdEvent;
    }

    @Override
    public boolean deleteEvent(String calendarId, String eventId) {
        boolean flag = true;
        try {
            gCalendarService.events().delete(calendarId, eventId).execute();
        } catch (IOException e) {
            flag = false;
            throw new CalendarException(String.format("Error deleting slots from Calendar(%s) with ID(%s).\nError Message: %s",
                    calendarId,
                    eventId,
                    e.getMessage()));
        }
        return flag;
    }

    @Override
    public Event getEvent(String calendarId, String eventId) {
        Event event = null;
        try {
            event = gCalendarService.events().get(calendarId, eventId).execute();
        } catch (IOException e) {
            throw new CalendarException(String.format("Error Retreiving Event from Calendar(%s) with Event(%s).\nError Message: %s",
                    calendarId,
                    eventId,
                    e.getMessage()));
        }
        return event;
    }

    @Override
    public List<Event> getAllEvents(String calendarId) {
        List<Event> events = null;
        try {
            events = gCalendarService.events().list(calendarId).execute().getItems();
        } catch (IOException e) {
            throw new CalendarException(String.format("Error retrieving all events from calendar(%s).\nError Message: %s",
                    calendarId,
                    e.getMessage()));
        }
        return events;
    }

    /**
     * Update/modify existing slots from user request
     *
     * @param event
     * @return
     */
    @Override
    public Event updateEvent(EventModel event) {
        Event existingEvent = null;
        try {
            existingEvent = gCalendarService.events().get(event.getCalendarId(), event.getEventId()).setTimeZone(event.getTimezone()).execute();
        } catch (IOException e) {
            log.info(String.format(
                    "Error retrieving existing slots  with slots id(%s) and calendar id(%s)",
                    event.getEventId(), event.getCalendarId()
            ));
        }

        // update only  when required
        if (!StringUtils.isEmpty(event.getSummary()))
            existingEvent.setSummary(event.getSummary());
        if (!StringUtils.isEmpty(event.getPatientEmail()))
            existingEvent.getAttendees().set(0, new EventAttendee().setEmail(event.getPatientEmail()));
        if (!StringUtils.isEmpty(event.getDoctorEmail()))
            existingEvent.getAttendees().set(1, new EventAttendee().setEmail(event.getDoctorEmail()));
        if (!StringUtils.isEmpty(event.getStartTime()))
        {
            existingEvent.setStart(
                    new EventDateTime().setDateTime(
                            new DateTime(
                                    DateUtil.convertDateToIsoOffsetFormat(
                                            Instant.ofEpochMilli(event.getStartTime()).atZone(ZoneId.systemDefault()).toLocalDateTime()
                                    )
                            )
                    ).setTimeZone(event.getTimezone())
            );
        }
        if (!StringUtils.isEmpty(event.getEndTime()))
        {
            existingEvent.setEnd(new EventDateTime()
                    .setDateTime(
                            new DateTime(
                                    DateUtil.convertDateToIsoOffsetFormat(
                                            Instant.ofEpochMilli(event.getEndTime()).atZone(ZoneId.systemDefault()).toLocalDateTime())
                            )
                    )
                    .setTimeZone(event.getTimezone())
            );
        }

        Event newEvent = new Event()
                .setSummary(event.getSummary())
                .setStart(new EventDateTime()
                        .setDateTime(
                                new DateTime(
                                        DateUtil.convertDateToIsoOffsetFormat(
                                                Instant.ofEpochMilli(event.getStartTime()).atZone(ZoneId.systemDefault()).toLocalDateTime()
                                        )
                                )
                        )
                        .setTimeZone(event.getTimezone()))
                .setEnd(new EventDateTime()
                        .setDateTime(
                                new DateTime(
                                        DateUtil.convertDateToIsoOffsetFormat(
                                                Instant.ofEpochMilli(event.getEndTime()).atZone(ZoneId.systemDefault()).toLocalDateTime())
                                )
                        )
                        .setTimeZone(event.getTimezone())
                )
                .setAttendees(
                       Arrays.asList(
                               new EventAttendee[]{
                                       new EventAttendee().setEmail(event.getPatientEmail()),
                                       new EventAttendee().setEmail(event.getDoctorEmail())
                               }
                       )
                );
        try {
            existingEvent = gCalendarService.events().patch(event.getCalendarId(), event.getEventId(), newEvent).execute();
        } catch (IOException e) {
            throw new CalendarException(String.format(
                    "Error modifying slots from calendar(%s) and slots Id (%s)\nError Message: (%s)",
                    event.getCalendarId(), event.getEventId(), e.getMessage()
            ));
        }
        return existingEvent;
    }

    /**
     * Fetch all the available time slots for the doctor
     *
     * @param doctor
     * @param iso8601Date
     * @return
     */
    @Override
    public List<TimeSlot> fetchEmptySlots(Optional<User> doctor, String iso8601Date) {
        List<OnlineAppointment> doctorAppointments = appointmentService.fetchOnlineAppointmentsByUserId(doctor.get().getId());

        List<TimeSlot> slots = this.timeSlotService.getAllSlots();
        List<TimeSlot> emptyTimeSlots = new ArrayList<>();

        for (TimeSlot slot : slots) {

            /*for (OnlineAppointment doctorAppointment : doctorAppointments) {
                if (doctorAppointment.getSelectedTimeSlotId().equals(slot.getId())
                        && doctorAppointment.getSelectedDate().equals(
                                DateUtil.convertDateFormatWithSDF(iso8601Date, DateUtil.ISO8601_FORMAT, DateUtil.YODA_DATE_FORMAT)))
                    continue;
            }*/

            if (doctorAppointments.stream().anyMatch(
                    onlineAppointment ->
                            onlineAppointment.getSelectedTimeSlotId().equals(slot.getId())
                                    && onlineAppointment.getSelectedDate().equals(
                                            DateUtil.convertDateFormatWithSDF(iso8601Date, DateUtil.ISO8601_FORMAT, DateUtil.YODA_DATE_FORMAT) )))
                continue;

            emptyTimeSlots.add(slot);
        }
        return emptyTimeSlots;
    }


}
