package com.stacc.clinic.counterclinic.resources.appointment;

import com.google.api.services.calendar.model.Event;
import com.google.zxing.qrcode.QRCodeWriter;
import com.stacc.clinic.counterclinic.resources.appointment.actions.*;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.CalendarService;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.CalendarUtil;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.EventModelOutput;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.slots.TimeSlot;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.slots.TimeSlotService;
import com.stacc.clinic.counterclinic.resources.appointment.queue.AppointmentQueueService;
import com.stacc.clinic.counterclinic.resources.sitemeta.SiteMetaService;
import com.stacc.clinic.counterclinic.resources.user.User;
import com.stacc.clinic.counterclinic.resources.user.UserService;
import com.stacc.clinic.counterclinic.utils.QRCodeException;
import com.stacc.clinic.counterclinic.utils.QRCodeUtil;
import com.stacc.clinic.counterclinic.utils.SiteConfig;
import com.stacc.clinic.counterclinic.utils.TokenManager;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.File;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Log4j
@PropertySource("classpath:application.properties")
@Service
public class AppointmentServiceImpl implements AppointmentService {

    private OnlineAppointmentRepository  onlineAppointmentRepository;
    private AppointmentRepository appointmentRepository;
    private AppointmentQueueService appointmentQueueService;
    private UserService userService;

    @Autowired @Lazy
    private CalendarService calendarService;

    @Autowired @Lazy
    private TimeSlotService timeSlotService;

    private SiteMetaService siteMetaService;

    @Value("${qrcode.folder.path}")
    String qrCodeFolderPath;

    @Value("${qrcode.uri}")
    String qrCodeUri;

    @Autowired
    public AppointmentServiceImpl(
            OnlineAppointmentRepository onlineAppointmentRepository,
            AppointmentRepository appointmentRepository,
            AppointmentQueueService  appointmentQueueService,
            UserService userService
    )
    {
        this.onlineAppointmentRepository = onlineAppointmentRepository;
        this.appointmentRepository = appointmentRepository;
        this.appointmentQueueService =  appointmentQueueService;
        this.userService = userService;
    }

    @Autowired @Lazy
    public void setSiteMetaService(SiteMetaService siteMetaService)
    {
        this.siteMetaService = siteMetaService;
    }


    @Override
    public Optional<Appointment> createNewAppointment(AppointmentForm appointmentForm) {
        final String CURR_TIME_IN_MILLIS = String.valueOf(System.currentTimeMillis());
        Optional<User> doctor = userService.getUserById(appointmentForm.getDoctorId());
        if (!doctor.isPresent())
            throw new AppointmentCreationFailedException("Invalid doctor selected!");

        Appointment appointment = this.appointmentQueueService.onCreateAppointment(appointmentForm); // performs needed actions on new appointment
        appointment.setPatientName(appointmentForm.getPatientName());
        appointment.setDoctorId(appointmentForm.getDoctorId());
        appointment.setQrCodeName(String.format("%s.%s", CURR_TIME_IN_MILLIS, "png"));
        appointment.setQrCodeImageUrl(String.format("%s/%s.%s", qrCodeUri, CURR_TIME_IN_MILLIS, "png"));
        appointment.setQrCodeImagePath(generateQRCodeImagePath(CURR_TIME_IN_MILLIS));
        appointment.setUniqueToken(TokenManager.generateUniqToken());
        appointment.setAppointmentTimeStamp(Long.valueOf(CURR_TIME_IN_MILLIS));
        appointment.setDoctorName(String.format("%s %s", doctor.get().getFirstName(), doctor.get().getLastName()));
        appointment.setClinic(doctor.get().getClinic());

        log.debug(appointment);
        Appointment savedAppointment = appointmentRepository.insert(appointment);

        log.debug(String.format("Saved Appointment: %s",  savedAppointment.getAppointmentNumber()));

        generateQRCodeImage(appointment);
        return Optional.of(savedAppointment);
    }

    public Optional<List<EventModelOutput>> createOnlineAppointment(OnlineAppointment onlineAppointment)
    {
        Event event = new CreateGoogleEvent<Event>(onlineAppointment, timeSlotService, userService, calendarService).execute();
        OnlineAppointment createdAppointment = new SaveOnlineAppointment<OnlineAppointment>(onlineAppointment, onlineAppointmentRepository).execute();

        // async call for sending emails
        CompletableFuture.supplyAsync(()->new SendEmailToParticipants<Boolean>(onlineAppointment, timeSlotService, userService).execute());


        return Optional.of(CalendarUtil.convertListOfEventsToEventModelOutput(
                Arrays.asList(new Event[]{event}), userService)
        );
    }

    @Override
    public Optional<Appointment> fetchAppointment(String id) {
        Optional<Appointment> appointment = appointmentRepository.findById(id);
        appointment.ifPresent(a->{
            userService.getUserById(a.getDoctorId())
                    .ifPresent(user -> {
                        a.setClinic(user.getClinic());
                    });
        });
        return appointment;
    }

    @Override
    public List<Appointment> fetchAppointments() {
        return appointmentRepository.findAllByOrderByAppointmentTimeStamp();
    }

    @Override
    public List<Appointment> fetchAppointmentByDoctorId(String doctorId) {
        return appointmentRepository.findByDoctorId(doctorId);
    }

    @Override
    public void deleteAppointment(String id) {
        Optional<Appointment> appointmentToDelete = appointmentRepository.findById(id);

        appointmentToDelete.ifPresent(appointment ->
                OnDeleteAction.newInstance(appointment).execute());

        appointmentRepository.deleteById(id);
        log.info(String.format("Appointment with Id[%s] deleted successfully!", id));
    }

    /**
     * Deletes all the appointments from the database by giving call to appointment repository
     */
    @Override
    public void deleteAllAppointments() {
        List<Appointment> appointmentsToDelete = appointmentRepository.findAll();
        appointmentsToDelete.forEach(a->
                OnDeleteAction.newInstance(a).execute());
        appointmentRepository.deleteAll();
        log.info("All the appointments have been deleted successfully!");
    }

    /**
     * Fetches all the online appointments entries
     *
     * @return
     */
    @Override
    public List<OnlineAppointment.Expand> fetchOnlineAppointments() {
        List<OnlineAppointment> onlineAppointments = onlineAppointmentRepository.findAll();
        List<OnlineAppointment.Expand> onlineAppointmentsExpanded = new ArrayList<>();
        onlineAppointments.forEach(onlineAppointment -> {
            Optional<User> patient = userService.getUserById(onlineAppointment.getPatientId());
            Optional<User> doctor = userService.getUserById(onlineAppointment.getSelectedDoctorId());
            Optional<TimeSlot> timeSlot = timeSlotService.getTimeSlot(onlineAppointment.getSelectedTimeSlotId());
            String date = onlineAppointment.getSelectedDate();
            onlineAppointmentsExpanded.add(onlineAppointment.new Expand()
                    .patient(patient.get())
                    .doctor(doctor.get())
                    .timeSlot(timeSlot.get())
                    .date(date)
            );
        });
        return onlineAppointmentsExpanded;
    }

    /**
     * Fetches all appointments by user id
     *
     * @param userId
     * @return
     */
    @Override
    public List<OnlineAppointment> fetchOnlineAppointmentsByUserId(String userId) {
        return onlineAppointmentRepository.findByPatientId(userId);
    }

    /**
     * fetches appointment status for the provided appointment number
     *
     * @param appointmentNumber
     * @return
     */
    @Override
    public Optional<Appointment> getAppointmentStatus(int appointmentNumber) {
        Optional<Appointment> appointment = appointmentRepository.findByAppointmentNumber(appointmentNumber);
        return appointment;
    }

    /**
     * Fetches appointment by appointment number
     *
     * @param appointmentNumber
     * @return
     */
    @Override
    public Optional<Appointment> fetchAppointmentByAppointmentNumber(int appointmentNumber) {
        return appointmentRepository.findByAppointmentNumber(appointmentNumber);
    }

    @Override
    public List<OnlineAppointment> fetchOnlineAppointmentsByDoctorId(String id) {
        return onlineAppointmentRepository.findBySelectedDoctorId(id);
    }

    @Override
    public Map<String, List<DisplayOnlineAppointment>> displayOnlineAppointments(User user) {
        Map<String,  List<OnlineAppointment>> onlineAppointments = fetchOnlineAppointmentsByDoctorId(user.getId())
                .stream().collect(
                Collectors.groupingBy(OnlineAppointment::getSelectedDate, HashMap::new, Collectors.toCollection(ArrayList::new))
        );
        Map<String, List<DisplayOnlineAppointment>> displayOnlineAppointmentsMap = new HashMap<>();
        onlineAppointments.forEach((date, onlineAppointmentList) -> {
            List<DisplayOnlineAppointment> displayOnlineAppointmentList = new LinkedList<>();
            onlineAppointmentList.forEach(onlineAppointment -> {
                DisplayOnlineAppointment displayOnlineAppointment = new DisplayOnlineAppointment();
                displayOnlineAppointment.setPatientName(
                        userService.getUserById(onlineAppointment.getPatientId())
                            .map(patient->{
                                return patient.getFirstName() + " " + patient.getLastName();
                            }).orElse("Patient name not found!")
                    );
                displayOnlineAppointment.setDoctorName(userService.getUserById(user.getId())
                        .map(doc->{
                            return doc.getFirstName() + " " + doc.getLastName();
                        }).orElse("Patient name not found!"));
                displayOnlineAppointment.setAppointmentDate(onlineAppointment.getSelectedDate());
                displayOnlineAppointment.setAppointmentTime(
                        timeSlotService.getTimeSlot(onlineAppointment.getSelectedTimeSlotId()).get().getStartTime());

                displayOnlineAppointmentList.add(displayOnlineAppointment);
            });

            displayOnlineAppointmentsMap.put(date, displayOnlineAppointmentList);

        });
        return displayOnlineAppointmentsMap;
    }

    private void generateQRCodeImage(Appointment appointment) {
        try {
            long appointmentNumber = appointment.getAppointmentNumber();
            boolean isQRCodeGenerated = QRCodeUtil.generateQRCode(
                    new QRCodeWriter(),
                    new QRCodeUtil.QRCodeInputBuilder()
                            .textToEncode("{\"appointmentId\":"+(appointmentNumber)+"}")
                            .height(600)
                            .width(600)
                            .fileFormat("png")
                            .saveToFilePath(true)
                            .filePath(appointment.getQrCodeImagePath())
                            .build()
            );
            if (isQRCodeGenerated) {
                log.info(String.format("QRCode with file path [%s] has been generated!", appointment.getQrCodeImagePath()));
            }
        } catch (QRCodeException e) {
            e.printStackTrace();
        }
    }

    private String generateQRCodeImagePath(String uniqFileName) {
        final String FILE_EXT = "png";

        if (StringUtils.isEmpty(qrCodeFolderPath))
            qrCodeFolderPath = String.format("%/src/main/resources/static/application/images/qr",System.getProperty("user.dir"));

        File file = new File(qrCodeFolderPath);
        if (!  file.exists())  {
            file.mkdirs();
        }
        return String.format("%s/%s.%s",  file.getAbsolutePath(), uniqFileName, FILE_EXT);
    }
}
