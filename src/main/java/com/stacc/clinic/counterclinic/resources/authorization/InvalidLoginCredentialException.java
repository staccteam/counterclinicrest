package com.stacc.clinic.counterclinic.resources.authorization;

public class InvalidLoginCredentialException extends RuntimeException {
    /**
     * Constructs a new runtime exception with {@code null} as its
     * detail message.  The cause is not initialized, and may subsequently be
     * initialized by a call to {@link #initCause}.
     */
    InvalidLoginCredentialException() {
        super();
    }

    InvalidLoginCredentialException(String msg) {
        super(msg);
    }
}
