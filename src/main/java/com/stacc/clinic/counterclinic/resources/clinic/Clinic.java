package com.stacc.clinic.counterclinic.resources.clinic;

import com.mongodb.lang.Nullable;
import com.stacc.clinic.counterclinic.resources.user.User;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;

@Data
@Document(collection = "clinics")
public class Clinic {
    @Id
    private String id;
    @NotNull
    private String clinicNumber;

    public Clinic() {}

    public Clinic(@NotNull String clinicNumber) {
        this.clinicNumber = clinicNumber;
    }

    public Clinic(String id, String clinicNumber) {
        this.id = id;
        this.clinicNumber = clinicNumber;
    }

    public static Clinic newInstance() {
        return new Clinic();
    }

    public static Clinic newInstance(String id, String clinicNumber) {
        return new Clinic(id, clinicNumber);
    }

    public static Clinic newInstance(String clinicNumber) {
        return new Clinic(clinicNumber);
    }
}
