package com.stacc.clinic.counterclinic.resources.clinic;

import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Log4j
@Service
public class ClinicServiceImpl implements ClinicService {
    private ClinicRepository clinicRepository;

    public ClinicServiceImpl(ClinicRepository clinicRepository) {
        this.clinicRepository = clinicRepository;
    }

    public List<Clinic> fetchAllClinics() {
        log.debug("Fetching list of all clinics");
        return clinicRepository.findAll();
    }

    @Override
    public Clinic createClinic(Clinic clinic) {
        return clinicRepository.insert(clinic);
    }

    @Override
    public Clinic updateClinic(Clinic clinic) {
        return clinicRepository.save(clinic);
    }

    /**
     * Find clinic by clinic id and returns optional
     *
     * @param clinicId
     * @return
     */
    @Override
    public Optional<Clinic> fetchClinicById(String clinicId) {
        return clinicRepository.findById(clinicId);
    }

    @Override
    public void deleteClinic(String clinicId) {
        clinicRepository.deleteById(clinicId);
    }
}
