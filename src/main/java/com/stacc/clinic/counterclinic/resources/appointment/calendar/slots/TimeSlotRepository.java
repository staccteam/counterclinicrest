package com.stacc.clinic.counterclinic.resources.appointment.calendar.slots;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TimeSlotRepository extends MongoRepository<TimeSlot, String> {

    @Override
    List<TimeSlot> findAll();

    /**
     * Inserts the given entity. Assumes the instance to be new to be able to apply insertion optimizations. Use the
     * returned instance for further operations as the save operation might have changed the entity instance completely.
     * Prefer using {@link #save(Object)} instead to avoid the usage of store-specific API.
     *
     * @param entity must not be {@literal null}.
     * @return the saved entity
     * @since 1.7
     */
    @Override
    TimeSlot insert(TimeSlot entity);

    /**
     * Saves a given entity. Use the returned instance for further operations as the save operation might have changed the
     * entity instance completely.
     *
     * @param entity must not be {@literal null}.
     * @return the saved entity will never be {@literal null}.
     */
    @Override
    TimeSlot save(TimeSlot entity);

    /**
     * Retrieves an entity by its id.
     *
     * @param slotId must not be {@literal null}.
     * @return the entity with the given id or {@literal Optional#empty()} if none found
     * @throws IllegalArgumentException if {@code id} is {@literal null}.
     */
    @Override
    Optional<TimeSlot> findById(String slotId);

    /**
     * Retrieves all the timeslot for a given type
     * @param whatFor
     * @return
     */
    List<TimeSlot> findByWhatFor(String whatFor);

    /**
     * Returns whether an entity with the given id exists.
     *
     * @param slotId must not be {@literal null}.
     * @return {@literal true} if an entity with the given id exists, {@literal false} otherwise.
     * @throws IllegalArgumentException if {@code id} is {@literal null}.
     */
    @Override
    boolean existsById(String slotId);

    /**
     * Deletes the entity with the given id.
     *
     * @param slotId must not be {@literal null}.
     * @throws IllegalArgumentException in case the given {@code id} is {@literal null}
     */
    @Override
    void deleteById(String slotId);

    /**
     * Deletes all entities managed by the repository.
     */
    @Override
    void deleteAll();

    void deleteByWhatFor(String whatFor);
}
