package com.stacc.clinic.counterclinic.resources.appointment.queue;

import com.stacc.clinic.counterclinic.resources.appointment.Appointment;
import com.stacc.clinic.counterclinic.resources.appointment.AppointmentService;
import com.stacc.clinic.counterclinic.resources.user.User;
import com.stacc.clinic.counterclinic.resources.user.UserService;
import com.stacc.clinic.counterclinic.utils.SiteConfig;
import com.stacc.clinic.counterclinic.utils.TokenManager;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Log4j
@RestController
@RequestMapping("/api/appointmentQueue")
public class AppointmentQueueController {

    private UserService userService;
    private AppointmentQueueService appointmentQueueService;
    private AppointmentService appointmentService;

    @Autowired
    public AppointmentQueueController(UserService userService,  AppointmentQueueService appointmentQueueService, AppointmentService appointmentService) {
        this.userService = userService;
        this.appointmentQueueService  = appointmentQueueService;
        this.appointmentService = appointmentService;
    }

    @GetMapping("/reset-avg-time")
    public ResponseEntity resetAvgTimeInAppointmentQueue(
            @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken
    )
    {
        log.debug("Doctor Dashboard Doctor Taking Break");
        Optional<User> user = provideUserFromToken(authToken);

        if (!user.isPresent())
        {
            log.debug("Auth Token is empty.\nAuth Token:" + authToken);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Un-authorized");
        }

        Optional<AppointmentQueue> appointmentQueue = appointmentQueueService.addAvgTime(user.get(), 0L);

        return ResponseEntity.ok("SUCCESS");
    }

    @GetMapping("/status/{appointmentNumber}")
    public ResponseEntity getAppointmentStatusByAppointmentNumber(
            @PathVariable("appointmentNumber") int appointmentNumber)
    {
        Optional<Appointment> appointment = appointmentService.fetchAppointmentByAppointmentNumber(appointmentNumber);
        if (!appointment.isPresent() || StringUtils.isEmpty(appointment.get().getAppointmentQueueId()))
        {
            return ResponseEntity.status(org.apache.http.HttpStatus.SC_NOT_FOUND)
                    .body("No appointment found in the database for given appointment number. (" + appointmentNumber + ")");
        }
        Optional<AppointmentQueue> appointmentQueue = appointmentQueueService.getAppointmentQueueById(appointment.get().getAppointmentQueueId());
        return ResponseEntity.ok(appointmentQueue.get());
    }

    @GetMapping("/user")
    public ResponseEntity getAppointmentStatusByAppointmentQueueId(
            @RequestParam(SiteConfig.SITE_AUTH_COOKIE) String authToken
    )
    {
        Optional<User> doctor = provideUserFromToken(authToken);
        Optional<AppointmentQueue> appointmentQueue = appointmentQueueService.findByDoctorId(doctor.get().getId());
        return ResponseEntity.ok(appointmentQueue);
    }

    @GetMapping("/getDoctorStatus/{doctorId}")
    public ResponseEntity getDoctorStatus(
//            @CookieValue(SiteConfig.SITE_AUTH_COOKIE) String authToken,
            @PathVariable("doctorId") String doctorId
    )
    {
        Optional<AppointmentQueue> appointmentQueue = appointmentQueueService.findByDoctorId(doctorId);
        if (appointmentQueue.isPresent())
            return ResponseEntity.ok(appointmentQueue.get());

        return ResponseEntity.notFound().build();
    }

    protected Optional<User> provideUserFromToken(String jwt)
    {
        if (StringUtils.isEmpty(jwt))
        {
            log.debug("Auth Token is empty.");
            return Optional.empty();
        }
        return userService.getUserByUsername(
                String.valueOf(
                        TokenManager.validateAccessToken(jwt)
                                .getBody().get("username")));
    }



}
