package com.stacc.clinic.counterclinic.resources.appointment.actions;

import com.stacc.clinic.counterclinic.resources.appointment.queue.AppointmentQueue;
import com.stacc.clinic.counterclinic.resources.appointment.queue.AppointmentQueueRepository;
import com.stacc.clinic.counterclinic.resources.appointment.queue.DoctorStatus;
import com.stacc.clinic.counterclinic.resources.user.User;

import java.util.List;
import java.util.Optional;

public class OnBreakAction implements AppointmentAction<AppointmentQueue> {

    private AppointmentQueue appointmentQueue;
    private Long duration;

    public OnBreakAction(AppointmentQueue appointmentQueue, Long duration) {
        this.appointmentQueue = appointmentQueue;
        this.duration = duration;
    }

    @Override
    public AppointmentQueue execute() {
        appointmentQueue.setDoctorStatus(DoctorStatus.ON_BREAK);
        appointmentQueue.setExtendedTime(duration);
        return appointmentQueue;
    }
}
