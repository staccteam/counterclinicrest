package com.stacc.clinic.counterclinic.resources.appointment;

import com.stacc.clinic.counterclinic.resources.appointment.calendar.EventModelOutput;
import com.stacc.clinic.counterclinic.resources.user.User;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface AppointmentService {

    public Optional<List<EventModelOutput>> createOnlineAppointment(OnlineAppointment onlineAppointment);

    /**
     * Perform all the necessary actions before creating new appointment
     * And calls appointment repository to create new appointment into the
     * database.
     *
     * @param appointment
     * @return Returns the Newly created appointment with its id.
     */
    Optional<Appointment> createNewAppointment(AppointmentForm appointment);

    /**
     * Fetches the appointment by given id by giving call
     * to appointmentRepository
     * @param id
     * @return Appointment object is returned wrapped in Optional.
     */
    Optional<Appointment> fetchAppointment(String id);

    /**
     * Fetches all the available appointments present in the database
     * by giving call to appointment repository
     * @return
     */
    List<Appointment> fetchAppointments();

    /**
     * Fetches all the appointments for the given doctor Id
     * by calling appointment repository
     * @param doctorId
     * @return List of appointments is returned. If no appointment found, returns empty list.
     */
    List<Appointment> fetchAppointmentByDoctorId(String doctorId);

    /**
     * Perform all the necessary actions before deleting the appointment
     * by giving call to appointment repository
     * @param id
     */
    void deleteAppointment(String id);

    /**
     * Deletes all the appointments from the database by giving call to appointment repository
     */
    void deleteAllAppointments();

    /**
     * Fetches all the online appointments entries
     * @return
     */
    List<OnlineAppointment.Expand> fetchOnlineAppointments();

    /**
     * Fetches all appointments by user id
     * @param userId
     * @return
     */
    List<OnlineAppointment> fetchOnlineAppointmentsByUserId(String userId);

    /**
     * fetches appointment status for the provided appointment number
     * @param appointmentNumber
     * @return
     */
    Optional<Appointment> getAppointmentStatus(int appointmentNumber);

    /**
     * Fetches appointment by appointment number
     * @param appointmentNumber
     * @return
     */
    Optional<Appointment> fetchAppointmentByAppointmentNumber(int appointmentNumber);

    List<OnlineAppointment> fetchOnlineAppointmentsByDoctorId(String id);

    Map<String, List<DisplayOnlineAppointment>> displayOnlineAppointments(User user);
}
