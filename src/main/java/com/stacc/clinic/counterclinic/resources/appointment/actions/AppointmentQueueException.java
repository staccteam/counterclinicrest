package com.stacc.clinic.counterclinic.resources.appointment.actions;

import javax.validation.constraints.NotNull;

public class AppointmentQueueException extends RuntimeException {
    public AppointmentQueueException(@NotNull String msg) {
        super(msg);
    }
}
