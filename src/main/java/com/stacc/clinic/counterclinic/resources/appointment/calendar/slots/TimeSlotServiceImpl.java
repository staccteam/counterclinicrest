package com.stacc.clinic.counterclinic.resources.appointment.calendar.slots;

import com.stacc.clinic.counterclinic.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Optional;

@Service
public class TimeSlotServiceImpl implements TimeSlotService {

    private TimeSlotRepository timeSlotRepository;

    @Autowired
    public TimeSlotServiceImpl(TimeSlotRepository timeSlotRepository) {
        this.timeSlotRepository = timeSlotRepository;
    }

    public void removeAllTimeSlots()
    {
        this.timeSlotRepository.deleteAll();
    }

    public void removeByType(String type)
    {
        this.timeSlotRepository.deleteByWhatFor(type);
    }

    @Override
    public List<TimeSlot> refreshTimeSlots(SlotTemplate slotTemplate) {
        removeByType(slotTemplate.getWhatFor());
        List<TimeSlot> timeSlots = DateUtil.getTimeSlots(slotTemplate.getStartTime(),
                slotTemplate.getEndTime(), slotTemplate.getSlotDuration());
        timeSlots.forEach(timeSlot -> {
            timeSlot.setWhatFor(slotTemplate.getWhatFor());
            this.timeSlotRepository.save(timeSlot);
        });
        return timeSlots;
    }

    @Override
    public List<TimeSlot> getAllSlots() {
        return this.timeSlotRepository.findAll();
    }

    /**
     * Fetch timeslot by respective id
     *
     * @param timeSlotId
     * @return
     */
    @Override
    public Optional<TimeSlot> getTimeSlot(String timeSlotId) {
        return this.timeSlotRepository.findById(timeSlotId);
    }
}
