package com.stacc.clinic.counterclinic.resources.user;

public enum UserRoles {
    DOCTOR,
    RECEPTION,
    ADMIN,
    PATIENT,
    SUPER_ADMIN;
}
