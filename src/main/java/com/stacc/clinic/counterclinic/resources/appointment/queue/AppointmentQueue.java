package com.stacc.clinic.counterclinic.resources.appointment.queue;

import com.stacc.clinic.counterclinic.resources.user.User;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

@Data
@Document(collection = "appointment_queues")
public class AppointmentQueue {
    @Id
    private String id;
//    @Indexed(unique = true)
    private String doctorId;
    private Integer queueSize;
    private Integer currQueueCount;
    private Long extendedTime;
    private Long avgCheckupTime;
    private Long patientInTimeStamp;
    private Long totalCheckupTime;
    private AverageTimeCalculationLogic averageTimeCalculationLogic;
    private DoctorStatus doctorStatus;

    public static AppointmentQueue initForDoctor(String doctorId) {
        return newInstance()
                .doctorId(doctorId)
                .doctorStatus(DoctorStatus.INIT)
                .queueSize(0)
                .currQueueCount(0)
                .extendedTime(0L)
                .avgCheckupTime(Duration.ofMinutes(15).toMillis())
                .patientInTimeStamp(System.currentTimeMillis())
                .totalCheckupTime(0L)
                .avgTimeCalculationLogin(AverageTimeCalculationLogic.AUTOMATIC);
    }

    private static Long minutesToSeconds(int minutes) {
        return minutes*60L;
    }

    public static AppointmentQueue newInstance() {
        return new AppointmentQueue();
    }

    public AppointmentQueue doctorId(String doctorId) {
        this.doctorId = doctorId;
        return this;
    }

    public AppointmentQueue queueSize(Integer queueSize) {
        this.queueSize = queueSize;
        return this;
    }

    public AppointmentQueue currQueueCount(Integer currQueueCount) {
        this.currQueueCount = currQueueCount;
        return this;
    }

    public AppointmentQueue extendedTime(Long extendedTime) {
        this.extendedTime = extendedTime;
        return this;
    }

    public AppointmentQueue avgCheckupTime(Long avgCheckupTime) {
        this.avgCheckupTime = avgCheckupTime;
        return this;
    }

    public AppointmentQueue doctorStatus(DoctorStatus doctorStatus) {
        this.doctorStatus = doctorStatus;
        return this;
    }

    public AppointmentQueue patientInTimeStamp(Long timestamp)
    {
        this.patientInTimeStamp = timestamp;
        return this;
    }

    public AppointmentQueue totalCheckupTime(Long timestamp)
    {
        this.totalCheckupTime = timestamp;
        return this;
    }

    public AppointmentQueue avgTimeCalculationLogin(AverageTimeCalculationLogic averageTimeCalculationLogic)
    {
        this.averageTimeCalculationLogic = averageTimeCalculationLogic;
        return this;
    }

    public Optional<User> findDoctorWithId(List<User> doctors, String id) {
        return doctors.stream()
                .filter(doc-> doc.getId().equals(id))
                .findFirst();
    }

}
