package com.stacc.clinic.counterclinic.resources.appointment;

import com.stacc.clinic.counterclinic.resources.user.User;
import lombok.Getter;

public class AppointmentCreationFailedException extends RuntimeException {
    @Getter private User user;

    public AppointmentCreationFailedException(String msg) {
        super(msg);
    }

    public AppointmentCreationFailedException(String msg, User user) {
        this.user = user;
    }
}
