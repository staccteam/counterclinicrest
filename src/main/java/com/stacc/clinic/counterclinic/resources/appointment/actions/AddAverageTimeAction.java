package com.stacc.clinic.counterclinic.resources.appointment.actions;

import com.stacc.clinic.counterclinic.resources.appointment.queue.AppointmentQueue;
import com.stacc.clinic.counterclinic.resources.appointment.queue.AverageTimeCalculationLogic;

import java.time.Duration;

public class AddAverageTimeAction implements AppointmentAction<AppointmentQueue> {

    private AppointmentQueue appointmentQueue;
    private Long avgCheckupTimeInMinutes;

    public AddAverageTimeAction(AppointmentQueue appointmentQueue, Long avgCheckupTimeInMinutes) {
        this.appointmentQueue = appointmentQueue;
        this.avgCheckupTimeInMinutes = avgCheckupTimeInMinutes;
    }


    @Override
    public AppointmentQueue execute() {
        if (avgCheckupTimeInMinutes > 0)
            appointmentQueue.setAverageTimeCalculationLogic(AverageTimeCalculationLogic.MANUAL);
        else
            appointmentQueue.setAverageTimeCalculationLogic(AverageTimeCalculationLogic.AUTOMATIC);
        appointmentQueue.setAvgCheckupTime(Duration.ofMinutes(avgCheckupTimeInMinutes).toMillis());
        return appointmentQueue;
    }
}
