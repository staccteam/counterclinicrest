package com.stacc.clinic.counterclinic.resources.appointment.calendar.slots;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "time_slots")
public class TimeSlot {
    private String id;
    private String startTime;
    private String endTime;
    private String whatFor;

    public TimeSlot() {
    }

    public TimeSlot(String startTime, String endTime, String whatFor) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.whatFor = whatFor;
    }

    public TimeSlot(String startTime, String endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }
}
