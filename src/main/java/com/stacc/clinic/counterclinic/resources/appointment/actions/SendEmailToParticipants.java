package com.stacc.clinic.counterclinic.resources.appointment.actions;

import com.stacc.clinic.counterclinic.resources.appointment.OnlineAppointment;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.slots.TimeSlot;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.slots.TimeSlotService;
import com.stacc.clinic.counterclinic.resources.user.User;
import com.stacc.clinic.counterclinic.resources.user.UserService;
import com.stacc.clinic.counterclinic.utils.EmailUtil;
import lombok.extern.log4j.Log4j;

import java.util.function.Supplier;

@Log4j
public class SendEmailToParticipants<T> implements AppointmentAction {
    private final OnlineAppointment onlineAppointment;
    private final TimeSlotService timeSlotService;
    private final UserService userService;

    public SendEmailToParticipants(OnlineAppointment onlineAppointment, TimeSlotService timeSlotService, UserService userService) {
        this.onlineAppointment = onlineAppointment;
        this.timeSlotService = timeSlotService;
        this.userService = userService;
    }

    @Override
    public Boolean execute() {
        User doctor = userService.getUserById(onlineAppointment.getSelectedDoctorId()).get();
        User patient = userService.getUserById(onlineAppointment.getPatientId()).get();
        TimeSlot timeSlot = timeSlotService.getTimeSlot(onlineAppointment.getSelectedTimeSlotId()).get();

        OnlineAppointment.Expand onlineAppointmentDetails = new OnlineAppointment().new Expand();
        onlineAppointmentDetails.setDoctor(doctor);
        onlineAppointmentDetails.setPatient(patient);
        onlineAppointmentDetails.setDate(onlineAppointment.getSelectedDate());
        onlineAppointmentDetails.setSlot(timeSlot);

        try
        {
            String htmlBody = new EmailHtmlBuilder()
                    .salutation(String.format("Hello %s %s,", doctor.getFirstName(), doctor.getLastName()))
                    .newPara("You have a new appointment. Below are the details:")
                    .newPara(String.format(
                            "<table style=\"border: 1px solid;\">" +
                                    "<thead>" +
                                        "<th>" +
                                        "Appointment Details" +
                                        "</th>" +
                                    "</thead>" +
                                    "<tr>" +
                                        "<td>Patient Name</td>" +
                                        "<td>%s %s</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                        "<td>Appointment Date</td>" +
                                        "<td>%s</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                        "<td>Appointment Time</td>" +
                                        "<td>%s</td>" +
                                    "</tr>" +
                                    "</table>",
                            patient.getFirstName(), patient.getLastName(),
                            onlineAppointment.getSelectedDate(),
                            timeSlot.getStartTime()
                    ))
                    .newPara("Thanks & Regards,")
                    .newPara("Counter Clinic")
                    .build();
            EmailUtil.sendMail(onlineAppointmentDetails, doctor.getEmail(), htmlBody);

            htmlBody = new EmailHtmlBuilder()
                    .salutation(String.format("Hello %s %s,", patient.getFirstName(), patient.getLastName()))
                    .newPara("Your appointment have been booked successfully. Below are the details:")
                    .newPara(String.format(
                            "<table style=\"border: 1px solid;\">" +
                                    "<thead>" +
                                        "<th>" +
                                            "Appointment Details" +
                                        "</th>" +
                                    "</thead>" +
                                    "<tr>" +
                                    "<td>Appointed Doctor</td>" +
                                    "<td>%s %s</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                    "<td>Appointment Date</td>" +
                                    "<td>%s</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                    "<td>Appointment Time</td>" +
                                    "<td>%s</td>" +
                                    "</tr>" +
                                    "</table>",
                            doctor.getFirstName(), doctor.getLastName(),
                            onlineAppointment.getSelectedDate(),
                            timeSlot.getStartTime()
                    ))
                    .newPara("Thanks & Regards,")
                    .newPara("Counter Clinic")
                    .build();
            EmailUtil.sendMail(onlineAppointmentDetails, patient.getEmail(),htmlBody);
        } catch (Exception e)
        {
            log.error("Cannot send email. Error: " + e.getMessage(), e);
            return false;
        }


        return true;
    }

    private static class EmailHtmlBuilder
    {
        StringBuilder sb = new StringBuilder("");

        public EmailHtmlBuilder salutation(String salutation)
        {
            sb.append("<p><strong>")
                    .append(salutation)
            .append("</strong></p>");
            return this;
        }

        public EmailHtmlBuilder end(String end)
        {
            sb.append(end);
            return this;
        }

        public EmailHtmlBuilder newLine()
        {
            sb.append("<br/>");
            return  this;
        }

        public EmailHtmlBuilder newPara(String text)
        {
            sb.append("<p>").append(text).append("</p>");
            return  this;
        }

        public String build()
        {
            return sb.toString();
        }


    }
}
