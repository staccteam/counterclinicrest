package com.stacc.clinic.counterclinic.resources.appointment.calendar;

import com.stacc.clinic.counterclinic.resources.user.User;
import lombok.Data;

import java.util.Date;

@Data
public class EventModel {
    private String calendarId;
    private String eventId;
    private String summary;
    private String description;
    private String patientEmail;
    private String doctorEmail;
    private String startDateTime;
    private Long startTime;
    private Long endTime;
    private String timezone;

    public EventModel calendarId(String calendarId)
    {
        this.calendarId = calendarId;
        return this;
    }

    public EventModel eventId(String eventId)
    {
        this.eventId = eventId;
        return this;
    }

    public EventModel summary(String summary)
    {
        this.summary = summary;
        return this;
    }

    public EventModel description(String description)
    {
        this.description = description;
        return this;
    }

    public EventModel patientEmail(String email)
    {
        this.patientEmail = email;
        return this;
    }

    public EventModel doctorEmail(String email)
    {
        this.doctorEmail = email;
        return this;
    }

    public EventModel startTime(Long startTime)
    {
        this.startTime = startTime;
        return this;
    }

    public EventModel endTime(Long endTime)
    {
        this.endTime = endTime;
        return this;
    }

    public EventModel timezone(String timezone)
    {
        this.timezone = timezone;
        return  this;
    }

}
