package com.stacc.clinic.counterclinic.resources.appointment;

public enum AppointmentType {
    WALK_IN,
    ONLINE
}
