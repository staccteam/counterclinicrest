package com.stacc.clinic.counterclinic.resources.appointment.calendar;

import com.stacc.clinic.counterclinic.resources.user.User;
import lombok.Data;
import lombok.extern.java.Log;
import lombok.extern.log4j.Log4j;

@Log4j
@Data
public class CalendarModel {

    private String calendarId;
    private String summary;
    private String kind;
    private String timezone;

}
