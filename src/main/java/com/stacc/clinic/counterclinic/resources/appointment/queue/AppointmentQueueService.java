package com.stacc.clinic.counterclinic.resources.appointment.queue;

import com.stacc.clinic.counterclinic.resources.appointment.Appointment;
import com.stacc.clinic.counterclinic.resources.appointment.AppointmentForm;
import com.stacc.clinic.counterclinic.resources.user.User;

import java.util.List;
import java.util.Optional;

public interface AppointmentQueueService {
    Appointment onCreateAppointment(AppointmentForm latestAppointmentRequest);

    /**
     * When doctor calls next patient in
     * updates the doctor queue accordingly
     * @param doctorId
     * @return
     */
    AppointmentQueue onNextPatientIn(User doctorId);

    List<AppointmentQueue> fetchAllQueues();

    Optional<AppointmentQueue> findByDoctorId(String doctorId);

    Optional<AppointmentQueue> updateDoctorStatus(User user, DoctorStatus doctorStatus);


    Optional<AppointmentQueue> takeBreak(User user, Long breakDurationInMillis);

    /**
     * Doctor comes back after the break
     * Sets extended time to 0
     * @param user
     * @return
     */
    Optional<AppointmentQueue> resumeAfterBreak(User user);

    /**
     * Doctor Clicks on the logout button/end-of-day button
     * change doctor status to LOGGED_OUT
     * @param user
     * @return
     */
    Optional<AppointmentQueue> endOfDay(User user);

    /**
     * Adds average time  to for the doctors queue.
     * @param user
     * @return
     */
    Optional<AppointmentQueue> addAvgTime(User user, Long avgTime);


    /**
     * Fetches appointment queue for the given id
     * @param appointmentQueueId
     * @return
     */
    Optional<AppointmentQueue> getAppointmentQueueById(String appointmentQueueId);

    void resetAllQueues();

    void resetQueue(String appointmentQueueId, String doctorId);
}
