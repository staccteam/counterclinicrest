package com.stacc.clinic.counterclinic.resources.clinic;

import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public interface ClinicService {

    public List<Clinic> fetchAllClinics();

    public Clinic createClinic(Clinic clinic);

    public Clinic updateClinic(Clinic clinic);

    /**
     * Find clinic by clinic id and returns optional
     * @param clinicId
     * @return
     */
    public Optional<Clinic> fetchClinicById(String clinicId);

    public void deleteClinic(String clinicId);
}
