package com.stacc.clinic.counterclinic.resources.sitemeta;

import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface SiteMetaService {

    List<SiteOption> fetchAllSiteOptions();

    /**
     * Find Site Option by the provide key
     */
    Optional<SiteOption> fetchSiteOptionByKey(String key);

    SiteOption createNewSiteMetaOption(SiteOption option);

    void deleteSiteMetaOption(String optionId);
}
