package com.stacc.clinic.counterclinic.resources.appointment.calendar.slots;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface SlotTemplateService {

    /**
     * Creates Slot Template if not exist
     * otherwise refreshed it with the new values
     * @param slotTemplate
     * @return
     */
    List<TimeSlot> refreshSlotTemplate(SlotTemplate slotTemplate);

    /**
     * Fetchesall the slot templates from the backend
     * @return
     */
    public List<SlotTemplate> getAllSlotTemplates();

    void deleteTemplateById(String slotId);
}
