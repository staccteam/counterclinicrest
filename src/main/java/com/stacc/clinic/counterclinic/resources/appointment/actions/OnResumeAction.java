package com.stacc.clinic.counterclinic.resources.appointment.actions;

import com.stacc.clinic.counterclinic.resources.appointment.queue.AppointmentQueue;
import com.stacc.clinic.counterclinic.resources.appointment.queue.DoctorStatus;

public class OnResumeAction implements AppointmentAction<AppointmentQueue> {

    private AppointmentQueue aq;

    public OnResumeAction(AppointmentQueue aq) {
        this.aq = aq;
    }

    @Override
    public AppointmentQueue execute() {
         aq.setDoctorStatus(DoctorStatus.IN_PROGRESS);
         aq.setAvgCheckupTime(aq.getAvgCheckupTime() - aq.getExtendedTime());
         aq.setExtendedTime(0L);
         return aq;
    }
}
