package com.stacc.clinic.counterclinic.resources.authorization;

import com.stacc.clinic.counterclinic.resources.user.User;

import java.util.Optional;


public interface AuthorizationService {
    Optional<User> loginUser(LoginUserInput user);

    Optional<User> authorizeUser(String accessToken);
}
