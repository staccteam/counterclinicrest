package com.stacc.clinic.counterclinic.resources.appointment.actions;

import com.stacc.clinic.counterclinic.resources.appointment.queue.AppointmentQueue;
import com.stacc.clinic.counterclinic.resources.appointment.queue.AverageTimeCalculationLogic;


public class CalculateAverageCheckupTimeAction implements AppointmentAction<AppointmentQueue> {
    private AppointmentQueue appointmentQueue;

    public CalculateAverageCheckupTimeAction(AppointmentQueue appointmentQueue) {
        this.appointmentQueue = appointmentQueue;
    }

    @Override
    public AppointmentQueue execute() {
        if (appointmentQueue.getAverageTimeCalculationLogic().equals(AverageTimeCalculationLogic.MANUAL))
            return appointmentQueue;
        Long totalCheckupTime = appointmentQueue.getTotalCheckupTime();
        Long extendedTime = appointmentQueue.getExtendedTime();
        Integer currQueueCount  = appointmentQueue.getCurrQueueCount();
        Long averageCheckupTime = 0L;
        if (currQueueCount != 0)
            averageCheckupTime = (totalCheckupTime + extendedTime)/currQueueCount;
        appointmentQueue.setAvgCheckupTime(averageCheckupTime);
        return appointmentQueue;
    }
}
