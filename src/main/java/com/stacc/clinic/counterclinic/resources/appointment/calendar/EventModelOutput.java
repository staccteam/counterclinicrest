package com.stacc.clinic.counterclinic.resources.appointment.calendar;

import com.stacc.clinic.counterclinic.resources.user.User;
import lombok.Data;

@Data
public class EventModelOutput {
    private String eventId;
    private String summary;
    private Long startTime;
    private Long endTime;
    private User patient;
    private User doctor;
    private String eventUrl;
    private String timezone;
}
