package com.stacc.clinic.counterclinic.resources.appointment.calendar;

import com.google.api.services.calendar.model.Event;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.slots.TimeSlot;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.slots.TimeSlotService;
import com.stacc.clinic.counterclinic.resources.user.User;
import com.stacc.clinic.counterclinic.resources.user.UserService;
import com.stacc.clinic.counterclinic.utils.SiteConfig;
import com.stacc.clinic.counterclinic.utils.TokenManager;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Log4j
@RestController
@RequestMapping("/api/calendar")
public class CalendarController {

    private UserService userService;
    private CalendarService calendarService;
    @Autowired @Lazy
    private TimeSlotService timeSlotService;

    @Autowired @Lazy
    public CalendarController(UserService userService, CalendarService calendarService) {
        this.userService = userService;
        this.calendarService = calendarService;
    }

    @GetMapping("/{calendarId}/events")
    public ResponseEntity getCalendarEvents(
            @RequestParam(SiteConfig.SITE_AUTH_COOKIE) String authToken,
            @PathVariable("calendarId") String calendarId)
    {
        Optional<User> user = TokenManager.provideUserFromToken(authToken, userService);

        if (!user.isPresent())
        {
            log.debug("Auth Token is empty.\nAuth Token:" + authToken);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("UNAUTHORIZED!");
        }

        return ResponseEntity.ok(calendarService.getAllEvents(calendarId));
    }

    @PostMapping("/slots")
    public ResponseEntity createAppointment(
            @RequestParam(SiteConfig.SITE_AUTH_COOKIE) String authToken,
            @RequestParam("slots") EventModel event)
    {
        Optional<User> user = TokenManager.provideUserFromToken(authToken, userService);
        if (! user.isPresent())
        {
            log.debug("Auth Token is empty.\nAuth Token: " + authToken);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("UNAUTHORIZED");
        }

        return ResponseEntity.ok(calendarService.createEvent(event));
    }


    @PatchMapping("/slots")
    public ResponseEntity updateAppointment(
            @RequestParam(SiteConfig.SITE_AUTH_COOKIE) String authToken,
            @RequestParam("slots") EventModel event
    )
    {
        Optional<User> user = TokenManager.provideUserFromToken(authToken, userService);

        if (! user.isPresent())
        {
            log.debug("Auth Token is empty.\nAuth Token: " + authToken);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("UNAUTHORIZED");
        }

        if (StringUtils.isEmpty(event.getEventId())
            || StringUtils.isEmpty(event.getCalendarId()))
        {
            log.debug("Event Id or Calendar Id is null.");
            return ResponseEntity.badRequest().body("Event Id and Calendar Id must be present.");
        }

        return ResponseEntity.ok(calendarService.updateEvent(event));
    }

    @GetMapping("/all-appointments")
    public  ResponseEntity getAllUserAppointments(
            @RequestParam(SiteConfig.SITE_AUTH_COOKIE)  String authToken
    )
    {
        Optional<User> user = TokenManager.provideUserFromToken(authToken, userService);
        if (! user.isPresent())
        {
            log.debug("Auth Token is empty.\nAuth Token: " + authToken);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("UNAUTHORIZED");
        }

        List<Event> events = calendarService.getAllEvents(user.get().getCalendar().getCalendarId());
        List<EventModelOutput> allEvents = CalendarUtil.convertListOfEventsToEventModelOutput(events, userService);
        return ResponseEntity.ok(allEvents);
    }


    @GetMapping("/all-slots")
    public ResponseEntity getAllAppointmentSlots(
            @RequestParam(SiteConfig.SITE_AUTH_COOKIE)  String authToken
    )
    {
        Optional<User> user = TokenManager.provideUserFromToken(authToken, userService);
        if (! user.isPresent())
        {
            log.debug("Auth Token is empty.\nAuth Token: " + authToken);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("UNAUTHORIZED");
        }

        List<TimeSlot> timeSlots = this.timeSlotService.getAllSlots();
        return ResponseEntity.ok(timeSlots);
    }

    @GetMapping("/empty-slots/{doctorId}/{appointmentDate}")
    public ResponseEntity getEmptySlots(
            @PathVariable("doctorId") String doctorId,
            @PathVariable("appointmentDate") String iso8601Date
    )
    {
        Optional<User> doctor = userService.getUserById(doctorId);
        List<TimeSlot> timeSlots = calendarService.fetchEmptySlots(doctor, iso8601Date);
        return ResponseEntity.ok(timeSlots);
    }
}
