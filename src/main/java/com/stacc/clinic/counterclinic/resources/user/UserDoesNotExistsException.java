package com.stacc.clinic.counterclinic.resources.user;

public class UserDoesNotExistsException extends RuntimeException {

    public UserDoesNotExistsException()
    {
        super();
    }

    public UserDoesNotExistsException(String message) {
        super(message);
    }
}
