package com.stacc.clinic.counterclinic.resources.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mongodb.lang.Nullable;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.CalendarModel;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.slots.TimeSlot;
import com.stacc.clinic.counterclinic.resources.appointment.queue.AppointmentQueue;
import com.stacc.clinic.counterclinic.resources.clinic.Clinic;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.util.DigestUtils;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Document(collection = "users")
public class User {

    @Id
    private String id;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    @Indexed(unique = true)
    private String email;

    @NotNull
    private String mobile;

    @Indexed(unique = true)
    private String username;

    private String password;

    @Nullable
    private List<Role> roles;

    @Nullable
    private Clinic clinic;

    @Nullable
    private AppointmentQueue appointmentQueue;

    @Nullable
    private CalendarModel calendar;

    private Long createdTimestamp;

    @Nullable
    private Double fees;

    @Nullable
    private String displayImagePath;
    @Nullable
    private String displayImageName;

    @Nullable
    private List<TimeSlot> bookedSlots;
    @Nullable
    private List<TimeSlot> freeSlots;

    public User(
            String firstName, String lastName,
            String email, String mobile,
            String username, String password)
    {
        this.firstName= firstName;
        this.lastName  = lastName;
        this.email  =  email;
        this.mobile  = mobile;
        this.username  = username;
        setPassword(password);
        this.createdTimestamp = System.currentTimeMillis();
    }

    public User firstName(String firstName)
    {
        this.firstName = firstName;
        return this;
    }

    public User lastName(String lastName)
    {
        this.lastName =  lastName;
        return this;
    }

    public User username(String username)
    {
        this.username = username;
        return this;
    }

    public User password(String password)
    {
        setPassword(password);
        return this;
    }

    public User email(String email)
    {
        this.email = email;
        return this;
    }

    public User mobile(String mobile)
    {
        this.mobile = mobile;
        return this;
    }

    public User roles(List<Role>  roles)
    {
        this.roles = roles;
        return this;
    }

    public User calendar(CalendarModel calendar)
    {
        this.calendar = calendar;
        return this;
    }

    public User clinic(Clinic clinic)
    {
        this.clinic = clinic;
        return this;
    }

    public User fees(Double fees)
    {
        this.fees = fees;
        return this;
    }

    public User() {}

    public static User newInstance() {
        return new User();
    }

    public void setPassword(String password) {
        this.password = DigestUtils.md5DigestAsHex(password.getBytes());
    }

    @JsonIgnore
    public boolean isUsernameEmpty() {
        return null == this.username || this.username.isEmpty();
    }

    @JsonIgnore
    public boolean isEmailEmpty() {
        return null == this.email || this.email.isEmpty();
    }
}
