package com.stacc.clinic.counterclinic.resources.authorization;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.util.DigestUtils;

import javax.validation.constraints.NotNull;

@Data
public class LoginUserInput {
    private String username;
    private String email;
    @NotNull
    private String password;
    private String fcmId;
    private String timestamp;

    @JsonIgnore
    public boolean isUsernameEmpty() {
        return null == this.username || this.username.isEmpty();
    }

    @JsonIgnore
    public boolean isEmailEmpty() {
        return null == this.email || this.email.isEmpty();
    }

    public void setPassword(String password) {
        this.password = DigestUtils.md5DigestAsHex(password.getBytes());
    }
}
