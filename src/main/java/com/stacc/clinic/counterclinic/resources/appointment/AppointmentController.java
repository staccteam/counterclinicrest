package com.stacc.clinic.counterclinic.resources.appointment;

import com.stacc.clinic.counterclinic.resources.appointment.calendar.CalendarService;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.EventModelOutput;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.slots.TimeSlot;
import com.stacc.clinic.counterclinic.resources.user.User;
import com.stacc.clinic.counterclinic.resources.user.UserService;
import com.stacc.clinic.counterclinic.utils.SiteConfig;
import com.stacc.clinic.counterclinic.utils.TokenManager;
import lombok.extern.log4j.Log4j;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Log4j
@RestController
@RequestMapping("/api/appointment")
public class AppointmentController {

    private AppointmentService appointmentService;
    private UserService userService;
    private CalendarService calendarService;

    @Autowired
    public AppointmentController(AppointmentService appointmentService, UserService userService, CalendarService calendarService) {
        this.appointmentService = appointmentService;
        this.userService = userService;
        this.calendarService = calendarService;
    }

    @PostMapping("/save")
    public ResponseEntity createNewOnlineAppointment(
            @RequestHeader(SiteConfig.SITE_AUTH_COOKIE) String authToken,
            @RequestBody @Valid OnlineAppointment appointment, BindingResult binding) {
        Optional<User> user = TokenManager.provideUserFromToken(authToken, userService);
        if (! user.isPresent())
            return ResponseEntity.status(org.springframework.http.HttpStatus.UNAUTHORIZED).body("Unauthorized!");
        if (binding.hasErrors())
            return ResponseEntity.badRequest().body("Invalid appointment data!");

        log.debug("Online Appointment\n" + appointment);

        appointment.setPatientId(user.get().getId());
        Optional<List<EventModelOutput>> newAppointment = appointmentService.createOnlineAppointment(appointment);

        if (! newAppointment.isPresent())
            return ResponseEntity.status(HttpStatus.SC_BAD_REQUEST).body("New Appointment was not created!");

        return ResponseEntity.ok(newAppointment.get());
    }

    @GetMapping
    public ResponseEntity getOnlineAppointments() {
        List<OnlineAppointment.Expand> appointments = appointmentService.fetchOnlineAppointments();
        return ResponseEntity.ok(appointments);
    }

    @GetMapping("/{id}")
    public ResponseEntity getAppointment(@PathVariable("id") String id) {
        Optional<Appointment> appointment = appointmentService.fetchAppointment(id);
        if (!  appointment.isPresent())
            return ResponseEntity.ok(Optional.empty());
        return ResponseEntity.ok(appointment.get());
    }

    @GetMapping("/user/{id}")
    public ResponseEntity getAppointmentsByUser(@PathVariable("id") String userId)
    {
        List<OnlineAppointment> userAppointments  = appointmentService.fetchOnlineAppointmentsByUserId(userId);
        return ResponseEntity.ok(userAppointments);
    }

    @GetMapping("/for/doctor/{doctorId}")
    public ResponseEntity getAppointmentsForDoctor(@PathVariable("doctorId") String doctorId){
        List<Appointment> appointments = appointmentService.fetchAppointmentByDoctorId(doctorId);
        return ResponseEntity.ok(appointments);
    }

    @GetMapping("/number/{appointmentNumber}")
    public ResponseEntity getAppointmentByAppointmentNumber(
            @PathVariable("appointmentNumber") int appointmentNumber)
    {
        Optional<Appointment> appointment = appointmentService.fetchAppointmentByAppointmentNumber(appointmentNumber);
        if (!appointment.isPresent())
            return ResponseEntity.status(HttpStatus.SC_NOT_FOUND)
                    .body("No appointment found in the database for given appointment number. (" + appointmentNumber + ")");
        return ResponseEntity.ok(appointment);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteAppointment(@PathVariable("id") String id) {
        appointmentService.deleteAppointment(id);
        return ResponseEntity.ok(true);
    }

    @DeleteMapping
    public ResponseEntity deleteAllAppointments() {
        appointmentService.deleteAllAppointments();
        return ResponseEntity.ok(true);
    }


}
