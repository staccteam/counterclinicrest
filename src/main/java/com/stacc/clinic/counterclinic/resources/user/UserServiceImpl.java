package com.stacc.clinic.counterclinic.resources.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.services.calendar.model.Calendar;
import com.google.gson.Gson;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.CalendarException;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.CalendarModel;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.CalendarService;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.CalendarUtil;
import com.stacc.clinic.counterclinic.resources.appointment.queue.AppointmentQueueService;
import com.stacc.clinic.counterclinic.utils.FileUploadUtil;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * @author Varun Shrivastava
 * 13th Oct, 2018
 */
@Log4j
@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private AppointmentQueueService appointmentQueueService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, AppointmentQueueService appointmentQueueService) {
        this.userRepository = userRepository;
        this.appointmentQueueService = appointmentQueueService;
    }

    @Autowired @Lazy
    private CalendarService calendarService;

    /**
     * Retrieves all user information from the database
     *
     * @return returns the list of all the users
     */
    @Override
    public List<User> getAllUsers() {
        List<User>  users = userRepository.findAll();

        users.forEach(u->{
            appointmentQueueService.findByDoctorId(u.getId())
                    .ifPresent(queue->{
                        u.setAppointmentQueue(queue);
                    });
        });

        return users;
    }

    /**
     * Fetches the user information based on theid passed
     *
     * @param userId User id of the user
     * @return fetches user by the given id
     */
    @Override
    public Optional<User> getUserById(String userId) {
        return userRepository.findById(userId);
    }

    /**
     * Performs logic to create new user in the database
     *
     * @param user User info to persist into the database
     */
    @Override
    public User createNewUser(User user) {
        // TODO: Un comment below code for production
//        Calendar calendar = calendarService.createCalendar(
//                new Calendar()
//                        .setSummary(user.getEmail())
//                        .setDescription(new Gson().toJson(user))
//        );
        // TODO: Remove this line in production
        CalendarModel userCal = new CalendarModel();
        userCal.setCalendarId("rd2j42vm121navh4ln6et1ahns@group.calendar.google.com");
        return userRepository.insert(user
                .calendar(userCal)
                .roles(Arrays.asList(new Role[]{Role.newInstance(UserRoles.PATIENT)}))
        );
//                .calendar(CalendarUtil.convertGoogleCalendarToCalendarModel(calendar)));
    }

    /**
     * Delets user from the database
     *
     * @param userId
     */
    @Override
    public void removeUser(String userId) {
        getUserById(userId)
                .ifPresent(user -> {
                    if (!StringUtils.isEmpty(user.getDisplayImagePath()))
                        FileUploadUtil.removeFile(user.getDisplayImagePath());
                });
        userRepository.deleteById(userId);
    }

    /**
     * Performs the logic to update the user details into the database
     *
     * @param user User info to persist into the database
     */
    @Override
    public User updateUser(User user) {
        return userRepository.save(user);
    }

    /**
     * Retrieve the user on the basis of username
     *
     * @param username
     * @return
     */
    @Override
    public Optional<User> getUserByUsername(String username) {
        Optional<User> user = userRepository.findByUsername(username)
                .stream().findFirst();

        user.ifPresent(u ->{
            appointmentQueueService.findByDoctorId(u.getId())
                    .ifPresent(queue->{
                        u.setAppointmentQueue(queue);
                    });
        });

        return user;
    }

    /**
     * Fetch the user object by its email
     *
     * @param email
     * @return
     */
    @Override
    public Optional<User> getUserByEmail(String email) {
        Optional<User> user = userRepository.findByEmail(email).stream().findFirst();
        user.ifPresent(u ->{
            appointmentQueueService.findByDoctorId(u.getId())
                    .ifPresent(queue->{
                        u.setAppointmentQueue(queue);
                    });
        });
        return user;
    }

    /**
     * Creates new  user from the registered information
     *
     * @param userRegistrationForm
     * @return
     */
    @Override
    public User registerNewUser(UserRegistrationForm userRegistrationForm) {

        Calendar calendar = calendarService.createCalendar(
                new Calendar()
                        .setSummary(userRegistrationForm.getEmail())
                        .setDescription(new Gson().toJson(userRegistrationForm))
        );

        if (calendar== null)
            throw new CalendarException("Error creating new Calendar");

        log.debug("Calendar Created!");
        log.debug("Calendar Id: " + calendar.getId());

        User user = userRepository.insert(User.newInstance()
                .firstName(userRegistrationForm.getFirstName())
                        .lastName(userRegistrationForm.getLastName())
                        .username(userRegistrationForm.getUsername())
                        .password(userRegistrationForm.getPassword())
                        .email(userRegistrationForm.getEmail())
                        .roles(Arrays.asList(new Role[]{Role.newInstance(userRegistrationForm.getUserType())}))
                        .calendar(CalendarUtil.convertGoogleCalendarToCalendarModel(calendar))
                .mobile(userRegistrationForm.getMobile())
        );

        return user;
    }

    @Override
    public Optional<File> uploadDisplayPicture(MultipartFile file, String uploadFolder, User user) {
        final String UPLOAD_DIR = uploadFolder;
        Optional<User> userOptional =  getUserById(user.getId());
        File uploadedFile = null;
        if (userOptional.isPresent())
        {
            User u = userOptional.get();
            FileUploadUtil.removeFile(user.getDisplayImagePath());
            uploadedFile = FileUploadUtil.uploadFile(file, UPLOAD_DIR);
            u.setDisplayImagePath(uploadedFile.getAbsolutePath());
            u.setDisplayImageName(uploadedFile.getName());
            updateUser(u);
        }
        return Optional.ofNullable(uploadedFile);
    }

    @Override
    public void removeAllUsers() {
        getAllUsers().forEach(user -> {
            removeUser(user.getId());
        });
    }


}
