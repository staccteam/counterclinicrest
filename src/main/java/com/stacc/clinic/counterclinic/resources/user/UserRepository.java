package com.stacc.clinic.counterclinic.resources.user;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends MongoRepository<User, String> {


    @Override
    void deleteAll();

    /**
     * Deletes the entity with the given id.
     *
     * @param s must not be {@literal null}.
     * @throws IllegalArgumentException in case the given {@code id} is {@literal null}
     */
    @Override
    void deleteById(String s);

    @Override
    List<User> findAll();

    Optional<User> findById(String userId);

    @Override
    User insert(User user);

    @Override
    User save(User user);

    List<User> findByEmail(String email);

    List<User> findByUsername(String username);

}
