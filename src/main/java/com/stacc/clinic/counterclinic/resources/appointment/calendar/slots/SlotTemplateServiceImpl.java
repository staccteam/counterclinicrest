package com.stacc.clinic.counterclinic.resources.appointment.calendar.slots;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class SlotTemplateServiceImpl implements SlotTemplateService {

    private SlotTemplateRepository slotTemplateRepository;
    private TimeSlotService timeSlotService;

    @Autowired
    public SlotTemplateServiceImpl(SlotTemplateRepository slotTemplateRepository, TimeSlotService timeSlotService) {
        this.slotTemplateRepository = slotTemplateRepository;
        this.timeSlotService = timeSlotService;
    }

    public SlotTemplate createNewSlotTemplate(SlotTemplate slotTemplate)
    {
        return this.slotTemplateRepository.insert(slotTemplate);
    }

    public SlotTemplate updateSlotTemplate(SlotTemplate existingSlotTemplate, SlotTemplate slotTemplate)
    {
        SlotTemplate template = existingSlotTemplate;
        template.setStartTime(slotTemplate.getStartTime());
        template.setEndTime(slotTemplate.getEndTime());
        template.setSlotDuration(slotTemplate.getSlotDuration());
        template.setSelectedWeekdays(slotTemplate.getSelectedWeekdays());
        return this.slotTemplateRepository.save(template);
    }

    /**
     * Creates Slot Template if not exist
     * otherwise refreshed it with the new values
     *
     * @param slotTemplate
     * @return
     */
    @Transactional
    @Override
    public List<TimeSlot> refreshSlotTemplate(SlotTemplate slotTemplate) {

        // fetch template with the type
        List<SlotTemplate> slotTemplateList = this.slotTemplateRepository.findByWhatFor(slotTemplate.getWhatFor());
        // if present then update
        Optional<SlotTemplate> existingSlotTemplate = slotTemplateList.stream().findFirst();
        if (existingSlotTemplate.isPresent())
        {
            slotTemplate = updateSlotTemplate(existingSlotTemplate.get(), slotTemplate);
        }

        else
        {
            slotTemplate = this.slotTemplateRepository.insert(slotTemplate);
        }

        List<TimeSlot> timeSlots = this.timeSlotService.refreshTimeSlots(slotTemplate);

        // insert slot template
        return timeSlots;
    }

    public List<SlotTemplate> getAllSlotTemplates()
    {
        return slotTemplateRepository.findAll();
    }

    @Override
    public void deleteTemplateById(String slotId) {
        slotTemplateRepository.deleteById(slotId);
    }
}
