package com.stacc.clinic.counterclinic.resources.user;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Role {
    private UserRoles name;
    private List<Permission> permissions;

    public Role(){}

    public Role(List<Permission> permissions) {
        this.permissions = permissions;
    }

    public Role(UserRoles name)  {
        this.name  =  name;
    }

    public static Role newInstance(UserRoles name) {
        return new Role(name);
    }

    public Role addPermission(Permission permission) {
        if  (null == permissions)
            permissions =  new ArrayList<>();
        permissions.add(permission);
        return this;
    }

    public Role removePermission(Permission permission) {
        if (null == permissions)
            return this;
        permissions.remove(permission);
        return this;
    }
}
