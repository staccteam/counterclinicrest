package com.stacc.clinic.counterclinic.resources.appointment.actions;

import com.stacc.clinic.counterclinic.resources.appointment.Appointment;
import com.stacc.clinic.counterclinic.resources.appointment.AppointmentForm;
import com.stacc.clinic.counterclinic.resources.appointment.queue.AppointmentQueue;
import com.stacc.clinic.counterclinic.resources.appointment.queue.AppointmentQueueRepository;
import com.stacc.clinic.counterclinic.resources.appointment.queue.AverageTimeCalculationLogic;

import java.util.List;
import java.util.Optional;

public class OnCreateAction implements AppointmentAction<Appointment> {

    private final AppointmentForm appointmentForm;
    private Appointment newAppointment;

    private AppointmentQueueRepository appointmentQueueRepository;

    public OnCreateAction(AppointmentQueueRepository appointmentQueueRepository, AppointmentForm currAppointmentRequest) {
        this.appointmentQueueRepository = appointmentQueueRepository;
        this.newAppointment = new Appointment();
        this.appointmentForm = currAppointmentRequest;
    }

    /**
     * Executes the actions of incrementing the queueSize by one.
     * @author  Varun Shrivastava
     * @return
     */
    @Override
    public Appointment execute() {

        List<AppointmentQueue> appointmentQueues = appointmentQueueRepository.findByDoctorId(this.appointmentForm.getDoctorId());

        /**
         * If  appointment queue is empty then
         * init a new appointment queue
         */
        if (appointmentQueues.isEmpty()) {
            AppointmentQueue currQueueState = this.appointmentQueueRepository.insert(AppointmentQueue.initForDoctor(this.appointmentForm.getDoctorId()));
            newAppointment.setAppointmentTime(calculateApproxRemainingTime(currQueueState));
            newAppointment.setAppointmentNumber(currQueueState.getQueueSize().longValue());
            newAppointment.setAppointmentQueueId(currQueueState.getId());
            return newAppointment;
        }

        Optional<AppointmentQueue> currQueueState = appointmentQueues.stream().findFirst();
        if (! currQueueState.isPresent())
            throw new AppointmentQueueException("Error trying to increment the queue count of doctor with id: "  + this.appointmentForm.getDoctorId());

        /**
         * If appointment queue is present then
         * increment queueSize by 1
         */
        currQueueState.get().setQueueSize(currQueueState.get().getQueueSize()+1);  // increment queueSize by 1
        newAppointment.setAppointmentTime(calculateApproxRemainingTime(currQueueState.get()));
        newAppointment.setAppointmentNumber(currQueueState.get().getQueueSize().longValue());
        newAppointment.setAppointmentQueueId(currQueueState.get().getId());
        this.appointmentQueueRepository.save(currQueueState.get());
        return newAppointment;
    }

    /**
     * Calculates average time based on the average time calculation login
     * @param currQueueState
     * @return
     */
    private long calculateAvgCheckupTime(Optional<AppointmentQueue> currQueueState) {
        if (currQueueState.get().getAverageTimeCalculationLogic().equals(AverageTimeCalculationLogic.AUTOMATIC))
        {
            return currQueueState.get().getTotalCheckupTime() /  currQueueState.get().getQueueSize();
        }
        return currQueueState.get().getAvgCheckupTime();
    }

    /**
     * Finds the current patient total checkup time
     * @param currQueueState
     * @return
     */
    private long findPatientCheckupTime(Optional<AppointmentQueue> currQueueState) {
        return System.currentTimeMillis() - currQueueState.get().getPatientInTimeStamp();
    }

    /**
     * Calculates approx time remaining before the next patient turn comes in
     * @param currQueueState
     * @return
     */
    private String calculateApproxRemainingTime(AppointmentQueue currQueueState) {
        return String.valueOf(
                currQueueState.getExtendedTime() + (currQueueState.getAvgCheckupTime()  *  (currQueueState.getQueueSize()  -  currQueueState.getCurrQueueCount()))
        );
    }
}
