package com.stacc.clinic.counterclinic.resources.clinic;

import com.stacc.clinic.counterclinic.resources.user.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface ClinicRepository extends MongoRepository<Clinic, String> {

    @Override
    void deleteAll();

    @Override
    List<Clinic> findAll();

    Optional<Clinic> findById(String clinicId);

    @Override
    Clinic insert(Clinic clinic);

    @Override
    Clinic save(Clinic clinic);

    @Override
    void deleteById(String clinicId);
}
