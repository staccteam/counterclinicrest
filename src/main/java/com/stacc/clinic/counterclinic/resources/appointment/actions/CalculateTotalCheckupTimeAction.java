package com.stacc.clinic.counterclinic.resources.appointment.actions;

import com.stacc.clinic.counterclinic.resources.appointment.queue.AppointmentQueue;
import com.stacc.clinic.counterclinic.resources.appointment.queue.AppointmentQueueRepository;
import com.stacc.clinic.counterclinic.resources.user.User;

import java.util.List;
import java.util.Optional;

public class CalculateTotalCheckupTimeAction implements AppointmentAction<AppointmentQueue> {

    private AppointmentQueue  appointmentQueue;

    public CalculateTotalCheckupTimeAction(AppointmentQueue appointmentQueue) {
        this.appointmentQueue = appointmentQueue;
    }

    @Override
    public AppointmentQueue execute() {
        long lastPatientInTimeStamp = appointmentQueue.getPatientInTimeStamp();
        if (lastPatientInTimeStamp == 0)
        {
            appointmentQueue.setTotalCheckupTime(0L);
            return appointmentQueue;
        }
        long currPatientInTimeStamp = System.currentTimeMillis();
        long totalCheckupTime = appointmentQueue.getTotalCheckupTime() + (currPatientInTimeStamp - lastPatientInTimeStamp);
        appointmentQueue.setTotalCheckupTime(totalCheckupTime);
        return appointmentQueue;
    }
}
