package com.stacc.clinic.counterclinic.resources.authorization;

import com.stacc.clinic.counterclinic.resources.user.User;

public class UserNotAuthorizedException extends RuntimeException {
    private User user;

    public UserNotAuthorizedException(String msg, User user) {
        super(msg);
        this.user  = user;
    }
}
