package com.stacc.clinic.counterclinic.resources.appointment.calendar.slots;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface TimeSlotService {
    List<TimeSlot> refreshTimeSlots(SlotTemplate slotTemplate);

    List<TimeSlot> getAllSlots();

    /**
     * Fetch timeslot by respective id
     * @param timeSlotId
     * @return
     */
    Optional<TimeSlot> getTimeSlot(String timeSlotId);
}
