package com.stacc.clinic.counterclinic.resources.appointment.calendar;

public class CalendarException  extends RuntimeException {

    public CalendarException()
    {
        super();
    }

    public CalendarException(String msg)
    {
        super(msg);
    }
}
