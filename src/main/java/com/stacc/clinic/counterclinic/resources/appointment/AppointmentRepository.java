package com.stacc.clinic.counterclinic.resources.appointment;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AppointmentRepository extends MongoRepository<Appointment, String> {

    Optional<Appointment> findById(String appointmentId);

    @Override
    List<Appointment> findAll(Sort sort);

    List<Appointment> findAllByOrderByAppointmentTimeStamp();

    Appointment insert(Appointment appointment);

    Appointment save(Appointment appointment);

    /**
     * Deletes the entity with the given id.
     *
     * @param appointmentId must not be {@literal null}.
     * @throws IllegalArgumentException in case the given {@code id} is {@literal null}
     */
    @Override
    void deleteById(String appointmentId);

    /**
     * Deletes all entities managed by the repository.
     */
    @Override
    void deleteAll();

    Long countByAppointmentType(AppointmentType appointmentType);

    List<Appointment> findByDoctorId(String doctorId);

    Optional<Appointment> findByAppointmentNumber(int appointmentNumber);
}
