package com.stacc.clinic.counterclinic.resources.appointment.calendar.slots;

public class InvalidSlotTemplateException extends RuntimeException {
    public InvalidSlotTemplateException(String msg) {
        super(msg);
    }
}
