package com.stacc.clinic.counterclinic.resources.appointment.actions;

import com.stacc.clinic.counterclinic.resources.appointment.queue.AppointmentQueue;
import com.stacc.clinic.counterclinic.resources.appointment.queue.AppointmentQueueRepository;
import com.stacc.clinic.counterclinic.resources.user.User;

import java.util.List;
import java.util.Optional;

public interface AppointmentAction<E> {
    final int FIRST = 0;

    static Optional<AppointmentQueue> getAppointmentQueue(AppointmentQueueRepository appointmentQueueRepository, User doctor) {
        List<AppointmentQueue> appointmentQueueList = appointmentQueueRepository.findByDoctorId(doctor.getId());
        if (!appointmentQueueList.isEmpty())
            return Optional.of(appointmentQueueList.get(FIRST));

        return Optional.empty();
    }

    E execute();
}
