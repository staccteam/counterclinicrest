package com.stacc.clinic.counterclinic.resources.appointment.actions;

import com.stacc.clinic.counterclinic.resources.appointment.queue.AppointmentQueue;
import com.stacc.clinic.counterclinic.resources.appointment.queue.AppointmentQueueRepository;
import com.stacc.clinic.counterclinic.resources.user.User;
import lombok.extern.log4j.Log4j;

import java.util.List;
import java.util.Optional;

@Log4j
public class IncrementCurrentQueueCount implements AppointmentAction<AppointmentQueue> {

    private AppointmentQueue appointmentQueue;

    public IncrementCurrentQueueCount(AppointmentQueue appointmentQueue) {
        this.appointmentQueue = appointmentQueue;
    }

    @Override
    public AppointmentQueue execute() {
        Integer currQueueCount = appointmentQueue.getCurrQueueCount();
        if (currQueueCount < appointmentQueue.getQueueSize())
        {
            appointmentQueue.setCurrQueueCount(currQueueCount+1);
        }
        return appointmentQueue;
    }
}
