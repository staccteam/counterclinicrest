package com.stacc.clinic.counterclinic.resources.appointment.queue;

public enum AverageTimeCalculationLogic {
    AUTOMATIC,
    MANUAL
}
