package com.stacc.clinic.counterclinic.utils;

import com.stacc.clinic.counterclinic.resources.appointment.OnlineAppointment;
import lombok.extern.log4j.Log4j;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

@Log4j
public class EmailUtil {

    public static void sendMail(OnlineAppointment.Expand onlineAppointment,  String to, String htmlBody)
    {
        String gmailId = System.getenv("gmail_id");
        log.debug("Gmail ID: " + gmailId);
        try {
            Properties props = new Properties();
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.port", "587");
            Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(gmailId, System.getenv("gmail_password"));
                }
            });

            Multipart mp = new MimeMultipart();

            MimeBodyPart htmlPart = new MimeBodyPart();
            htmlPart.setContent(htmlBody, "text/html");
            mp.addBodyPart(htmlPart);

//            MimeBodyPart attachment = new MimeBodyPart();
//            attachment.setFileName(to+".pdf");
//            attachment.setContent(pdfContent, "application/pdf");
//            mp.addBodyPart(attachment);

            Message msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress("varunshrivastava007@gmail.com", "Senior Developer"));
            msg.addRecipient(Message.RecipientType.TO,
                    new InternetAddress(to, "Mr/Mrs User"));
            msg.setSubject("Mail from BeMyAficionado' Web Development Team");
            msg.setText("Greetings :) Please find the receipt of your transaction at Counter Clinic");
            msg.setContent(mp);
            Transport.send(msg);
        } catch (MessagingException | UnsupportedEncodingException e) {
            log.error("no mail sent", e);
            throw new UtilException("Cannot send email.");
        }
    }
    public static void sendMail(OnlineAppointment.Expand onlineAppointment, String to) {
        String htmlBody = ("<p><strong>Greetings :)</strong></p> " +
                "<p>Please find the appointment details below:</p>" +
                "<table border=\"1\">" +
                "<tr>" +
                "<td>Appointed Doctor</td>" +
                "<td>:doctor</td>" +
                "</tr>" +
                "<tr>" +
                "<td>Appointment Date</td>" +
                "<td>:date</td>" +
                "</tr>" +
                "<tr>" +
                "<td>Appointment Time</td>" +
                "<td>:time</td>" +
                "</tr>" +
                "</table>" +
                "<p>Thanks & Regards,</p>" +
                "<p>Counter Clinic</p>").replaceAll(":doctor", onlineAppointment.getDoctor().getFirstName() +" "+ onlineAppointment.getDoctor().getLastName())
                .replaceAll(":date", onlineAppointment.getDate())
                .replaceAll(":time", onlineAppointment.getSlot().getStartTime());
        sendMail(onlineAppointment, to, htmlBody);
    }
}
