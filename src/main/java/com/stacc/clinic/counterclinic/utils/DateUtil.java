package com.stacc.clinic.counterclinic.utils;

import com.google.api.client.util.DateTime;
import com.stacc.clinic.counterclinic.resources.appointment.calendar.slots.TimeSlot;
import freemarker.template.SimpleDate;
import lombok.extern.log4j.Log4j;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Log4j
public class DateUtil {

    public static final String ISO8601_FORMAT = "yyyy-mm-dd'T'hh:mm:ss";
    public static final String ISO8601_LOCAL_FORMAT = "yyyy-mm-dd'T'HH:mm";
    public static final String YODA_DATE_TIME_FORMAT = "yyyy-mm-dd hh:mm:ss";
    public static final String YODA_DATE_TIME_FORMAT_24Hour = "yyyy-mm-dd HH:mm:ss";
    public static final String YODA_TIME_FORMAT = "hh:mm:ss";
    public static final String UTC_TIME_FORMAT = "yyyy-mm-dd'T'hh:mmZ";
    public static final String TIMESLOT_DATETIME_FORMAT = "HH:mm";
    public static final String YODA_DATE_FORMAT = "yyyy-MM-dd";

    /**
     * Formats the date based on the input and output parameter
     * @param date
     * @param inputFormat
     * @param outputFormat
     * @return
     */
    public static String convertDateFormat(String date, String inputFormat, String outputFormat)
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(inputFormat);
        LocalDateTime inputDate = LocalDateTime.parse(date, formatter);
        DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern(outputFormat);
        return ZonedDateTime.of(inputDate, ZoneId.systemDefault()).format(outputFormatter);
//
//        SimpleDateFormat inputFormatter = new SimpleDateFormat(inputFormat);
//        SimpleDateFormat outputFormatter = new SimpleDateFormat(outputFormat);
//        String outputDate  = "";
//        try {
//            Date inputDate = inputFormatter.parse(date);
//            outputDate = outputFormatter.format(inputDate);
//        } catch (ParseException e) {
//            throw new UtilException(String.format("Error parsing date(%s)\nError Message: %s", date, e.getMessage()));
//        }
//        return outputDate;
    }

    public static String convertDateFormatWithSDF(String  date, String inputFormat, String outputFormat)
    {
        SimpleDateFormat sdfIn = new SimpleDateFormat(inputFormat);
        SimpleDateFormat sdfOut = new SimpleDateFormat(outputFormat);
        Date inputDate = null;
        try {
            inputDate = sdfIn.parse(date);
        } catch (ParseException e) {
            throw new UtilException("Problem parsing file. " + e.getMessage());
        }
        String formattedDate = sdfOut.format(inputDate);
        log.debug("Incoming Date: " + date + ", Input format: " + inputFormat);
        log.debug("Outgoing Date: " + formattedDate + ", Output Format: " + outputFormat);
        return formattedDate;
    }

    public static Long convertISO8601LocalToMilliseconds(String dateTime) {
        LocalDateTime inputDate = LocalDateTime.parse(dateTime, DateTimeFormatter.ISO_DATE_TIME);
        return inputDate.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }

    public static Long addMinutesToTime(Long timeInMillis, int minutes) {
        return timeInMillis + Duration.ofMinutes(minutes).toMillis();
    }

    public static String convertDateToIsoOffsetFormat(LocalDateTime date) {
        DateTimeFormatter formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
        return ZonedDateTime.of(date, ZoneId.systemDefault()).format(formatter);
    }

    public static Long convertDateTimeToMillis(DateTime dateTime) {
        return dateTime.getValue();
    }

    public static String[] getWeekdays()
    {
        DateFormatSymbols dfs = new DateFormatSymbols();
        return dfs.getWeekdays();
    }

    public static List<String> getTimeIn15MinutesSlots()
    {
        String postfix = "AM";
        int hour = 0;
        int min = 0;
        int hourCounter = 0;
        List<String> timeSlots = new ArrayList<>();
        while(hourCounter < 24)
        {
            timeSlots.add(String.format("%s:%s %s",
                    padLeft(2, "0", hour),
                    padLeft(2, "0", min),
                    postfix));

            min += 15;
            if (min == 60)
            {
                min = 0;
                hour ++;
                hourCounter ++;
                if (hour >= 12)
                    postfix = "PM";
            }
            if (hour >= 12)
                hour = 0;
        }
        return timeSlots;

    }

    public static List<TimeSlot> getTimeSlots(String startTime, String endTime, int slotDuration)
    {
        SimpleDateFormat sdf =  new SimpleDateFormat(TIMESLOT_DATETIME_FORMAT);
        Date startDate = null;
        Date endDate = null;
        try {
            startDate = sdf.parse(startTime);
            endDate = sdf.parse(endTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar  cal = Calendar.getInstance();
        cal.setTime(startDate);

        List<TimeSlot> timeSlots = new ArrayList<>();
        while (endDate.after(cal.getTime()))
        {
            String s = sdf.format(cal.getTime());
            cal.add(Calendar.MINUTE, slotDuration);
            timeSlots.add(new TimeSlot(s, sdf.format(cal.getTime())));
        }

        return timeSlots;
    }

    public static String padLeft(int reqLength, String padWith, Object val)
    {
        String input = String.valueOf(val);
        int padSize = reqLength - input.length();
        for (int i=0; i<padSize; i++)
        {
            input = (padWith + input);
        }
        return input;
    }

    public static Date parseDate(String dateTime, String inputDateFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(inputDateFormat);
        Date parsedDate = null;
        try {
            parsedDate = sdf.parse(dateTime);
        } catch (ParseException e) {
            throw new UtilException("Invalid Date Exception");
        }
        return parsedDate;
    }
}
