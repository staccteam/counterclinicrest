package com.stacc.clinic.counterclinic.utils;

import lombok.extern.log4j.Log4j;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Paths;

@Log4j
public class FileUploadUtil {
    public static File uploadFile(MultipartFile multipartFile, String uploadDir) {
        File uploadDirectory = new File(uploadDir);
        String fileUploadPath = uploadDirectory.getAbsolutePath() + "/" + multipartFile.getOriginalFilename();
        log.debug("File upload path: " +  fileUploadPath);
        if  (! uploadDirectory.isDirectory())
        {
            uploadDirectory.mkdirs();
            writeBytesToFile(multipartFile, fileUploadPath);
            return new File(fileUploadPath);
        }
        writeBytesToFile(multipartFile, fileUploadPath);
        return new File(fileUploadPath);
    }

    private static void writeBytesToFile(MultipartFile multipartFile, String fileUploadPath) {
        try (FileOutputStream fos = new FileOutputStream(fileUploadPath))
        {
            fos.write(multipartFile.getBytes());
        } catch (IOException e)
        {
            log.error("Error uploading file. Message: " + e.getMessage(), e);
        }
    }

    public static void removeFile(String displayImagePath) {
        log.debug("removing file: "  + displayImagePath);
        if (!StringUtils.isEmpty(displayImagePath))
        {
            File file = new File(displayImagePath);
            if (file.exists())
                file.delete();
        }

    }

    public static void cleanDirectory(File directory) {
        try
        {
            if (directory.isDirectory())
                FileUtils.cleanDirectory(directory);
        } catch (IOException e) {
            log.error("Problem uploading advertisement picture.  Message: " + e.getMessage(), e);
        }
    }
}
