package com.stacc.clinic.counterclinic.utils;

public class UtilException extends RuntimeException {
    public UtilException(String message) {
        super(message);
    }
}
