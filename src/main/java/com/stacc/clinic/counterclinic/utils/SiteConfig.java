package com.stacc.clinic.counterclinic.utils;

import com.stacc.clinic.counterclinic.resources.sitemeta.SiteMetaService;
import com.stacc.clinic.counterclinic.resources.user.User;
import com.stacc.clinic.counterclinic.resources.user.UserRoles;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@Log4j
public class SiteConfig {

    private SiteMetaService siteMetaService;

    public SiteConfig(SiteMetaService siteMetaService) {
        this.siteMetaService = siteMetaService;
    }

    public static final String SITE_NAME  = "site_name";
    public static final String SITE_LOGO = "site_logo";
    public static final String SITE_TIMEZONE = "site_timezone";
    public static final String SITE_SETTINGS = "site_settings";
    public static final String SITE_ROUTES = "site_routes";
    public static final String SITE_AUTH_COOKIE = "auth_token";
    private static final Map<String, Map<String,String>> GLOBAL_CONFIG;
    private static final Map<String, String> GLOBAL_SETTING;
    private static final Map<String, String> GLOBAL_ROUTES;


    public static final String LOGIN_ROUTE = "login_route";
    public static final String LOGOUT_ROUTE = "logout_route";
    public static final String DOCTOR_DASHBOARD_ROUTE = "doctor_dashboard";
    public static final String RECEPTION_DASHBOARD_ROUTE = "reception_dashboard";
    public static final String ADMIN_DASHBOARD_ROUTE = "admin_dashboard";
    public static final String SUPER_ADMIN_DASHBOARD_ROUTE = "superadmin_dashboard";
    public static final String MAKE_APPOINTMENT_ROUTE = "make_appointment";
    public static final String ASSIGN_CLINIC_DOCTOR_ROUTE = "assign_clinic_doctor";
    public static final String MODIFY_EVENT_DETAILS_PAGE_ROUTE = "modify_event_page";
    public static final String ADMIN_ONLINE_APPOINTMENTS_ROUTE = "admin_online_appointments_page";


    static
    {
        GLOBAL_CONFIG = new HashMap<>();
        GLOBAL_SETTING = new HashMap<>();
        GLOBAL_ROUTES = new HashMap<>();
        
        GLOBAL_CONFIG.put(SITE_SETTINGS, GLOBAL_SETTING);
        GLOBAL_CONFIG.put(SITE_ROUTES, GLOBAL_ROUTES);
        
        // Global Site Settings
        GLOBAL_SETTING.put(SITE_LOGO, "https://i2.wp.com/www.bemyaficionado.com/wp-content/uploads/2017/04/bma-temp-logo.png");
        GLOBAL_SETTING.put(SITE_TIMEZONE, "Asia/Calcutta");
        GLOBAL_SETTING.put(SITE_NAME, "Counter Clinic");

        // Global Site Routes
        GLOBAL_ROUTES.put(LOGIN_ROUTE, "/login/user");
        GLOBAL_ROUTES.put(LOGOUT_ROUTE, "/logout");
        GLOBAL_ROUTES.put(DOCTOR_DASHBOARD_ROUTE, "/doctor");
        GLOBAL_ROUTES.put(RECEPTION_DASHBOARD_ROUTE, "/reception");
        GLOBAL_ROUTES.put(ADMIN_DASHBOARD_ROUTE, "/admin");
        GLOBAL_ROUTES.put(SUPER_ADMIN_DASHBOARD_ROUTE, "/superadmin");
        GLOBAL_ROUTES.put(ADMIN_ONLINE_APPOINTMENTS_ROUTE, "/admin/register-slots");
        GLOBAL_ROUTES.put(MAKE_APPOINTMENT_ROUTE, "/reception/make-appointment");
        GLOBAL_ROUTES.put(ASSIGN_CLINIC_DOCTOR_ROUTE, "/reception/assign-clinic-doctor");
        GLOBAL_ROUTES.put(MODIFY_EVENT_DETAILS_PAGE_ROUTE, "/reception/edit/appointments/{calendarId}/{eventId}");
    }

    private SiteConfig()  {}

    public static Map<String, String> get(String key) {
        return GLOBAL_CONFIG.get(key);
    }

    public static String getDashboardUrl(Optional<User> user) {
        String dashboardUrl = get(SITE_ROUTES).get(LOGIN_ROUTE);
        if (user.isPresent())
        {
            UserRoles roleName = user.get().getRoles().stream().findFirst().get().getName();

            if (roleName.equals(UserRoles.DOCTOR))
                dashboardUrl = get(SITE_ROUTES).get(DOCTOR_DASHBOARD_ROUTE);

            if (roleName.equals(UserRoles.RECEPTION))
                dashboardUrl = get(SITE_ROUTES).get(RECEPTION_DASHBOARD_ROUTE);

            if (roleName.equals(UserRoles.ADMIN))
                dashboardUrl = get(SITE_ROUTES).get(ADMIN_DASHBOARD_ROUTE);

            if (roleName.equals(UserRoles.SUPER_ADMIN))
                dashboardUrl = get(SITE_ROUTES).get(SUPER_ADMIN_DASHBOARD_ROUTE);
        }
        return dashboardUrl;
    }

    public static SiteConfig newInstance(SiteMetaService siteMetaService)
    {
        return new SiteConfig(siteMetaService);
    }

    public String getSiteTimezone()
    {
        return siteMetaService.fetchSiteOptionByKey(SITE_TIMEZONE)
                .map(siteOption -> {
                    log.info("Site Timezone: " + siteOption.getVal());
                    return siteOption.getVal();
                }).orElse("Asia/Kuwait");
    }
}
