package com.stacc.clinic.counterclinic.utils;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import lombok.Data;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;

@Log4j
@Component
public class QRCodeUtil {
    private static final String DEFAULT_QRCODE_FILEPATH = "./";

    public static boolean generateQRCode(QRCodeWriter  qrCodeWriter, QRCodeInput qrCodeInput) throws QRCodeException {
        BitMatrix bitMatrix;
        Path path;
        boolean isSuccess = false;
        try {
            bitMatrix = qrCodeWriter.encode(qrCodeInput.getTextToEncode()
                    , BarcodeFormat.QR_CODE, qrCodeInput.getWidth(), qrCodeInput.getHeight());
            if (qrCodeInput.isSaveToFilePath()) {
                path = FileSystems.getDefault().getPath(qrCodeInput.getFilePath());
            } else {
                path = FileSystems.getDefault().getPath(DEFAULT_QRCODE_FILEPATH);
            }
            MatrixToImageWriter.writeToPath(bitMatrix, qrCodeInput.getFileFormat(), path);
            isSuccess = true;
        } catch (WriterException | IOException e) {
            String msg = "Error Encountered while generating QRCode: " + e.getMessage();
            log.error(msg, e);
            throw new QRCodeException(msg);
        } finally {
            log.debug("IS QR Code generation successful? " + isSuccess);
        }
        return isSuccess;
    }

    @Data
    public static class QRCodeInput {
        private String textToEncode;
        private Integer height;
        private Integer width;
        private String fileName;
        private boolean saveToFilePath;
        private String filePath;
        private boolean isEncryptionRequired;
        private String encryptionKey;
        private String fileFormat;

        public QRCodeInput(QRCodeInputBuilder builder) {
            log.debug("Building QRCode Input Object");
            this.textToEncode = builder.textToEncode;
            this.height = builder.height;
            this.width =  builder.width;
            this.fileName = builder.fileName;
            this.saveToFilePath =  builder.saveToFilePath;
            this.filePath =  builder.filePath;
            this.isEncryptionRequired  = builder.isEncryptionRequired;
            this.encryptionKey = builder.encryptionKey;
            this.fileFormat  = builder.fileFormat;
        }
    }

    @Data
    public static class QRCodeInputBuilder {
        private String textToEncode;
        private Integer height;
        private Integer width;
        private String fileName;
        private boolean saveToFilePath;
        private String filePath;
        private boolean isEncryptionRequired;
        private String encryptionKey;
        private String fileFormat;

        public QRCodeInputBuilder textToEncode(String textToEncode) {
            this.textToEncode  = textToEncode;
            return this;
        }

        public QRCodeInputBuilder height(Integer height) {
            this.height = height;
            return this;
        }

        public QRCodeInputBuilder width(Integer width) {
            this.width = width;
            return this;
        }

        public QRCodeInputBuilder fileName(String fileName) {
            this.fileName =  fileName;
            return this;
        }

        public QRCodeInputBuilder saveToFilePath(boolean saveToFilePath){
            this.saveToFilePath = saveToFilePath;
            return this;
        }

        public QRCodeInputBuilder filePath(String filePath) {
            this.filePath  = filePath;
            return this;
        }

        public QRCodeInputBuilder isEncryptionReq(boolean isEncryptionRequired) {
            this.isEncryptionRequired =  isEncryptionRequired;
            return this;
        }

        public QRCodeInputBuilder encryptionKey(String encryptionKey) {
            this.encryptionKey = encryptionKey;
            return this;
        }

        public QRCodeInputBuilder fileFormat(String fileFormat) {
            this.fileFormat  = fileFormat;
            return this;
        }

        public QRCodeInput build() {
            return new QRCodeInput(this);
        }
    }
}
