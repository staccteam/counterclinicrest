package com.stacc.clinic.counterclinic.utils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.stacc.clinic.counterclinic.resources.user.User;
import com.stacc.clinic.counterclinic.resources.user.UserService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.log4j.Log4j;
import org.jasypt.util.text.BasicTextEncryptor;
import org.jasypt.util.text.StrongTextEncryptor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.*;

@Log4j
@Component
public class TokenManager {

    private TokenManager(){}

    public static String createAccessToken(User user) {
        return Jwts.builder().setSubject("counter-clinic-rest-client-1")
                .setIssuer("https://www.bemyaficionado.com/")
                .setExpiration(getFutureDateInHours(24))
                .setAudience("consumers")
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .claim("authTime", System.currentTimeMillis())
                .claim("username", user.getUsername())
                .claim("firstName", user.getFirstName())
                .claim("lastName", user.getLastName())
                .signWith(SignatureAlgorithm.HS256, secretKey())
                .compact();
    }

    public static String createRefreshToken(User user){
        BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
        textEncryptor.setPassword("CHANGE_THIS_PASSWORD");
        Map<String, Object> refreshToken = new HashMap<>();
        refreshToken.put("email", user.getEmail());
        refreshToken.put("timestamp",  System.currentTimeMillis());
        return textEncryptor.encrypt(new Gson().toJson(refreshToken));
    }

    private static byte[] secretKey() {
        return "SECRET_KEY".getBytes();
    }

    private static Date getFutureDateInHours(int hour) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR,  hour);
        return cal.getTime();
    }


    public static Jws<Claims> validateAccessToken(String accessToken) {
        return Jwts.parser()
                .setSigningKey(secretKey())
                .parseClaimsJws(accessToken);
    }

    public static Optional<User> provideUserFromToken(String jwt, UserService userService)
    {
        if (StringUtils.isEmpty(jwt))
        {
            log.debug("Auth Token is empty.");
            return Optional.empty();
        }
        return userService.getUserByUsername(
                String.valueOf(
                        TokenManager.validateAccessToken(jwt)
                                .getBody().get("username")));
    }

    public static String generateUniqToken() {
        return UUID.randomUUID().toString().replaceAll("-",  "#");
    }
}
