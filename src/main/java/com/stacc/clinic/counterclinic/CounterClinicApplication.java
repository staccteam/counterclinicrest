package com.stacc.clinic.counterclinic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class CounterClinicApplication {

	public static void main(String[] args) {
		SpringApplication.run(CounterClinicApplication.class, args);
		if (System.getenv("gmail_id") == null)
			throw new RuntimeException("Set Env Variables for gmail_id and gmail_password.");
	}
}
